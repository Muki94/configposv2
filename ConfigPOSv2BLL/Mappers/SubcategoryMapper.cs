﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class SubcategoryMapper
    {
        public static SubcategoryDM MapSubcategoryToSubcategoryDM(this SUBCATEGORIES subcategory) 
        {
            if (subcategory == null)
                return null;

            SubcategoryDM subcategoryModel = new SubcategoryDM()
            {
                SubcategoryID = subcategory.SubcategoryID,
                CategoryID = subcategory.CategoryID,
                Name = subcategory.Name,
                Active = subcategory.Active
            };

            return subcategoryModel;
        }

        public static List<SubcategoryDM> MapSubcategoryListToSubcategoryDMList(this List<SUBCATEGORIES> subcategories)
        {
            if (subcategories == null)
                return null;

            List<SubcategoryDM> subcategoryModels = new List<SubcategoryDM>();

            foreach (var subcategory in subcategories)
            {
                subcategoryModels.Add(new SubcategoryDM()
                {
                    SubcategoryID = subcategory.SubcategoryID,
                    CategoryID = subcategory.CategoryID,
                    Name = subcategory.Name,
                    Active = subcategory.Active
                });
            }

            return subcategoryModels;
        }
    }
}
