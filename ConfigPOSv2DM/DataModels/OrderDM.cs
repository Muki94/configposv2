﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class OrderDM
    {
        public int OrderID { get; set; }
        
        public int ShiftID { get; set; }
        
        public Nullable<int> TableID { get; set; }
        
        public bool IsVIP { get; set; }
        
        public System.DateTime OpenTime { get; set; }
        
        public Nullable<System.DateTime> CloseTime { get; set; }
       
        public bool Closed { get; set; }
        
        public int OpenUserID { get; set; }
       
        public Nullable<int> CloseUserID { get; set; }
    }
}
