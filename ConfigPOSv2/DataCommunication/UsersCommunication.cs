﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class UsersCommunication
    {
        private static UsersService usersService = new UsersService();

        public static List<UserDM> UsersGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<UserDM>>("api/USERS/GetAll", null);
            else
                return usersService.GetUsersAll();
        }

        public static UserDM GetUserByLogin(string passwordHash, string username = null)
        {
            UserDM result = null;

            if (App.usingWebApi)
            {
                result = username != null && username != String.Empty ? WebApiHelper.Get<UserDM>("api/USERS/GetByUsernamePassword", new FilterAttributesDM { password = passwordHash, username = username }) : WebApiHelper.Get<UserDM>("api/USERS/GetByPin", new FilterAttributesDM { password = passwordHash });
                if (result != null)
                    return result;
            }
            else
            {
                result = username != null && username != String.Empty ? usersService.LoginByUsernamePassword(username, passwordHash) : usersService.LoginByPIN(passwordHash);
                if (result != null)
                    return result;
            }

            return null;
        }

        public static bool UsersLogin(string passwordHash, string username = null)
        {
            if (App.usingWebApi)
            {
                var result = WebApiHelper.SetToken(passwordHash, username);
                if (result != null)
                    return true;
            }
            else
            {
                var result = username != null ? usersService.LoginByUsernamePassword(username, passwordHash):usersService.LoginByPIN(passwordHash);

                if (result != null)
                {
                    App.Current.username = result.Name;
                    App.Current.userType = usersService.GetUserGroupByUserGroupId(result.UserGroupID).Name;
                    App.Current.userId = result.UserID.ToString();
                    return true;
                }
            }

            return false;
        }
    }
}
