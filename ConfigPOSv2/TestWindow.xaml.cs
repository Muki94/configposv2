﻿using ConfigPOSv2.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window
    {
        DispatcherTimer timerDateTime = new DispatcherTimer();

        public TestWindow()
        {
            InitializeComponent();
            LoadSettings();

            SetDateTime();
            //Timer za update datuma i vremena
            timerDateTime.Interval = new TimeSpan(0, 1, 0);
            timerDateTime.Tick += timerDateTime_Tick;
            timerDateTime.Start();

            if (JournalForm.IsJournalOpen())
                Shift.IsEnabled = true;

            if (ShiftForm.IsShiftOpen())
                UserShift.IsEnabled = true;
        }

        private void timerDateTime_Tick(object sender, EventArgs e)
        {
            try
            {
                SetDateTime();
            }
            catch
            {

            }
        }
        private void SetDateTime()
        {
            lblTime.Content = DateTime.Now.ToString("HH:mm");
            lblDate.Content = DateTime.Now.ToString("dd.MM.yyyy.");
        }

        private void LoadSettings()
        {

            if (App.login_type == "USERNAME_PASS")
            {
                gridPinPrijava.Visibility = Visibility.Collapsed;
                gridPassPrijava.Visibility = Visibility.Visible;
                gridShiftUsernamePass.Visibility = Visibility.Visible;
                txtUserName.Focus();
            }
            else
            {
                gridShiftPIN.Visibility = Visibility.Visible;
                //txtPIN.Focus();
            }

        }
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            frmMessageBox frm = new frmMessageBox();
            this.Opacity = 0.5;
            if (Convert.ToBoolean(frm.ShowDialog()))
            {
                this.Close();
            }
            this.Opacity = 1;
        }

        private void btnInput_Click(object sender, RoutedEventArgs e)
        {
            Button number = (Button)sender;
            if (number.Uid == "X")
            {
                txtPIN.Password = string.Empty;
            }
            else if (number.Uid == "-1")
            {
                txtPIN.Password = txtPIN.Password.Length > 0 ? txtPIN.Password.Remove(txtPIN.Password.Length - 1) : string.Empty;
            }
            else
            {
                txtPIN.Password += number.Uid;
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        #region manipulisanje smjenama korisnika
        private void UserShift_Click(object sender, RoutedEventArgs e)
        {
            gridShiftLogin.Visibility = Visibility.Visible;
            txtShiftPIN.Focus();
        }

        private void OpenShiftClick(object sender, RoutedEventArgs e)
        {
            var user = ShiftForm.OpenUserShift(App.login_type == "PIN" ? txtShiftPIN.Password : txtShiftPassword.Password, App.login_type == "PIN" ? "" : txtShiftUserName.Text);

            //if (user != null)
            //{
            //    LoginShiftSuccessMessage.Visibility = Visibility.Visible;
            //    LoginSuccessMessage.Content = "Prijava na smjenu uspješna.Dobrodošli," + user.Username;

            //    CallAnimationFadeOut();

            //    gridShiftLogin.Visibility = Visibility.Collapsed;
            //    return;
            //}

            //LoginShiftErrorMessage.Visibility = Visibility.Visible;
            //LoginErrorMessage.Content = "Došlo je do greške!";

            //CallAnimationFadeOut();
        }

        private void CloseShiftClick(object sender, RoutedEventArgs e)
        {
            var user = ShiftForm.CloseUserShift(App.login_type == "PIN" ? txtShiftPIN.Password : txtShiftPassword.Password, App.login_type == "PIN" ? "" : txtShiftUserName.Text);

            if (user != null)
            {
                LoginShiftSuccessMessage.Visibility = Visibility.Visible;
                LoginSuccessMessage.Content = "Odjava sa smjene uspješna.Doviđenja," + user.Username;

                CallAnimationFadeOut();

                gridShiftLogin.Visibility = Visibility.Collapsed;
                return;
            }

            LoginShiftErrorMessage.Visibility = Visibility.Visible;
            LoginErrorMessage.Content = "Zatvaranje smjene nije uspjelo!";

            CallAnimationFadeOut();
        }

        private void CloseForm(object sender, RoutedEventArgs e)
        {
            gridShiftLogin.Visibility = Visibility.Collapsed;
        }

        private void btnCalculatorInput_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;

            if (b.Uid.ToString() == "-1")
            {
                if (txtShiftPIN.Password.Length > 0)
                    txtShiftPIN.Password = txtShiftPIN.Password.Remove(txtShiftPIN.Password.Length - 1);
            }
            else
                txtShiftPIN.Password += b.Uid;
        }

        private void CallAnimationFadeOut()
        {
            //animacija za fadeout
            var a = new DoubleAnimation
            {
                From = 1.0,
                To = 0.0,
                FillBehavior = FillBehavior.Stop,
                BeginTime = TimeSpan.FromSeconds(2),
                Duration = new Duration(TimeSpan.FromSeconds(0.5))
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, LoginShiftSuccessMessage);
            Storyboard.SetTarget(a, LoginShiftErrorMessage);
            Storyboard.SetTargetProperty(a, new PropertyPath(OpacityProperty));
            storyboard.Completed += delegate { LoginShiftSuccessMessage.Visibility = Visibility.Collapsed; };
            storyboard.Completed += delegate { LoginShiftErrorMessage.Visibility = Visibility.Collapsed; };
            storyboard.Begin();
        }
        #endregion
    }
}
