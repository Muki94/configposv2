﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class SectorMapper
    {
        public static SectorDM MapSectorsToSectorDM(this SECTORS sector)
        {
            if (sector == null)
                return null;

            SectorDM sectorModel = new SectorDM()
            {
                SectorID = sector.SectorID,
                Name = sector.Name,
                NumberOrder = sector.NumberOrder,
                Layout = sector.Layout,
                Deleted = sector.Deleted
            };

            return sectorModel;
        }

        public static List<SectorDM> MapSectorsListToSectorDMList(this List<SECTORS> sectors)
        {
            if (sectors == null)
                return null;

            List<SectorDM> sectorModels = new List<SectorDM>();

            foreach (var sector in sectors)
            {
                sectorModels.Add(new SectorDM()
                {
                    SectorID = sector.SectorID,
                    Name = sector.Name,
                    NumberOrder = sector.NumberOrder,
                    Layout = sector.Layout,
                    Deleted = sector.Deleted
                });
            }

            return sectorModels;
        }
         
    }
}
