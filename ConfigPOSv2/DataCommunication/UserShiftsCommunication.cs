﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class UserShiftsCommunication
    {
        private static UserShiftsService userShiftService = new UserShiftsService();

        public static int UserShiftsInsert(UserShiftDM userShift)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, UserShiftDM>("api/USERSHIFTS/Insert", userShift);
            else
                return userShiftService.UserShiftInsert(userShift);
        }

        public static bool UserShiftsClose(UserShiftDM userShift)
        {
            if (App.usingWebApi)
                return WebApiHelper.Put<bool, UserShiftDM>("api/USERSHIFTS/Close", userShift);
            else
                return userShiftService.UserShiftClose(userShift);
        }

        public static UserShiftDM GetUserShift(int userId, int shiftId)
        {
            if (App.usingWebApi)
               return WebApiHelper.Get<UserShiftDM>("api/USERSHIFTS/Get", new FilterAttributesDM { idName = shiftId.ToString(), userId = userId });
            else
               return userShiftService.GetUserShift(userId,shiftId);
        }

        public static List<UserShiftDM> GetNotClosedUserShiftsByShiftId(int shiftId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<UserShiftDM>>("api/USERSHIFTS/GetNotClosedUserShiftsByShiftId", new FilterAttributesDM { idName = shiftId.ToString() });
            else
                return userShiftService.GetNotClosedUserShiftsByShiftId(shiftId);
        }
    }
}
