﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class BillsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public int BillsInsert(BillsDM bill)
        {
            OpenConnection();
            int BillID = Convert.ToInt32(entity.stp_BILLS_Insert(bill.Number, bill.FullBillNumber, bill.OrderID, bill.CreateUserID, bill.StationID, bill.FiscalNumber, bill.Total, bill.ProtectionCode,bill.BillUniqueIdentifier, bill.Tip).FirstOrDefault());
            CloseConnection();
            return BillID;
        }

        public int BillsGetLastNumber()
        {
            OpenConnection();
            int billNumber = Convert.ToInt32(entity.stp_BILLS_GetLastNumber().FirstOrDefault());
            CloseConnection();
            return billNumber;
        }
    }
}
