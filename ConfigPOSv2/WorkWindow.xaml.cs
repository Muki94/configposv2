﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for WorkWindow.xaml
    /// </summary>
    public partial class WorkWindow : Window
    {
        public WorkWindow()
        {
            InitializeComponent();
            LoadCategories();
            LoadSubcategories();
        }

        private void btnCategorySave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           CategoryDM categoryModel = new CategoryDM() { 
                Name = txtCategoryName.Text,
                Active = cbxCategoryActive.IsChecked.Value
           };

           var categoriesInsert = CategoriesCommunication.CategoriesInsert(categoryModel);        	
        }

        public void LoadCategories()
        {
            List<CategoryDM> categories = CategoriesCommunication.CategoriesGetAll();
            cmbCategory.ItemsSource = categories;
            cmbCategory.DisplayMemberPath = "Name";
            cmbCategory.SelectedValuePath = "CategoryID";

            cmbCategory_Update.ItemsSource = categories;
            cmbCategory_Update.DisplayMemberPath = "Name";
            cmbCategory_Update.SelectedValuePath = "CategoryID";

            cmbCategory_Update_2.ItemsSource = categories;
            cmbCategory_Update_2.DisplayMemberPath = "Name";
            cmbCategory_Update_2.SelectedValuePath = "CategoryID";
        }

        public void LoadSubcategories()
        {
            List<SubcategoryDM> subcategories = SubcategoriesCommunication.SubcategoriesGetAll();
            cmbSubcategory_Update.ItemsSource = subcategories;
            cmbSubcategory_Update.DisplayMemberPath = "Name";
            cmbSubcategory_Update.SelectedValuePath = "SubcategoryID";
        }

        private void btnSubCategorySave_Click(object sender, RoutedEventArgs e)
        {
            SubcategoryDM subcategoryModel = new SubcategoryDM() { 
                CategoryID = Int32.Parse(cmbCategory.SelectedValue.ToString()),
                Name = txtSubcategoryName.Text,
                Active = cbxSubcategoryActive.IsChecked.Value
            };

            var subcategoriesInsert = SubcategoriesCommunication.SubcategoriesInsert(subcategoryModel);  
        }

        private void cmbCategory_Update_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtCategoryName_Update.Text = ((CategoryDM)cmbCategory_Update.SelectedItem).Name;
        }

        private void btnCategorySave_Update_Click(object sender, RoutedEventArgs e)
        {
            CategoryDM categoryModel = new CategoryDM()
            {
                Name = txtCategoryName_Update.Text,
                Active = cbxCategoryActive_Update.IsChecked.Value,
                CategoryID = Int32.Parse(cmbCategory_Update.SelectedValue.ToString())
            };

            var categoriesUpdate = WebApiHelper.Put<CategoryDM, CategoryDM>("api/CATEGORIES/Update", categoryModel);        	
        }

        private void cmbSubcategory_Update_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<CategoryDM> categories = CategoriesCommunication.CategoriesGetAll();

            SubcategoryDM item = (SubcategoryDM)cmbSubcategory_Update.SelectedItem;
            txtSubcategoryName_Update.Text = item.Name;

            cmbCategory_Update_2.ItemsSource = categories;
            cmbCategory_Update_2.DisplayMemberPath = "Name";
            cmbCategory_Update_2.SelectedValuePath = "CategoryID";
            cmbCategory_Update_2.SelectedValue = item.CategoryID;

            cbxSubcategoryActive_Update.IsChecked = item.Active;

        }

        private void btnSubCategorySave_Update_Click(object sender, RoutedEventArgs e)
        {
            SubcategoryDM subcategoryModel = new SubcategoryDM()
            {
                SubcategoryID =Int32.Parse(cmbSubcategory_Update.SelectedValue.ToString()),
                CategoryID = Int32.Parse(cmbCategory.SelectedValue.ToString()),
                Name = txtSubcategoryName.Text,
                Active = cbxSubcategoryActive.IsChecked.Value
            };

            var subcategoriesUpdate = WebApiHelper.Put<SubcategoryDM, SubcategoryDM>("api/SUBCATEGORIES/Update", subcategoryModel);        	
        }

        private void btnEditItem_Click(object sender, RoutedEventArgs e)
        {
            frmItemsEdit frm = new frmItemsEdit();
            frm.ShowDialog();
        }

        private void btnAddCategory_Click(object sender, RoutedEventArgs e)
        {
            frmCategoriesAdd frm = new frmCategoriesAdd();
            frm.ShowDialog();
        }
    }
}
