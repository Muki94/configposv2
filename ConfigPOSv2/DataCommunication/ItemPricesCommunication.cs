﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class ItemPricesCommunication
    {
        private static ItemPricesService itemPricesService = new ItemPricesService();

        public static ItemPricesDM ItemPricesGetByItemID(string ItemID,string BussinesUnitID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<ItemPricesDM>("api/ITEM_PRICES/GetByItemID", new FilterAttributesDM { idName = ItemID, username = BussinesUnitID });
            else
                return itemPricesService.ItemPricesGetByItemID(Convert.ToInt32(ItemID),Convert.ToInt32(BussinesUnitID));
        }
    }
}
