﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class RoundItemViewDM
    {
        public int OrderID { get; set; }

        public int RoundID { get; set; }

        public int StationID { get; set; }

        public int RoundItemID { get; set; }

        public int ItemID { get; set; }

        public string ItemName { get; set; }

        public int Number { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }

        public decimal ItemAmount { get; set; }

    }
}
