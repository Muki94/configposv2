﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class BillsItems
    {
        public int BillItemID { get; set; }
        public int BillID { get; set; }
        public int ItemID { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal PriceWithoutDiscount { get; set; }
        public decimal TotalWithoutDiscount { get; set; }
        public decimal TotalTaxesPercentage { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalWithoutTax { get; set; }
        public decimal BasePrice { get; set; }
        public decimal BasePriceWithoutDiscount { get; set; }
    }
}
