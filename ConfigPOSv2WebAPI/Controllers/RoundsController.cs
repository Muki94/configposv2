﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class RoundsController : ApiController
    {
        RoundsService roundsService = new RoundsService();

        // INSERT
        [Route("api/ROUNDS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertRound([FromBody]RoundDM item)
        {
            try
            {
                int addedId = roundsService.RoundInsert(item);
                return Request.CreateResponse(HttpStatusCode.OK, addedId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET 
        [Route("api/ROUNDS/GetRoundNextOrderNumber")]
        [HttpGet]
        public HttpResponseMessage GetRoundNextOrderNumber([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, roundsService.RoundGetNextOrderNumber());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY SECTOR ID
        [Route("api/ROUNDS/GetByTableId")]
        [HttpGet]
        public HttpResponseMessage GetByTableId([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, roundsService.RoundsGetByTableID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
