﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class PaymentsDM
    {
        public int PaymentID { get; set; }
        public int BillID { get; set; }
        public int PaymentTypeID { get; set; }
        public decimal Amount { get; set; }
        public decimal Tip { get; set; }
    }
}
