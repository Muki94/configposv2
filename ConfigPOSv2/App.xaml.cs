﻿using ConfigPOSv2.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2.Properties;
using System.Windows.Threading;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2.DataCommunication;
using Fiscal;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        
        #region Settings variables

        //Postavićemo na početku default-nu vrijednost za svaku postavku
        public static string login_type = "PIN";
        public static string culture = "SYSTEM";
        public static string eCardUsing = "False";

        public static bool prikazi_TeamViewer = false;
        public static string link_TeamViewer = string.Empty;
        public static string fileName = string.Empty;
        public static string TvExePath = string.Empty;
        public static bool usingWebApi = false;
        public static int stationID = 2;
        public static bool usingTables = true;
        #endregion

        #region Private Fields

        SettingsService sService = new SettingsService();


        CryptSection cryptSection = new CryptSection();
        CryptSectionGroup cryptGroup = new CryptSectionGroup();

        #endregion

        #region Splash screen delegate handling
        public App()
        {
            ApplicationInitialize = _applicationInitialize;
        }

        public static new App Current
        {
            get { return Application.Current as App; }
        }

        internal delegate void ApplicationInitializeDelegate(frmSplashScreen splashWindow);
        internal ApplicationInitializeDelegate ApplicationInitialize;
        private void _applicationInitialize(frmSplashScreen splashWindow)
        {
            // Taskovi koji se izvršavaju
            splashWindow.SetProgressText("Checking database connectivity...");
            splashWindow.SetProgress(25);
            try
            {
            }
            catch (Exception)
            {
                splashWindow.SetProgressText("Database not connected.");
                Thread.Sleep(1000);
                return;
            }
            Thread.Sleep(1000);
            splashWindow.SetProgress(50);
            splashWindow.SetProgressText("Loading all settings...");
            try
            {
                //GettingSettings();
                //sService.GetSettingsAll();
            }
            catch (Exception)
            {
                splashWindow.SetProgressText("Settings did not load correctly.");
                Thread.Sleep(1000);
                return;
            }
            Thread.Sleep(1000);
            splashWindow.SetProgress(75);
            splashWindow.SetProgressText("Loading all images...");

            Thread.Sleep(1000);
            splashWindow.SetProgress(100);
            //splashWindow.SetProgressText("Starting program...");
            Thread.Sleep(1000);

            // Create the main window, but on the UI thread.
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate
            {
                //MainWindow MainWindow = new MainWindow();
                MainWindow MainWindow = new MainWindow();
                MainWindow.Show();
            }));
        }
        #endregion

        public string access_token;
        public string refresh_token;

        public string username;
        public string userType;
        public string userId;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            GettingSettings();
            //Postavljamo ResourceDictionary za cijelu aplikaciju koji smo dobili iz Helpera prema Culture-i
            this.Resources.MergedDictionaries.Add(CultureHelper.GetLanguageDictionary());

            //Set XAML StringFormat DateTime for current system culture
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            string rezultat = Helpers.Hash.GetHash("config", Hash.HashType.SHA512);

            cryptSection.CryptSectionByName("connectionStrings");
            cryptSection.CryptSectionByName("log4net");

            cryptGroup.CryptSectionGroupByName("userSettings");

            this.StartupUri = new Uri("frmSplashScreen.xaml", UriKind.Relative);
            //this.StartupUri = new Uri("TestWindow.xaml", UriKind.Relative);

            string FPType = "DATECS";
            string FPPath = "\\\\HUSOM\\Print\\";
            string FPAnswerPath = "\\\\HUSOM\\Print\\Answer";
            string FPAnswerArchivePath = "\\\\HUSOM\\Print\\Answer arhiva";
            string FPNameFile = "print.txt";
            int FPSleepTime = 1000;
            string FPIOSANumber = "1234567890123456";
            string FPOperator = "1";
            string FPOperatorPassword = "00000000";
            string FPMessage1 = "Hvala na posjeti!";
            string FPMessage2 = "Ugodan dan vam želimo!";

            FP.settings = new FiscalSettings(FPType, FPPath, FPAnswerPath, FPAnswerArchivePath, FPNameFile, 1000, FPIOSANumber, FPOperator, FPOperatorPassword, FPMessage1, FPMessage2);

        }

        private void GettingSettings()
        {

            //Način na koji ćemo učitavati postavke iz Settings file-a
            login_type = Settings.Default.LOGIN_TYPE == "PIN" || Settings.Default.LOGIN_TYPE == "USERNAME_PASS" ? Settings.Default.LOGIN_TYPE : login_type;
            eCardUsing = Settings.Default.ECARD_ENABLED;
            usingTables = Settings.Default.TABLES_USING;
            
            //Prikazivanje teamviewer button-a
            prikazi_TeamViewer = Settings.Default.TEAMVIEWER_ENABLED;
            //link za download teamviewer-a
            link_TeamViewer = Settings.Default.TEAMVIEWER_LINK;
            //file name za teamviewer
            fileName = Settings.Default.TEAMVIEWER_FILENAME;
            //exe path za teamviewer
            TvExePath = string.Empty;

            //Ukljuciti settings da provjeri jel se koristi web api ili direktno na bazu 
            List<SettingDM> settings = SettingsCommunication.SettingsGetAll();

            //Način na koji ćemo učitavati postavke iz tabele SETTINGS u bazi 
            List<SettingDM> listSettings = new List<SettingDM>();
            listSettings = sService.GetSettingsAll();

            SettingDM setting = listSettings.Find(x => x.Name == "CULTURE");
            culture = setting != null ? setting.Value : culture;

            //this.StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
        }

    }
}
