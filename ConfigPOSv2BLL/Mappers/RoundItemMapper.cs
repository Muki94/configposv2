﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class RoundItemMapper
    {
        public static RoundItemViewDM MapRoundsItemsViewToRoundItemViewDM(this ROUNDS_ITEMS_VIEW item)
        {
            if (item == null)
                return null;

            RoundItemViewDM Model = new RoundItemViewDM()
            {
                Number = item.Number,
                OrderID = item.OrderID,
                RoundID = item.RoundID,
                ItemID = item.ItemID,
                Price = item.Price,
                RoundItemID = item.RoundItemID,
                ItemName = item.Name,
                Quantity = item.Quantity
            };

            return Model;
        }

        public static List<RoundItemViewDM> MapRoundsItemsViewListToRoundItemViewDMList(this List<ROUNDS_ITEMS_VIEW> items)
        {
            if (items == null)
                return null;

            List<RoundItemViewDM> Models = new List<RoundItemViewDM>();

            foreach (var item in items)
            {
                Models.Add(new RoundItemViewDM()
                {
                    Number = item.Number,
                    OrderID = item.OrderID,
                    RoundID = item.RoundID,
                    ItemID = item.ItemID,
                    Price = item.Price,
                    RoundItemID = item.RoundItemID,
                    ItemName = item.Name,
                    Quantity = item.Quantity
                });
            }

            return Models;
        }
        public static List<RoundItemViewDM> MapRoundsItemsByOrderListToRoundItemViewDMList(this List<ROUNDS_ITEMS_BYORDER> items)
        {
            if (items == null)
                return null;

            List<RoundItemViewDM> Models = new List<RoundItemViewDM>();

            foreach (var item in items)
            {
                Models.Add(new RoundItemViewDM()
                {
                    OrderID = item.OrderID,
                    StationID = item.StationID,
                    ItemID = item.ItemID,
                    Price = item.Price,
                    ItemName = item.Name,
                    Quantity = (decimal)item.Quantity,
                    ItemAmount = (decimal)item.ItemAmount
                });
            }

            return Models;
        }
        public static List<RoundItemViewDM> MapRoundsItemsSimpleListToRoundItemViewDMList(this List<ROUNDS_ITEMS_SIMPLE> items)
        {
            if (items == null)
                return null;

            List<RoundItemViewDM> Models = new List<RoundItemViewDM>();

            foreach (var item in items)
            {
                Models.Add(new RoundItemViewDM()
                {
                    OrderID = item.OrderID,
                    ItemID = item.ItemID,
                    RoundID = item.RoundID,
                    RoundItemID = item.RoundItemID,
                    Quantity = (decimal)item.Quantity
                });
            }

            return Models;
        }
    }
}
