﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class CategoriesCommunication
    {
        private static CategoriesService categoryService = new CategoriesService();

        public static int CategoriesInsert(CategoryDM category)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int,CategoryDM>("api/CATEGORIES/Insert",category);
            else
                return categoryService.CategoryInsert(category);
        }

        public static void CategoriesUpdate(CategoryDM category)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<CategoryDM, CategoryDM>("api/CATEGORIES/Update", category);
            else
                categoryService.CategoryUpdate(category);
        }

        public static void CategoriesDelete(int CategoryID)
        {
            if (App.usingWebApi)
                WebApiHelper.Delete<CategoryDM>("api/CATEGORIES/Update", CategoryID);
            else
                categoryService.CategoryDelete(CategoryID);
        }

        public static List<CategoryDM> CategoriesGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<CategoryDM>>("api/CATEGORIES/GetAll");
            else
                return categoryService.CategoriesGetAll();
        }

        public static CategoryDM CategoriesGetByID(int CategoryID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<CategoryDM>("api/CATEGORIES/GetById", new FilterAttributesDM(){ idName = CategoryID.ToString() });
            else
                return categoryService.CategoriesGetByID(CategoryID);
        }
    }
}
