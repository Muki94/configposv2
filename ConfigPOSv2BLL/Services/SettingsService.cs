﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class SettingsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih postavki iz baze podataka
        /// </summary>
        /// <returns>Lista postavki</returns>
        public List<SettingDM> GetSettingsAll()
        {
            OpenConnection();
            List<SettingDM> list = entity.stp_SETTINGS_GetAll().ToList().MapSettingToSettingDM();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća postavku na osnovu imena postavke
        /// </summary>
        /// <param name="name">ime postavke</param>
        /// <returns>Postavka</returns>
        public SettingDM GetSettingsByName(string name)
        {
            OpenConnection();
            SettingDM setting = entity.stp_SETTINGS_GetByName(name).FirstOrDefault().MapSettingToSettingDM();
            CloseConnection();

            return setting;
        }
    }
}
