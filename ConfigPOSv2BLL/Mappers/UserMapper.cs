﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class UserMapper
    {
        public static UserDM MapUserToUserDM(this USERS user)
        {
            if (user == null)
                return null;

            UserDM userModel = new UserDM()
            {
                UserID = user.UserID,
                UserGroupID = user.UserGroupID,
                Active = user.Active,
                Name = user.Name,
                LastLogin = user.LastLogin,
                Username = user.Username
            };

            return userModel;
        }

        public static List<UserDM> MapUserListToUserDMList(this List<USERS> users)
        {
            if (users == null)
                return null;

            List<UserDM> userModels = new List<UserDM>();

            foreach (var user in users)
            {
                userModels.Add(new UserDM()
                {
                    UserID = user.UserID,
                    UserGroupID = user.UserGroupID,
                    Active = user.Active,
                    Name = user.Name,
                    Password = user.Password,
                    PasswordPIN = user.PasswordPIN,
                    LastLogin = user.LastLogin,
                    Username = user.Username
                });
            }

            return userModels;
        }
    }
}
