﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for frmSplashScreen.xaml
    /// </summary>
    public partial class frmSplashScreen : Window
    {
        public frmSplashScreen()
        {
            InitializeComponent();
            txtCopyRightYear.Content = DateTime.Now.Year;
            this.Loaded += new RoutedEventHandler(Splash_Loaded);
        }

        void Splash_Loaded(object sender, RoutedEventArgs e)
        {
            IAsyncResult result = null;

            // initCompleted se poziva kada se svi taskovi odrade u pozadini
            AsyncCallback initCompleted = delegate(IAsyncResult ar)
            {
                App.Current.ApplicationInitialize.EndInvoke(result);

                // Zatvaranje splashScreena 
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate { Close(); }));
            };

            // Pokretanje inicjalizacije aplikacije
            result = App.Current.ApplicationInitialize.BeginInvoke(this, initCompleted, null);
        }

        public void SetProgress(double progress)
        {
            // Izmjena progress bara na UI-u
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate { pbLoading.Value = progress; pbCircle.Margin = new Thickness(progress*4.2 +10, 0, 0, 0); }));
        }

        public void SetProgressText(string progressText)
        {
            // Izmjena teksta na UI-u 
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(delegate { txtLoadingInformation.Content = progressText; }));
        }
    }
}
