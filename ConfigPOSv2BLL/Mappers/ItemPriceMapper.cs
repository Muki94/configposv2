﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class ItemPriceMapper
    {

        public static ItemPricesDM MapItemPriceToItemPriceDM(this ITEM_PRICES itemPrice)
        {
            if (itemPrice == null)
                return null;

            ItemPricesDM itemPriceModel = new ItemPricesDM()
            {
                BusinessUnitID = itemPrice.BusinessUnitID,
                ItemID = itemPrice.ItemID,
                Price = itemPrice.Price
            };
            return itemPriceModel;
        }

        public static List<ItemPricesDM> MapItemPriceListToItemPriceDMList(this List<ITEM_PRICES> itemPrices)
        {
            if (itemPrices == null)
                return null;

            List<ItemPricesDM> itemPricesModels = new List<ItemPricesDM>();

            foreach (var itemPrice in itemPricesModels)
            {
                itemPricesModels.Add(new ItemPricesDM()
                {
                    BusinessUnitID = itemPrice.BusinessUnitID,
                    ItemID = itemPrice.ItemID,
                    Price = itemPrice.Price
                });
            }

            return itemPricesModels;
        }
    }
}
