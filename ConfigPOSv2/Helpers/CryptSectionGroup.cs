﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ConfigPOSv2.Helpers
{
    public class CryptSectionGroup
    {
        public CryptSectionGroup()
        {
        
        }


        /// <summary>
        /// Funkcija koja vrši kriptovanje i dekriptovanje određene grupe u config fajlu.
        /// </summary>
        /// <param sectionGroupName="sectionGroupName">ime grupe</param>
        /// 
        public void CryptSectionGroupByName(string sectionGroupName)
        {
            bool encrypt = false;

            Configuration konfiguracija = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSectionGroup  grupa = konfiguracija.GetSectionGroup(sectionGroupName);

            ResetConfigMechanism();

            ConfigPOSv2.Properties.Settings.Default.Save();

            ResetConfigMechanism();

            encrypt = true;

            for (int i = 0; i < grupa.Sections.Count; i++)
                {
                    if ((!(grupa.Sections[i].ElementInformation.IsLocked)) && (!(grupa.Sections[i].SectionInformation.IsLocked)))
                    {
                        if (encrypt && !grupa.Sections[i].SectionInformation.IsProtected)
                        {
                            //this line will encrypt the file
                            grupa.Sections[i].SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                        }

                        if (!encrypt &&
                        grupa.Sections[i].SectionInformation.IsProtected)//encrypt is true so encrypt
                        {
                            //this line will decrypt the file. 
                            grupa.Sections[i].SectionInformation.UnprotectSection();
                        }
                        //re-save the configuration file section
                        grupa.Sections[i].SectionInformation.ForceSave = true;
                        // Save the current configuration

                        //Process.Start("notepad.exe", konfiguracija.FilePath);
                    }
                }

                konfiguracija.Save();

        }


        private void ResetConfigMechanism()
        {
            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic |  BindingFlags.Static).SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName ==
                            "System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic |
                                       BindingFlags.Static)
                .SetValue(null, null);
        }
    }
}
