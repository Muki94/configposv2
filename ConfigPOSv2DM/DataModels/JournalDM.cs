﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class JournalDM
    {
        public int JournalID { get; set; }
       
        public DateTime StartTime { get; set; }
       
        public DateTime? EndTime{ get; set; }
        
        public bool Closed{ get; set; }
    }
}
