﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class PaymentTypesController : ApiController
    {
        PaymentTypesService paymentTypesService = new PaymentTypesService();

        [Route("api/PAYMENT_TYPES/GetByID")]
        [HttpGet]
        public HttpResponseMessage GetByID([FromUri] FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, paymentTypesService.PaymentTypesGetByID(Convert.ToInt32(filter.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [Route("api/PAYMENT_TYPES/GetByName")]
        [HttpGet]
        public HttpResponseMessage GetByName([FromUri] FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, paymentTypesService.PaymentTypesGetByName(filter.idName));                
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,e.Message);
            }
        }
    }
}
