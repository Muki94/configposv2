﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

namespace ConfigPOSv2.Helpers
{
    public class CultureHelper
    {
        /// <summary>
        /// Funkcija za uzimanje izvora ResourceDictionary-a koji ćemo dobiti ovisno o culture-i
        /// </summary>
        /// <returns></returns>
        /// 
        public static ResourceDictionary GetLanguageDictionary()
        {
            //U budućnosti ćemo uzimati iz postavki culture-u postavljenu kao npr. bs, hr, sr, en, de...
            string culture = App.culture; 

            //Ako nije postavljena culture-a ili ako nije ništa postavljeno uzećemo defaultnu sistemsku culture-u
            if (App.culture == string.Empty || culture == "SYSTEM")
            {
                string sysCulture = Thread.CurrentThread.CurrentCulture.ToString();
                //Pošto svaki jezik ima više culture-a uzećemo samo prefiks prije crtice
                culture = sysCulture.Split('-')[0];
            }
            ResourceDictionary dictionary = new ResourceDictionary();

            switch (culture)
            {
                case "bs":
                    dictionary.Source = new Uri("..\\LanguageResources\\ba.xaml", UriKind.Relative);
                    break;
                case "de":
                    dictionary.Source = new Uri("..\\LanguageResources\\de.xaml", UriKind.Relative);
                    break;
                case "es":
                    dictionary.Source = new Uri("..\\LanguageResources\\es.xaml", UriKind.Relative);
                    break;
                //case "hr":
                //    dictionary.Source = new Uri("..\\LanguageResources\\hr.xaml", UriKind.Relative);
                //    break;
                //case "sr":
                //    dictionary.Source = new Uri("..\\LanguageResources\\sr.xaml", UriKind.Relative);
                //    break;
                //case "sl":
                //    dictionary.Source = new Uri("..\\LanguageResources\\sl.xaml", UriKind.Relative);
                //    break;
                //case "mk":
                //    dictionary.Source = new Uri("..\\LanguageResources\\mk.xaml", UriKind.Relative);
                //    break;
                default:
                    dictionary.Source = new Uri("..\\LanguageResources\\en.xaml", UriKind.Relative);
                    break;
            }
            return dictionary;
        }
    }
}
