﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class DataDM
    {
        public List<CategoryDM> ListOfCategory { get; set; }
        public List<SubcategoryDM> ListOfSubcategory { get; set; }
        public List<ImageDM> ListOfImage { get; set; }
        public List<ItemDM> ListOfItem { get; set; }
        public List<ItemGroupDM> ListOfItemGroup { get; set; }
        public List<UserDM> ListOfUser { get; set; }
        public List<UserGroupDM> ListOfUserGroup { get; set; }
    }
}
