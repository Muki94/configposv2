﻿using ConfigPOSv2.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ConfigPOSv2.Helpers
{
    public class StyleHelper
    {
        public static Button CreateKategorijaButton(string naziv)
        {
            Button b = new Button();
            //HorizontalAlignment="Left" Height="50" Margin="10,5,0,0" Style="{DynamicResource KategorijaButtonStyle}" VerticalAlignment="Top" Width="160" Focusable="False" IsDefault="True"
            //b.Style = (Style)Application.Current.Resources["KategorijaButtonStyle"];
            b.Height = 50;
            b.Width = 160;
            b.Margin = new Thickness(10, 5, 0, 0);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Focusable = false;
            b.IsDefault = true;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            if (naziv == string.Empty)
            {
                b.Content = "+ " + (String)App.Current.FindResource("BtnNewCategoryEditModeContent");//b.SetResourceReference(Button.ContentProperty, "BtnNewCategoryEditModeContent");
                b.BorderBrush = new SolidColorBrush(Colors.White);
            }
            else
            {
                b.Content = naziv;
                b.BorderBrush = null;
            }

            return b;
        }
        //Content="Najčešći artikli" HorizontalAlignment="Left" Height="50" Margin="10,5,0,0" Style="{DynamicResource PodkategorijaButtonStyle}" VerticalAlignment="Top" Width="160" Focusable="False"
        public static Button CreatePodkategorijaButton(string naziv)
        {
            Button b = new Button();
            //b.Style = (Style)Application.Current.Resources["PodkategorijaButtonStyle"];
            b.Height = 50;
            b.Width = 160;
            b.Margin = new Thickness(10, 5, 0, 0);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Focusable = false;
            b.IsDefault = true;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            if (naziv == string.Empty)
            {
                b.Content = "+ " + (String)App.Current.FindResource("BtnNewCategoryEditModeContent");//b.SetResourceReference(Button.ContentProperty, "BtnNewCategoryEditModeContent");
                b.BorderBrush = new SolidColorBrush(Colors.White);
            }
            else
            {
                b.Content = naziv;
                b.BorderBrush = null;
            }

            return b;
        }
        //Content="ORANGINA" HorizontalAlignment="Left" Height="120" Margin="10,10,0,0" Style="{DynamicResource ArtikalButtonStyle}" VerticalAlignment="Top" Width="200" Background="{DynamicResource oranginaImage}"/>
        public static Button CreateArtikalButton(string naziv, int artikalId, decimal cijena, byte[] imageData)
        {
            Button b = new Button();

            //b.Tag = cijena;
            //b.Style = (Style)Application.Current.Resources["ButtonStyleArtikal"];
            //b.Style = (Style)Application.Current.Resources["ArtikalButtonStyle"];
            b.Height = 120;
            b.Width = 200;
            b.Margin = new Thickness(10, 10, 0, 0);
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Uid = cijena.ToString("0.00 KM");
            b.Content = naziv;
            b.ToolTip = artikalId;
            b.Tag = cijena;

            try
            {
                if (imageData == null)
                {
                    Brush brush = (Brush)App.Current.FindResource("no-image_item");
                    //brush.ImageSource = (ImageSource)App.Current.FindResource("no-image_item");
                    b.Background = brush;
                }
                else
                {
                    var brush = new ImageBrush();
                    brush.ImageSource = LoadImage(imageData);// new BitmapImage(new Uri(putanjaSlika));
                    b.Background = brush;
                }
            }
            catch
            {
                Brush brush = (Brush)App.Current.FindResource("no-image_item");
                //brush.ImageSource = (ImageSource)App.Current.FindResource("no-image_item");
                b.Background = brush;
            }

            //b.IsCancel = App.prikazCijeneNaArtiklima;

            
            return b;
        }
        //<Button Content="NOVI ARTIKAL" HorizontalAlignment="Left" Height="120" Margin="10,10,0,0" Style="{DynamicResource NoviArtikalButtonStyle}" VerticalAlignment="Top" Width="200" Background="{x:Null}" BorderBrush="{x:Null}"/>

        public static Button CreateNoviArtikalButton()
        {
            Button b = new Button();

            //b.Tag = cijena;
            //b.Style = (Style)Application.Current.Resources["ButtonStyleArtikal"];
            //b.Style = (Style)Application.Current.Resources["NoviArtikalButtonStyle"];
            b.Height = 120;
            b.Width = 200;
            b.Margin = new Thickness(10, 10, 0, 0);
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Content = (String)App.Current.FindResource("BtnNewItemEditModeContent");
            b.Uid = "0";
            b.Background = null;
            b.BorderBrush = null;
            //b.IsCancel = App.prikazCijeneNaArtiklima;

            return b;
        }
        public static Button CreateStavkaNaRacunuButton(int itemID, string naziv, decimal kolicina, decimal cijena)
        {
            Button b = new Button();

            string broj = kolicina.ToString("0.000");
            bool sada = false;
            bool imaDecimala = false;

            //b.ContentStringFormat = cijena.ToString();
           
            b.IsTabStop = true;// za okvir na kolicini

            b.Uid = itemID.ToString();
            b.Height = 65;
            b.Width = 339;
            b.Margin = new Thickness(10, 0, 0, 0);
            
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Content = naziv;

            //b.Command = cijena.ToString();
            //Ako ima priloge
            b.ToolTip = cijena.ToString("0.00");
            //b.AllowDrop = true;

            for (int i = 0; i < broj.Length; i++)
            {
                if (sada)
                    if (broj[i] != '0')
                    {
                        imaDecimala = true;
                    }

                if (broj[i] == ',')
                    sada = true;
            }

            if (imaDecimala)
                b.Tag = kolicina.ToString("0.00").Replace(',', '.');
            else
                b.Tag = kolicina.ToString("0");

            return b;
        }



        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
        //<Button Content="Svi stolovi" HorizontalAlignment="Left" Height="60" Margin="10,5,0,0" Style="{DynamicResource SectorButtonStyle}" 
        //VerticalAlignment="Top" Width="185" Focusable="False" BorderBrush="{x:Null}"/>
        public static Button CreateSectorButton(string naziv)
        {
            Button b = new Button();
            b.Height = 60;
            b.Width = 185;
            b.Margin = new Thickness(10, 5, 0, 0);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.Focusable = false;
            b.IsDefault = true;
            if (naziv == string.Empty)
            {
                b.Content = "+ " + (String)App.Current.FindResource("BtnNewCategoryEditModeContent");
                b.BorderBrush = new SolidColorBrush(Colors.White);
            }
            else
            {
                b.Content = naziv;
                b.BorderBrush = null;
            }

            return b;
        }
        //<Button Content="Moj stol" ContentStringFormat="1" Height="156" Margin="20,10,0,10" Style="{DynamicResource TableButtonStyle}" Width="156" Tag="50,00" BorderBrush="{x:Null}" IsDefault="True" Visibility="Collapsed"/>

        public static Button CreateTableButton(string name, int numberOrder = 0)
        {
            Button b = new Button();
            b.Height = 156;
            b.Width = 156;
            b.Margin = new Thickness(20, 10, 0, 10);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.Focusable = false;
            b.IsDefault = true;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.Cursor = Cursors.Hand;
            b.BorderBrush = null;
            if (name == string.Empty)
            {
                b.ContentStringFormat = "+";
                b.Content = (String)App.Current.FindResource("BtnNewCategoryEditModeContent");
            }
            else
            {
                b.ContentStringFormat = numberOrder.ToString();
                b.Content = name;
            }

            return b;
        }
        //<Button Click="btnShowTableOrder_Click" Content="Narudžba #6410" HorizontalAlignment="Left" Height="65"
        //Margin="10,0,0,0" Style="{DynamicResource OrderOnOrderListButtonStyle}" VerticalAlignment="Top"
        //Width="339" Tag="35,50" IsDefault="true" IsTabStop="false" Padding="0"/>
        public static Button CreateTableRoundButton(int numberOrder, decimal roundAmount)
        {
            Button b = new Button();
            b.Height = 65;
            b.Width = 339;
            b.Margin = new Thickness(10, 0, 0, 0);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.Focusable = false;
            b.Content = (String)App.Current.FindResource("BtnShowOrderContent") + " #" + numberOrder.ToString();
            b.Tag = roundAmount.ToString("0.00");
            b.ContentStringFormat = "#"+numberOrder.ToString();
            return b;
        }
        //<Button Content="Fanta 0,2 l" HorizontalAlignment="Left" Height="65" Margin="10,0,0,0"
        //Style="{DynamicResource ArtikalNaRacunuButtonStyle}" VerticalAlignment="Top" Width="339" Tag="10"
        //IsDefault="true" IsTabStop="false"/>
        public static Button CreateTableRoundItemButton(string itemName, decimal itemQuantity)
        {
            Button b = new Button();
            b.Height = 65;
            b.Width = 339;
            b.Margin = new Thickness(10, 0, 0, 0);
            b.VerticalAlignment = VerticalAlignment.Top;
            b.HorizontalAlignment = HorizontalAlignment.Left;
            b.Focusable = false;
            b.IsTabStop = false;
            b.Content = itemName;
            b.IsEnabled = false;
            b.Tag = OrdersForm.GetItemQuantityWithOrWithoutDecimals(itemQuantity);// itemQuantity.ToString("0.00");

            return b;
        }
    }
}
