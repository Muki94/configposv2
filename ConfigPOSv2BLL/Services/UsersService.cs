﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigPOSv2Data.Entity;
using ConfigPOSv2BLL.Mappers;
using ConfigPOSv2DM.DataModels;

namespace ConfigPOSv2BLL.Services
{
    public class UsersService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih korisnika iz baze podataka.
        /// </summary>
        /// <returns>Lista korisnika</returns>
        public List<UserDM> GetUsersAll()
        {
            OpenConnection();
            List<UserDM> list = entity.stp_USERS_GetAll().ToList().MapUserListToUserDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija se korisiti za login pomoću korisničkog imena i lozinke.
        /// </summary>
        /// <param username="username">Korisničko ime</param>
        /// <param password="password">Lozinka</param>
        /// <returns>Korisnik</returns>
        public UserDM LoginByUsernamePassword(string username, string password)
        {
            OpenConnection();
            UserDM item = entity.stp_USERS_Login_UsernamePassword(username, password).FirstOrDefault().MapUserToUserDM();
            CloseConnection();

            return item;
        }

        /// <summary>
        /// Funkcija se korisiti za login pomoću PIN-a.
        /// </summary>
        /// <param PIN="PIN"> PIN korisnika</param>
        /// <returns>Korisnik</returns>
        public UserDM LoginByPIN(string PIN)
        {
            OpenConnection();
            UserDM item = entity.stp_USERS_Login_PIN(PIN).FirstOrDefault().MapUserToUserDM();
            CloseConnection();

            return item;
        }

        /// <summary>
        /// Funkcija se korisiti za login pomoću PIN-a.
        /// </summary>
        /// <param PIN="PIN"> PIN korisnika</param>
        /// <returns>Korisnik</returns>
        public UserGroupDM GetUserGroupByUserGroupId(int id)
        {
            OpenConnection();
            UserGroupDM item = entity.USER_GROUPS.Where(x => x.UserGroupID == id).FirstOrDefault().MapUserGroupToUserGroupDM();
            CloseConnection();

            return item;
        }
        /// <summary>
        /// Vraća sve grupe korisnika
        /// </summary>
        /// <returns>Lista grupa korisnika</returns>
        public List<UserGroupDM> GetUserGroupAll()
        {
            OpenConnection();
            List<UserGroupDM> list = entity.stp_USER_GROUPS_GetAll().ToList().MapUserGroupListToUserGroupDMList();
            CloseConnection();

            return list;
        }
    }
}
