﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2Data.Entity;

namespace ConfigPOSv2BLL.Mappers
{
    public static class ShiftMapper
    {
        public static ShiftDM MapSHIFTSToShiftDM(this SHIFTS item)
        {
            if (item == null)
                return null;

            ShiftDM itemModel = new ShiftDM()
            {
                ShiftId = item.ShiftID,
                JournalId = item.JournalID,
                OpenTime = item.OpenTime,
                CloseTime = item.CloseTime,
                Closed = item.Closed
            };

            return itemModel;
        }

        public static List<ShiftDM> MapSHIFTSListToShiftDMList(this List<SHIFTS> items)
        {
            if (items == null)
                return null;

            List<ShiftDM> itemModels = new List<ShiftDM>();

            foreach (var item in items)
            {
                itemModels.Add(new ShiftDM()
                {
                    ShiftId = item.ShiftID,
                    JournalId = item.JournalID,
                    CloseTime = item.CloseTime,
                    OpenTime = item.OpenTime,
                    Closed = item.Closed
                });
            }

            return itemModels;
        }
    }
}
