﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class UserShiftsService
    {
        ConfigPOSv2Entities entity;
        UsersService userService = new UsersService();

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke novog korisnika na smjenu
        /// </summary>
        /// <param name="ShiftID">Id smjene</param>
        /// <param name="UserID">Id korisnika</param>

        public int UserShiftInsert(UserShiftDM userShift)
        {
            OpenConnection();
            //provjera dali vec postoji taj user shift
            UserShiftDM userShiftModel = entity.stp_USER_SHIFTS_GetUserShift(userShift.ShiftID, userShift.UserID).FirstOrDefault().MapUSERSHIFTToUserShiftDM();

            if (userShiftModel != null)
                return 0;
             
            int addedUserShiftId = Convert.ToInt32(entity.stp_USER_SHIFTS_Insert(userShift.ShiftID, userShift.UserID).FirstOrDefault().Value);

            CloseConnection();

            return addedUserShiftId;
        }

        /// <summary>
        /// Funkcija koja zatvara korisnikovu smjenu
        /// </summary>
        /// <param name="ShiftID">Id smjene</param>
        /// <param name="UserID">Id korisnika</param>

        public bool UserShiftClose(UserShiftDM userShift)
        {
            //provjera dali je korisnik vec odjavljen
            List<UserShiftDM> notClosedShiftOfUser = GetNotClosedUserShiftsByShiftId(userShift.ShiftID).Where(x => x.UserID == userShift.UserID && x.SignOutTime != null).ToList();
           
            OpenConnection();
            //provjera dali ima nedovrsenih narudjbi korisnika
            List<OrderDM> orderList = entity.ORDERS.Where(x => x.ShiftID == userShift.ShiftID && x.OpenUserID == userShift.UserID && x.Closed == false).ToList().MapORDERSListToOrderDMList();

            if (orderList != null && orderList.Count > 0) {
                CloseConnection();
                return false;
            }

            if (notClosedShiftOfUser != null && notClosedShiftOfUser.Count > 0) {
                CloseConnection();
                return false;
            }
            
            entity.stp_USER_SHIFTS_Close(userShift.ShiftID, userShift.UserID);
            CloseConnection();

            return true;
        }

        /// <summary>
        /// Funkcija koja vraca korisnikovu smjenu na osnovu proslijedjenog id usera i id smjene
        /// </summary>
        /// <param name="shiftId">Id smjene</param>
        /// <param name="userId">Id korisnika</param>
        /// <returns>UserShiftDM</returns>

        public UserShiftDM GetUserShift(int userId, int shiftId)
        {
            OpenConnection();
            UserShiftDM userShift = entity.USER_SHIFTS.Where(x => x.ShiftID == shiftId && x.UserID == userId).FirstOrDefault().MapUSERSHIFTToUserShiftDM();
            CloseConnection();

            return userShift;
        }

        /// <summary>
        /// Funkcija koja vraca sve ne zatvorene smjene korisnika
        /// </summary>
        /// <param name="shiftId">Id smjene</param>
        /// <returns>Lista UserShiftDM</returns>

        public List<UserShiftDM> GetNotClosedUserShiftsByShiftId(int shiftId)
        {
            OpenConnection();
            List<UserShiftDM> userShifts = entity.USER_SHIFTS.Where(x => x.ShiftID == shiftId && x.SignOutTime == null).ToList().MapUSERSHIFTListToUserShiftDMList();
            CloseConnection();

            return userShifts;
        }
    }
}
