﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ConfigPOSv2.Helpers
{
    public class TeamViewerHelper
    {
        public TeamViewerHelper()
        {

        }


        public string LocateEXE(String filename)
        {
            string[] folders = Directory.GetFileSystemEntries("C:\\");

            foreach (String folder in folders)
            {
                if (File.Exists(folder + filename))
                {
                    return folder + filename;
                }
                else if (File.Exists(folder + "\\" + filename))
                {
                    return folder + "\\" + filename;
                }
            }

            return String.Empty;
        }

        public void RunApplication(string exe)
        {
            Process app = new Process();
            app.StartInfo.FileName = exe;
            app.Start();
        }

        public void DownloadTeamViewer(string link)
        {
            Process.Start(link);
        }

    }
}
