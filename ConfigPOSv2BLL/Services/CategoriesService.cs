﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigPOSv2BLL.Services
{
    public class CategoriesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novoj kategoriji
        /// </summary>
        /// <param name="Name">Naziv kategorije</param>
        /// <param name="Active">Aktivna kategorija</param>
        public int CategoryInsert(CategoryDM category)
        {
            OpenConnection();
            int addedCategoryId = Convert.ToInt32(entity.stp_CATEGORIES_Insert(category.Name, category.Active).FirstOrDefault().Value);
            CloseConnection();

            return addedCategoryId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o kategoriji na osnovu proslijeđenog ID kategorije
        /// </summary>
        /// <param name="CategoryID">ID kategorije</param>
        /// <param name="Name">Naziv kategorije</param>
        /// <param name="Active">Aktivna kategorija</param>
        public void CategoryUpdate(CategoryDM category)
        {
            OpenConnection();
            entity.stp_CATEGORIES_Update(category.CategoryID, category.Name, category.Active);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja briše podatke o kategoriji na osnovu proslijeđenog ID kategorije
        /// </summary>
        /// <param name="CategoryID">ID kategorije</param>
        public void CategoryDelete(int CategoryID)
        {
            OpenConnection();
            entity.stp_CATEGORIES_Delete(CategoryID);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih kategorija
        /// </summary>
        /// <returns>Lista kategorija</returns>
        public List<CategoryDM> CategoriesGetAll()
        {
            OpenConnection();
            List<CategoryDM> list = entity.stp_CATEGORIES_GetAll().ToList().MapCategoryListToCategoryDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća kategoriju na osnovu prosljeđenog ID kategorije
        /// </summary>
        /// <param name="CategoryID">ID kategorije</param>
        /// <returns>Kategorija</returns>
        public CategoryDM CategoriesGetByID(int CategoryID)
        {
            OpenConnection();
            CategoryDM category = entity.stp_CATEGORIES_GetByID(CategoryID).FirstOrDefault().MapCategoryToCategoryDM();
            CloseConnection();

            return category;
        }
    }
}
