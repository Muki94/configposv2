﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class StationsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public StationsDM StationsGetByUserID(int UserID)
        {
            OpenConnection();
            StationsDM Station = entity.stp_STATIONS_GetStationByUserID(UserID).FirstOrDefault().MapStationToStationsDM();
            CloseConnection();
            return Station;
        }

    }
}
