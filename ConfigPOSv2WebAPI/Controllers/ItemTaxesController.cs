﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class ItemTaxesController : ApiController
    {
        ItemTaxesService itemTaxesService = new ItemTaxesService();

        [Route("api/ITEM_TAXES/GetByItemID")]
        [HttpGet]
        public HttpResponseMessage GetByItemID([FromUri] FilterAttributesDM filter)
        {
            try
            {
                List<ItemTaxesViewDM> itemTaxesList = itemTaxesService.ItemTaxesGetByItemID(Convert.ToInt32(filter.idName));
                return Request.CreateResponse(HttpStatusCode.OK, itemTaxesList);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
