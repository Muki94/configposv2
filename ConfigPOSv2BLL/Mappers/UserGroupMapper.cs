﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class UserGroupMapper
    {
        public static UserGroupDM MapUserGroupToUserGroupDM(this USER_GROUPS userGroup) 
        {
            if (userGroup == null)
                return null;   
            
            UserGroupDM userGroupModel = new UserGroupDM() {
                UserGroupID = userGroup.UserGroupID,
                Name = userGroup.Name,
                Description = userGroup.Description
            };

            return userGroupModel;
        }

        public static List<UserGroupDM> MapUserGroupListToUserGroupDMList(this List<USER_GROUPS> userGroups)
        {
            if (userGroups == null)
                return null;

            List<UserGroupDM> userGroupModels = new List<UserGroupDM>();

            foreach (var userGroup in userGroups)
            {
                userGroupModels.Add(new UserGroupDM()
                {
                    UserGroupID = userGroup.UserGroupID,
                    Name = userGroup.Name,
                    Description = userGroup.Description
                });
            }

            return userGroupModels;
        }
    }
}
