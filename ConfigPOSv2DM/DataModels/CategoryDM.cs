﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class CategoryDM
    {
        public int CategoryID { get; set; }
        
        public string Name { get; set; }
        
        public bool Active { get; set; }

        public List<SubcategoryDM> Subcategories { get; set; }
    }
}
