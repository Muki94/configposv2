﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.Forms
{
    public static class ItemForm
    {
        public static ItemDM AddEditArticle(int itemId, string name, decimal price, int subcategoryId, bool active, bool dailyItem, bool favorite, string code = null, string description = null,
                                            string refCode = null, int? imageId = null, int? itemGroupId = null)
        {
            ItemDM itemModel = new ItemDM()
            {
                ItemID = itemId,
                Code = code,
                Name = name,
                Description = description,
                RefCode = refCode,
                Price = price,
                SubcategoryID = subcategoryId,
                ImageID = imageId,
                ItemGroupID = itemGroupId,
                Active = active,
                DailyItem = dailyItem,
                Favorite = favorite
            };

            if (itemId != 0)
                ItemsCommunication.ItemsUpdate(itemModel);
            else
                itemModel.ItemID = ItemsCommunication.ItemsInsert(itemModel);

            return itemModel;
        }

        public static ItemDM ItemGetByID(int ItemID)
        {            
            return ItemsCommunication.ItemsGetByID(ItemID);         
        }
    }
}
