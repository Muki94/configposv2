//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConfigPOSv2Data.Entity
{
    using System;
    
    public partial class ITEM_TAXES_GetByItemID_View
    {
        public int ItemID { get; set; }
        public int TaxID { get; set; }
        public decimal Rate { get; set; }
        public decimal RecalculatedRate { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public System.DateTime ValidFrom { get; set; }
        public System.DateTime ValidTo { get; set; }
    }
}
