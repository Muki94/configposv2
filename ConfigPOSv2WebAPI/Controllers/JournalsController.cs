﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class JournalsController : ApiController
    {
        JournalsService journalService = new JournalsService();

        // GET ALL
        [Route("api/JOURNALS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, journalService.JournalGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET LAST
        [Route("api/JOURNALS/GetLast")]
        [HttpGet]
        public HttpResponseMessage GetLast([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, journalService.JournalGetLast());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/JOURNALS/Insert")]
        [HttpPost]
        public HttpResponseMessage JournalInsert()
        {
            try
            {
                int addedJournalId = journalService.JournalInsert();
                return Request.CreateResponse(HttpStatusCode.OK, addedJournalId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/JOURNALS/Update")]
        [HttpPut]
        public HttpResponseMessage JournalUpdate([FromBody] JournalDM journal)
        {
            try
            {
                journalService.JournalUpdate(journal);
                return Request.CreateResponse(HttpStatusCode.OK, journal);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // CLOSE
        [Route("api/JOURNALS/Close")]
        [HttpPut]
        public HttpResponseMessage JournalClose([FromBody] int journalId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, journalService.JournalClose(journalId));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
