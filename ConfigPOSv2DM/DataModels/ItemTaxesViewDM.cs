﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ItemTaxesViewDM
    {
        public int ItemID { get; set; }
        public int TaxID { get; set; }
        public decimal Rate { get; set; }
        public decimal RecalculatedRate { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public System.DateTime ValidFrom { get; set; }
        public System.DateTime ValidTo { get; set; }
    }


}
