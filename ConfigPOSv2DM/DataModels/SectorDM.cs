﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class SectorDM
    {
        public int SectorID { get; set; }

        public string Name { get; set; }

        public int NumberOrder { get; set; }

        public byte[] Layout { get; set; }

        public bool Deleted { get; set; }
    }
}
