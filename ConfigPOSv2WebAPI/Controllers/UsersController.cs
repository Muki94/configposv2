﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class UsersController : ApiController
    {
        UsersService users = new UsersService();

        // GET ALL
        [Route("api/USERS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, users.GetUsersAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY USERNAME AND PASS
        [Route("api/USERS/GetByUsernamePassword")]
        [HttpGet]
        public HttpResponseMessage GetByUsernamePassword([FromUri]FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, users.LoginByUsernamePassword(filter.username, filter.password));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY PIN
        [Route("api/USERS/GetByPin")]
        [HttpGet]
        public HttpResponseMessage GetByPin([FromUri]FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, users.LoginByPIN(filter.password));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
