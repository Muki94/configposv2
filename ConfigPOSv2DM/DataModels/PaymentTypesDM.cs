﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class PaymentTypesDM
    {
        public int PaymentTypeID { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Password { get; set; }
        public bool Tips { get; set; }
        public bool Deleted { get; set; }
        public decimal Amount { get; set; }
    }
}
