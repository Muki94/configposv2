﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;


namespace ConfigPOSv2WebAPI.Controllers
{
    [Authorize]
    public class SubcategoriesController : ApiController
    {
        SubcategoriesService Subcategories = new SubcategoriesService();

        // INSERT
        [Route("api/SUBCATEGORIES/Insert")]
        [HttpPost]
        public HttpResponseMessage SubcategoryInsert([FromBody]SubcategoryDM subcategory)
        {
            try
            {
                int addedItemId = Subcategories.SubcategoryInsert(subcategory);
                return Request.CreateResponse(HttpStatusCode.OK, addedItemId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/SUBCATEGORIES/Update")]
        [HttpPut]
        public HttpResponseMessage SubcategoryUpdate([FromBody]SubcategoryDM subcategory)
        {
            try
            {
                Subcategories.SubcategoryUpdate(subcategory);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // DELETE
        [Route("api/SUBCATEGORIES/Delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage SubcategoryDelete(int id)
        {
            try
            {
                Subcategories.SubcategoryDelete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET ALL 
        [Route("api/SUBCATEGORIES/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Subcategories.SubcategoriesGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET ALL BY CATEGORYID
        [Route("api/SUBCATEGORIES/GetAllByCategoryId")]
        [HttpGet]
        public HttpResponseMessage GetAllByCategoryId([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Subcategories.SubcategoriesGetByCategoryID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET SINGLE
        [Route("api/SUBCATEGORIES/GetById")]
        [HttpGet]
        public HttpResponseMessage SubcategoryGetById([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Subcategories.SubcategoriesGetByID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
	}
}