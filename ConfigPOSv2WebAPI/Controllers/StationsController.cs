﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class StationsController : ApiController
    {
        StationsService stationService = new StationsService();

        [Route("api/STATIONS/GetByUserID")]
        [HttpGet]
        public HttpResponseMessage GetByUserID([FromUri] FilterAttributesDM filter)
        {
            try 
	        {	        
		        StationsDM station = stationService.StationsGetByUserID(Convert.ToInt32(filter.idName));
                return Request.CreateResponse(HttpStatusCode.OK,station);
	        }
	        catch (Exception e)
	        {
		        return Request.CreateResponse(HttpStatusCode.BadRequest,e.Message);
	        }
        }

    }
}
