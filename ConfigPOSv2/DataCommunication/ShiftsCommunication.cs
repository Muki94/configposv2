﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;

namespace ConfigPOSv2.DataCommunication
{
    public class ShiftsCommunication
    {
        private static ShiftsService shiftsService = new ShiftsService();

        public static int ShiftsInsert(int journalId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int>("api/SHIFTS/Insert");
            else
                return shiftsService.ShiftInsert();
        }

        public static void ShiftsUpdate(ShiftDM shift)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<ShiftDM, ShiftDM>("api/SHIFTS/Update", shift);
            else
                shiftsService.ShiftUpdate(shift);
        }

        public static bool ShiftsClose()
        {
            if (App.usingWebApi)
                return WebApiHelper.Put<bool>("api/SHIFTS/Close");
            else
                return shiftsService.ShiftClose();
        }

        public static List<ShiftDM> ShiftsGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<ShiftDM>>("api/SHIFTS/GetAll");
            else
                return shiftsService.ShiftGetAll();
        }

        public static ShiftDM ShiftsGetLast()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<ShiftDM>("api/SHIFTS/GetLast");
            else
                return shiftsService.ShiftGetLast();
        }
    }	
}
