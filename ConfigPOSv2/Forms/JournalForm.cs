﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.Forms
{
    public static class JournalForm
    {
        #region dodavanje dnevnika
        //Provjera dali ima nezatvorenih dnevnika
        public static bool IsJournalOpen()
        {
            JournalDM journal = JournalsCommunication.JournalsGetLast();

            if (journal == null)
                return false;
            else if (journal.Closed)
                return false;

            return true;
        }

        //Dodavanje/Editovanje dnevnika
        public static bool AddJournal()
        {
            int id = JournalsCommunication.JournalsInsert();

            if (id != 0)
                return true;

            return false;
        }
        #endregion

        #region zatvaranje dnevnika
        //Zatvaranje dnevnika
        public static bool CloseJournal()
        {
            return JournalsCommunication.JournalsClose(JournalsCommunication.JournalsGetLast().JournalID);
            //napraviti izvjestaj 
        }
        #endregion
    }
}
