﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
	/// <summary>
	/// Interaction logic for frmSubcategoriesAdd.xaml
	/// </summary>
	public partial class frmSubcategoriesAdd : Window
	{
		public frmSubcategoriesAdd()
		{
			this.InitializeComponent();

            LoadCategories();
		}

        /// <summary>
        /// Zatvaranje forme
        /// </summary>
		private void btnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            this.DialogResult = false;
			this.Close();
		}


        /// <summary>
        /// Funkcija koja služi za učitavanje svih kategorija u combobox kategorija
        /// </summary>
        public void LoadCategories()
        {
            cmbCategories.ItemsSource = CategoriesCommunication.CategoriesGetAll(); ;
            cmbCategories.DisplayMemberPath = "Name";
            cmbCategories.SelectedValuePath = "CategoryID";
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivna"
        /// </summary>
        public void btnActiv_Click(object sender, RoutedEventArgs e)
        {
            btnActiv.IsDefault = !btnActiv.IsDefault;
        }

        /// <summary>
        /// Funkcija koja sprema novu podkategoriju u bazu podataka
        /// </summary>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = SubcategoryForm.AddEditSubcategory(txtName.Text, !btnActiv.IsDefault, Int32.Parse(cmbCategories.SelectedValue.ToString())) != null ? true : false;

            if (this.DialogResult == false)
                return;

            this.Close();
        }
	}
}