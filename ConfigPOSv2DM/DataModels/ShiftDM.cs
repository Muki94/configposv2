﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ShiftDM
    {
        public int ShiftId { get; set; }
        
        public int JournalId { get; set; }
        
        public DateTime OpenTime { get; set; }
        
        public DateTime? CloseTime { get; set; }
        
        public bool Closed { get; set; }
    }
}
