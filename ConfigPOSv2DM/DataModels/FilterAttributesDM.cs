﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    [Serializable]
    public class FilterAttributesDM
    {
        public string idName { get; set; }

        public int userId { get; set; }

        public string username { get; set; }

        public string password { get; set; }
    }
}
