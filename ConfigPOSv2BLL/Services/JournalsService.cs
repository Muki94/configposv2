﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class JournalsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novom dnevniku
        /// </summary>
        /// <returns>Id dodatog dnevnika</returns>
        
        public int JournalInsert()
        {
            OpenConnection();
            int addedJournalId = Convert.ToInt32(entity.stp_JOURNALS_Insert().FirstOrDefault().Value);
            CloseConnection();

            return addedJournalId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o dnevniku na osnovu proslijeđenog ID dnevnika
        /// </summary>
        /// <param name="StartTime">Datum i vrijeme početka</param>
        /// <param name="EndTime">Datum i vrijeme kraja</param>
        /// <param name="Closed">Dali je zatvoren dan</param>

        public void JournalUpdate(JournalDM journal)
        {
            OpenConnection();
            entity.stp_JOURNALS_Update(journal.JournalID,journal.StartTime, journal.EndTime);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja radi update podatka closed u dnevniku
        /// </summary>
        /// <param name="journalId">Id dnevnika</param>

        public bool JournalClose(int journalId)
        {
            OpenConnection();
            if (entity.stp_SHIFTS_GetLast().FirstOrDefault().Closed)
            {
                entity.stp_JOURNALS_Close(journalId);
                CloseConnection();
                return true;
            }
            else
            {
                CloseConnection();
                return false;
            }
        }

        /// <summary>
        /// Funkcija koja vraća listu svih dnevnika
        /// </summary>
        /// <returns>Lista dnevnika</returns>
       
        public List<JournalDM> JournalGetAll()
        {
            OpenConnection();
            List<JournalDM> list = entity.stp_JOURNALS_GetAll().ToList().MapJOURNALSListToJournalDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća zadnji dnevnik
        /// </summary>
        /// <returns>Lista dnevnika</returns>
        
        public JournalDM JournalGetLast()
        {
            OpenConnection();
            JournalDM journal = entity.stp_JOURNALS_GetLast().FirstOrDefault().MapJOURNALSToJournalDM();
            CloseConnection();

            return journal;
        }
    }
}
