﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
	/// <summary>
	/// Interaction logic for frmCategoriesAdd.xaml
	/// </summary>
	public partial class frmCategoriesAdd : Window
	{
		public frmCategoriesAdd()
		{
			this.InitializeComponent();
			
			// Insert code required on object creation below this point.
		}

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivna"
        /// </summary>
        public void btnActiv_Click(object sender, RoutedEventArgs e)
        {
            btnActiv.IsDefault = !btnActiv.IsDefault;
        }

        /// <summary>
        /// Zatvaranje forme
        /// </summary>
		private void btnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            this.DialogResult = false;
			this.Close();
		}

        /// <summary>
        /// Funkcija koja sprema novu kategoriju u bazu podataka
        /// </summary>
        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.DialogResult = CategoryForm.AddEditCategory(txtName.Text, !btnActiv.IsDefault) != null ? true : false;

            if (this.DialogResult == false)
                return;

            this.Close();
        }
	}
}