﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ItemPricesDM
    {
        public int BusinessUnitID { get; set; }
        public int ItemID { get; set; }
        public decimal Price { get; set; }
    }
}
