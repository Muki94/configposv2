﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for frmTablesAdd.xaml
    /// </summary>
    public partial class frmTablesAdd : Window
    {
        private int m_tableID = 0;
        public bool tableActive = false;
        public bool isEdit = false;
        public TableDM addedEditedTable = new TableDM();
        public frmTablesAdd(int sectorID)
        {
            InitializeComponent();
            LoadSectors();
            if (sectorID != 0)
            {
                cmbSectors.SelectedValue = sectorID;
            }
        }

        public frmTablesAdd(int tableID, bool edit)
        {
            InitializeComponent();
            m_tableID = tableID;
            LoadSectors();
            addedEditedTable = TablesForm.TablesGetByID(m_tableID);// TablesCommunication.TablesGetByID(m_tableID);

            if (addedEditedTable != null)
            {
                isEdit = true;
                btnDeleteTable.IsEnabled = true;
                txtTableName.Text = addedEditedTable.Name;
                cmbSectors.SelectedValue = addedEditedTable.SectorID.ToString();
                chbActive.IsDefault = addedEditedTable.Deleted;
                for (int i = 0; i < wrapTableShapes.Children.Count; i++)
                {
                    Button b1 = (Button)wrapTableShapes.Children[i];
                    if (b1.Uid == addedEditedTable.Type.ToString())
                    {
                        b1.IsDefault = false;
                        break;
                    }
                }
            }
        }
        public void LoadSectors()
        {
            List<SectorDM> list = new List<SectorDM>();
            list.Add(new SectorDM { SectorID = 0, Name = "" });
            list.AddRange(SectorsCommunication.SectorsGetAll());

            cmbSectors.ItemsSource = list;
            cmbSectors.DisplayMemberPath = "Name";
            cmbSectors.SelectedValuePath = "SectorID";
        }
        private void btnShapeType_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            for (int i = 0; i < wrapTableShapes.Children.Count; i++)
            {
                Button b1 = (Button)wrapTableShapes.Children[i];
                b1.IsDefault = true;
            }
            b.IsDefault = false;
        }

        private void chbAktive_Click(object sender, RoutedEventArgs e)
        {
            chbActive.IsDefault = chbActive.IsDefault == true ? false : true;
        }

        private void btnDeleteTable_Click(object sender, RoutedEventArgs e)
        {
            if (m_tableID != 0)
            {
                frmMessageBox frm = new frmMessageBox("MsgBoxTitleDeleteTable", frmMessageBox.MsgButtons.YesNo);
                if (frm.ShowDialog() == true)
                {
                    TablesForm.TablesDelete(m_tableID);
                    tableActive = false;
                    DialogResult = true;
                    this.Close();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool notValid = false;

                //obicna validacija elemenata koji se moraju popuniti
                if (txtTableName == null || txtTableName.Text == String.Empty)
                {
                    notValid = true;
                }
                if (cmbSectors == null || cmbSectors.Text == String.Empty)
                {
                    notValid = true;
                }

                //prekid u slucaju da forma nije validna 
                if (notValid)
                    return;

                bool deleted = chbActive.IsDefault;
                int tableNumber = addedEditedTable != null ? addedEditedTable.NumberOrder : 0;
                int tableType = 0;
                for (int i = 0; i < wrapTableShapes.Children.Count; i++)
                {
                    Button b1 = (Button)wrapTableShapes.Children[i];
                    if (b1.IsDefault == false)
                    {
                        tableType = Convert.ToInt32(b1.Uid);
                        break;
                    }
                }

                //dodavanje editovanje stola
                this.addedEditedTable = TablesForm.AddEditTable(m_tableID, txtTableName.Text, tableNumber, Int32.Parse(cmbSectors.SelectedValue.ToString()), tableType, 0, 0, 0, deleted);
                tableActive = !deleted;
                DialogResult = true;
                this.Close();
            }
            catch (Exception)
            {

            }
           
        }
    }
}
