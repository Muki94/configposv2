﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class OrdersController : ApiController
    {
        OrdersService orderService = new OrdersService();

        // GET ALL
        [Route("api/ORDERS/GetAllByShiftId")]
        [HttpGet]
        public HttpResponseMessage GetAllByShiftId([FromUri]FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, orderService.OrderGetAllByShiftId(Int32.Parse(filter.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET ALL
        [Route("api/ORDERS/GetAllByShiftIdAndUserId")]
        [HttpGet]
        public HttpResponseMessage GetAllByShiftIdAndUserId([FromUri]FilterAttributesDM filter)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, orderService.OrderGetAllByShiftIdAndUserId(Int32.Parse(filter.idName), filter.userId));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/ORDERS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertOrder([FromBody]OrderDM item)
        {
            try
            {
                int addedId = orderService.OrderInsert(item);
                return Request.CreateResponse(HttpStatusCode.OK, addedId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        // UPDATE
        [Route("api/ORDERS/UpdateClosed")]
        [HttpPut]
        public HttpResponseMessage UpdateClosed([FromBody]OrderDM item)
        {
            try
            {
                orderService.OrdersUpdateClosed(item.OrderID, item.OpenUserID);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
