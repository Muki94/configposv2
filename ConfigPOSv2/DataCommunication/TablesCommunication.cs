﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class TablesCommunication
    {
        private static TablesService tableService = new TablesService();

        public static int TablesInsert(TableDM table)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, TableDM>("api/TABLES/Insert", table);
            else
                return tableService.TableInsert(table);
        }

        public static void TablesUpdate(TableDM table)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<TableDM, TableDM>("api/TABLES/Update", table);
            else
                tableService.TableUpdate(table);
        }

        public static void TablesDelete(int tableID)
        {
            if (App.usingWebApi)
                WebApiHelper.Delete<TableDM>("api/TABLES/Delete", tableID);
            else
                tableService.TableDelete(tableID);
        }

        public static List<TableDM> TablesGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<TableDM>>("api/TABLES/GetAll");
            else
                return tableService.TablesGetAll();
        }

        public static TableDM TablesGetByID(int tableID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<TableDM>("api/TABLES/GetById", new FilterAttributesDM() { idName = tableID.ToString() });
            else
                return tableService.TablesGetByID(tableID);
        }
        public static List<TableDM> TablesGetBySectorID(int sectorID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<TableDM>>("api/TABLES/GetBySectorId", new FilterAttributesDM() { idName = sectorID.ToString() });
            else
                return tableService.TablesGetBySectorID(sectorID);
        }
        public static int TablesGetNextNumberOrderBySectorID(int sectorID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<int>("api/TABLES/GetTableNextNumberOrderBySectorID", new FilterAttributesDM() { idName = sectorID.ToString() });
            else
                return tableService.TablesGetNextNumberOrderBySectorID(sectorID);
        }
    }
}
