﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class PaymentsController : ApiController
    {
        PaymentsService paymentsService = new PaymentsService();

        [Route("api/PAYMENTS/Insert")]
        [HttpPost]
        public HttpResponseMessage Insert([FromBody] PaymentsDM payment)
        {
            try
            {
                int addedPaymentID = paymentsService.PaymentsInsert(payment);
                return Request.CreateResponse(HttpStatusCode.OK,addedPaymentID);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,e.Message);
            }
        }
    }
}
