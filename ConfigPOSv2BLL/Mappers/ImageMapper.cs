﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class ImageMapper
    {
        public static ImageDM MapImageToImageDM(this IMAGES image) 
        {
            if (image == null)
                return null;

            ImageDM imageModel = new ImageDM()
            {
                ImageID = image.ImageID,
                Data = image.Data
            };

            return imageModel;
        }

        public static List<ImageDM> MapImageListToImageDMList(this List<IMAGES> images)
        {
            if (images == null)
                return null;

            List<ImageDM> imageModels = new List<ImageDM>();

            foreach (var image in images)
            {
                imageModels.Add(new ImageDM()
                {
                    ImageID = image.ImageID,
                    Data = image.Data
                });
            }

            return imageModels;
        }
    }
}
