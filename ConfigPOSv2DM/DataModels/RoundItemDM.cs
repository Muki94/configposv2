﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class RoundItemDM
    {
        public int RoundItemID { get; set; }

        public int RoundID { get; set; }

        public int ItemID { get; set; }

        public decimal Quantity { get; set; }

        public Nullable<int> ProductionUnitID { get; set; }
    }
}
