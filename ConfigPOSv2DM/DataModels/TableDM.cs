﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class TableDM
    {
        public int TableID { get; set; }

        public string Name { get; set; }

        public int NumberOrder { get; set; }

        public int SectorID { get; set; }
        
        public int Type { get; set; }
        
        public int GraphX { get; set; }
        
        public int GraphY { get; set; }
        
        public int Rotation { get; set; }

        public bool Deleted { get; set; }
    }
}
