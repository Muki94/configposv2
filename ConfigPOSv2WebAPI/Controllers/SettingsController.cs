﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class SettingsController : ApiController
    {
        SettingsService settings = new SettingsService();

        // GET ALL
        [Route("api/SETTINGS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, settings.GetSettingsAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET SINGLE
        [Route("api/SETTINGS/GetByName")]
        [HttpGet]
        public HttpResponseMessage GetByName([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, settings.GetSettingsByName(filters.idName));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
