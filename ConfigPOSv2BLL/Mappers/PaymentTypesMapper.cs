﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class PaymentTypesMapper
    {
        public static PaymentTypesDM MapPaymentTypeToPaymentTypeDM(this PAYMENT_TYPES paymentType)
        {
            if (paymentType == null)
                return null;

            PaymentTypesDM paymentTypeModel = new PaymentTypesDM()
            {
                PaymentTypeID = paymentType.PaymentTypeID,
                Name = paymentType.Name,
                Alias = paymentType.Alias,
                Password = paymentType.Password,
                Tips = paymentType.Tips,
                Deleted = paymentType.Deleted
            };
            return paymentTypeModel;
        }

        public static List<PaymentTypesDM> MapPaymentTypesListToPaymentTypesDMList(this List<PAYMENT_TYPES> paymentTypes)
        {
            if (paymentTypes == null)
                return null;

            List<PaymentTypesDM> paymentTypesModels = new List<PaymentTypesDM>();

            foreach (var paymentType in paymentTypes)
            {
                paymentTypesModels.Add(new PaymentTypesDM()
                {
                    PaymentTypeID = paymentType.PaymentTypeID,
                    Name = paymentType.Name,
                    Alias = paymentType.Alias,
                    Password = paymentType.Password,
                    Tips = paymentType.Tips,
                    Deleted = paymentType.Deleted
                });
            }

            return paymentTypesModels;
        }
    }
}
