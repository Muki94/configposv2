﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.IO;


namespace ConfigPOSv2.Helpers
{
    public class CryptSection
    {

        public CryptSection()
        {
        
        }


        /// <summary>
        /// Funkcija koja vrši kriptovanje i dekriptovanje određene sekcije u config fajlu.
        /// </summary>
        /// <param sectionName="sectionName">ime sekcije</param>
        /// 
        public void CryptSectionByName(string sectionName)
        {
            bool encrypt = false;

            Configuration konfiguracija = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection sekcija = konfiguracija.GetSection(sectionName);

            ResetConfigMechanism();

            ConfigPOSv2.Properties.Settings.Default.Save();

            ResetConfigMechanism();

            encrypt = true;

            if ((!(sekcija.ElementInformation.IsLocked)))
            { 
                if (encrypt && !sekcija.SectionInformation.IsProtected)
                {
                    //this line will encrypt the file
                    sekcija.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }

                if (!encrypt && sekcija.SectionInformation.IsProtected)//encrypt is true so encrypt
                {
                    //this line will decrypt the file. 
                    sekcija.SectionInformation.UnprotectSection();
                }

                sekcija.SectionInformation.ForceSave = true;
                // Save the current configuration

                //Process.Start("notepad.exe", konfiguracija.FilePath);
                            
            }

            konfiguracija.Save();


        }
        private void ResetConfigMechanism()
        {
            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic |
                                         BindingFlags.Static)
                .SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic |
                                            BindingFlags.Static)
                .SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName ==
                            "System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic |
                                       BindingFlags.Static)
                .SetValue(null, null);
        }

    }
}
