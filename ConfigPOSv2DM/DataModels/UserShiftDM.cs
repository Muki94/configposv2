﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class UserShiftDM
    {
        public int UserShiftID { get; set; }
        
        public int ShiftID { get; set; }
        
        public int UserID { get; set; }
        
        public DateTime SignInTime { get; set; }
        
        public DateTime? SignOutTime { get; set; }
    }
}
