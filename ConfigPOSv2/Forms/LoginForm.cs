﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ConfigPOSv2.Forms
{
    public class LoginForm
    {
       private static TeamViewerHelper teamV = new TeamViewerHelper();

        public static bool Login(string password, string username = "")
        {
            var passwordHash = Hash.GetHash(password, Hash.HashType.SHA512);
            bool loginSucces = false;
            if (username == string.Empty)
            {
                loginSucces = UsersCommunication.UsersLogin(passwordHash); //WebApiHelper.SetToken(passwordHash);
            }
            else
            {
                loginSucces = UsersCommunication.UsersLogin(passwordHash, username);  //WebApiHelper.SetToken(passwordHash, txtUserName.Text);
            }
            return loginSucces;
        }

        public static void StartTeamViewer()
        {
            App.TvExePath = teamV.LocateEXE(App.fileName);

            if (App.TvExePath != string.Empty)
            {
                teamV.RunApplication(App.TvExePath);
            }
            else
            {
                MessageBoxResult pitanje = MessageBox.Show("TeamViewer nije instaliran, da li želite instalirati aplikaciju?", "TeamViewer", MessageBoxButton.YesNo, MessageBoxImage.Question);

                switch (pitanje)
                {
                    case MessageBoxResult.Yes: teamV.DownloadTeamViewer(App.link_TeamViewer); break;
                    default: break;
                }
            }
        }

        public static void StartECard()
        {
            try
            {
                var prc = Application.ResourceAssembly.Location.ToString().Replace("ConfigPOSv2.exe", @"eCard\eCardShop.exe");
                Process[] processList = Process.GetProcessesByName("eCardShop");

                //if (processList.Length > 0)
                //{
                //    btnECard.IsDefault = true;
                //}
                //else
                //{
                //    Process.Start(prc);
                //}
                if (processList.Length <= 0)
                {
                    Process.Start(prc);
                }
            }
            catch
            {
                //dodati log zapis
            }
        }
    }
}
