﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for frmMessageBox.xaml
    /// </summary>
    public partial class frmMessageBox : Window
    {
        //public enum MsgType { Info, Question, Warning, Error, Exclamation };
        public enum MsgButtons { OK, YesNo, ZatvoriOdustani }
        //public enum Results { OK, Yes, No }
        public frmMessageBox()
        {
            InitializeComponent();
        }

        public frmMessageBox(string MessageBoxTitleResourceKey, MsgButtons MsgBoxButtons)
        {
            InitializeComponent();
            lblTitle.SetResourceReference(Label.ContentProperty, MessageBoxTitleResourceKey);

            if (MsgBoxButtons == MsgButtons.OK)
            {
                panelOK.Visibility = Visibility.Visible;
                panelYesNo.Visibility = Visibility.Collapsed;
            }
            else if (MsgBoxButtons == MsgButtons.YesNo)
            {
                panelOK.Visibility = Visibility.Collapsed;
                panelYesNo.Visibility = Visibility.Visible;
            }
            else if (MsgBoxButtons == MsgButtons.ZatvoriOdustani)
            {
                btnYes.SetResourceReference(Button.ContentProperty, "MsgBoxBtnClose");
                btnNo.SetResourceReference(Button.ContentProperty, "MsgBoxBtnQuit");
                panelOK.Visibility = Visibility.Collapsed;
                panelYesNo.Visibility = Visibility.Visible;
            }
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
