﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class BillsCommunication
    {
        private static BillsService BillsService = new BillsService();

        public static int BillsInsert(BillsDM Bill)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, BillsDM>("api/BILLS/Insert", Bill);
            else
                return BillsService.BillsInsert(Bill);
        }

        public static int BillsGetLastNumber()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<int>("api/BILLS/GetLastNumber");
            else
                return BillsService.BillsGetLastNumber();
        }
    }
}
