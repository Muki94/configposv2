﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigPOSv2DM.DataModels
{
    public class SubcategoryDM
    {
        public int SubcategoryID { get; set; }
        
        public string Name { get; set; }
        
        public int CategoryID { get; set; }
        
        public bool Active { get; set; }

        public List<ItemDM> Items { get; set; }
    }
}
