﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class SectorsCommunication
    {
        private static SectorsService sectorService = new SectorsService();

        public static int SectorsInsert(SectorDM sector)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, SectorDM>("api/SECTORS/Insert", sector);
            else
                return sectorService.SectorInsert(sector);
        }

        public static void SectorsUpdate(SectorDM sector)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<SectorDM, SectorDM>("api/SECTORS/Update", sector);
            else
                sectorService.SectorUpdate(sector);
        }

        public static void SectorsDelete(int sectorID)
        {
            if (App.usingWebApi)
                WebApiHelper.Delete<SectorDM>("api/SECTORS/Delete", sectorID);
            else
                sectorService.SectorDelete(sectorID);
        }

        public static List<SectorDM> SectorsGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<SectorDM>>("api/SECTORS/GetAll");
            else
                return sectorService.SectorsGetAll();
        }

        public static SectorDM SectorsGetByID(int sectorID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<SectorDM>("api/SECTORS/GetById", new FilterAttributesDM() { idName = sectorID.ToString() });
            else
                return sectorService.SectorsGetByID(sectorID);
        }
    }
}
