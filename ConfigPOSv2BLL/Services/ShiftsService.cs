﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class ShiftsService
    {
        ConfigPOSv2Entities entity;
        UserShiftsService userShiftService = new UserShiftsService();
        JournalsService journalService = new JournalsService();

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novoj smjeni
        /// </summary>
        /// <returns>Id dodate smjene</returns>

        public int ShiftInsert()
        {
            OpenConnection();
            int journalId = journalService.JournalGetLast().JournalID;
            int addedShiftId = Convert.ToInt32(entity.stp_SHIFTS_Insert(journalId).FirstOrDefault().Value);
            CloseConnection();

            return addedShiftId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o smjeni na osnovu proslijeđenog ID smjene
        /// </summary>
        /// <param name="ShiftId">Id smjene</param>
        /// <param name="JournalId">Id dnevnika</param>
        /// <param name="OpenTime">Datum i vrijeme početka</param>
        /// <param name="CloseTime">Datum i vrijeme kraja</param>
        /// <param name="Closed">Dali je zatvoren dan</param>

        public void ShiftUpdate(ShiftDM shift)
        {
            OpenConnection();
            entity.stp_SHIFTS_Update(shift.ShiftId, shift.JournalId, shift.OpenTime, shift.CloseTime);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja radi update podatka closed u smjeni
        /// </summary>
        /// <param name="ShiftId">Id smjene</param>

        public bool ShiftClose()
        {
            int shiftId = ShiftGetLast().ShiftId;

            List<UserShiftDM> notClosedUserShifts = userShiftService.GetNotClosedUserShiftsByShiftId(shiftId);

            if (notClosedUserShifts != null && notClosedUserShifts.Count > 0)
                return false;

            OpenConnection();
            entity.stp_SHIFTS_Close(shiftId);
            CloseConnection();
            return true;
        }

        /// <summary>
        /// Funkcija koja vraća listu svih smjena
        /// </summary>
        /// <returns>Lista smjena</returns>

        public List<ShiftDM> ShiftGetAll()
        {
            OpenConnection();
            List<ShiftDM> list = entity.stp_SHIFTS_GetAll().ToList().MapSHIFTSListToShiftDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća zadnje dodatu smjenu
        /// </summary>
        /// <returns>smjena</returns>

        public ShiftDM ShiftGetLast()
        {
            OpenConnection();
            ShiftDM shift = entity.stp_SHIFTS_GetLast().FirstOrDefault().MapSHIFTSToShiftDM();
            CloseConnection();

            return shift;
        }
    }
}
