﻿#pragma checksum "..\..\TestWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "13F1F37BE3934A409CC699B1299D38C61C6BF1A1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.DataPager;
using DevExpress.Xpf.Editors.DateNavigator;
using DevExpress.Xpf.Editors.ExpressionEditor;
using DevExpress.Xpf.Editors.Filtering;
using DevExpress.Xpf.Editors.Flyout;
using DevExpress.Xpf.Editors.Popups;
using DevExpress.Xpf.Editors.Popups.Calendar;
using DevExpress.Xpf.Editors.RangeControl;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Editors.Settings.Extension;
using DevExpress.Xpf.Editors.Validation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ConfigPOSv2 {
    
    
    /// <summary>
    /// TestWindow
    /// </summary>
    public partial class TestWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 43 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridHelpDesk;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid telephoneIcon;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridConfigLogo;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button eCardButton;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridPinPrijava;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnInput;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLogin;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridPIN;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPIN;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridPassPrijava;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUserName;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPassword;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTime;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDate;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExit;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMinimize;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTeamViewer;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridVersion;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVersion;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Journal;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Shift;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UserShift;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridShiftLogin;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridShiftLoginTypes;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridShiftPIN;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCalculatorInput;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtShiftPIN;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel PinPanelYesNo;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnConfirm;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridShiftUsernamePass;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtShiftUserName;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtShiftPassword;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel pinPanelYesNo;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnYes;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNo;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LoginShiftSuccessMessage;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LoginSuccessMessage;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LoginShiftErrorMessage;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\TestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LoginErrorMessage;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ConfigPOSv2;component/testwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TestWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.gridHelpDesk = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.telephoneIcon = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.gridConfigLogo = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.eCardButton = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.gridPinPrijava = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.btnInput = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\TestWindow.xaml"
            this.btnInput.Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 53 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 54 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 55 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 56 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 57 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 58 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 59 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 60 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 61 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 62 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btnLogin = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\TestWindow.xaml"
            this.btnLogin.Click += new System.Windows.RoutedEventHandler(this.btnLogin_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.gridPIN = ((System.Windows.Controls.Grid)(target));
            return;
            case 19:
            this.txtPIN = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 20:
            
            #line 69 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnInput_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.gridPassPrijava = ((System.Windows.Controls.Grid)(target));
            return;
            case 22:
            this.txtUserName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.txtPassword = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 24:
            this.lblTime = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.lblDate = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.btnExit = ((System.Windows.Controls.Button)(target));
            
            #line 78 "..\..\TestWindow.xaml"
            this.btnExit.Click += new System.Windows.RoutedEventHandler(this.btnExit_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.btnMinimize = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\TestWindow.xaml"
            this.btnMinimize.Click += new System.Windows.RoutedEventHandler(this.btnMinimize_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.btnTeamViewer = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.gridVersion = ((System.Windows.Controls.Grid)(target));
            return;
            case 30:
            this.lblVersion = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.Journal = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\TestWindow.xaml"
            this.Journal.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.Shift = ((System.Windows.Controls.Button)(target));
            
            #line 94 "..\..\TestWindow.xaml"
            this.Shift.Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 33:
            this.UserShift = ((System.Windows.Controls.Button)(target));
            
            #line 95 "..\..\TestWindow.xaml"
            this.UserShift.Click += new System.Windows.RoutedEventHandler(this.UserShift_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.gridShiftLogin = ((System.Windows.Controls.Grid)(target));
            return;
            case 35:
            this.gridShiftLoginTypes = ((System.Windows.Controls.Grid)(target));
            return;
            case 36:
            this.gridShiftPIN = ((System.Windows.Controls.Grid)(target));
            return;
            case 37:
            this.btnCalculatorInput = ((System.Windows.Controls.Button)(target));
            
            #line 113 "..\..\TestWindow.xaml"
            this.btnCalculatorInput.Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            
            #line 114 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            
            #line 115 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            
            #line 116 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 41:
            
            #line 117 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 42:
            
            #line 118 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 43:
            
            #line 119 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 44:
            
            #line 120 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 45:
            
            #line 121 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 46:
            
            #line 122 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 47:
            
            #line 123 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 48:
            
            #line 124 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCalculatorInput_Click);
            
            #line default
            #line hidden
            return;
            case 49:
            this.txtShiftPIN = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 50:
            this.PinPanelYesNo = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 51:
            this.btnConfirm = ((System.Windows.Controls.Button)(target));
            
            #line 130 "..\..\TestWindow.xaml"
            this.btnConfirm.Click += new System.Windows.RoutedEventHandler(this.OpenShiftClick);
            
            #line default
            #line hidden
            return;
            case 52:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 138 "..\..\TestWindow.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.CloseShiftClick);
            
            #line default
            #line hidden
            return;
            case 53:
            this.gridShiftUsernamePass = ((System.Windows.Controls.Grid)(target));
            return;
            case 54:
            this.txtShiftUserName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 55:
            this.txtShiftPassword = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 56:
            this.pinPanelYesNo = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 57:
            this.btnYes = ((System.Windows.Controls.Button)(target));
            
            #line 152 "..\..\TestWindow.xaml"
            this.btnYes.Click += new System.Windows.RoutedEventHandler(this.OpenShiftClick);
            
            #line default
            #line hidden
            return;
            case 58:
            this.btnNo = ((System.Windows.Controls.Button)(target));
            
            #line 160 "..\..\TestWindow.xaml"
            this.btnNo.Click += new System.Windows.RoutedEventHandler(this.CloseShiftClick);
            
            #line default
            #line hidden
            return;
            case 59:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 172 "..\..\TestWindow.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.CloseForm);
            
            #line default
            #line hidden
            return;
            case 60:
            this.LoginShiftSuccessMessage = ((System.Windows.Controls.Grid)(target));
            return;
            case 61:
            
            #line 185 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OpenShiftClick);
            
            #line default
            #line hidden
            return;
            case 62:
            this.LoginSuccessMessage = ((System.Windows.Controls.Label)(target));
            return;
            case 63:
            this.LoginShiftErrorMessage = ((System.Windows.Controls.Grid)(target));
            return;
            case 64:
            
            #line 200 "..\..\TestWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OpenShiftClick);
            
            #line default
            #line hidden
            return;
            case 65:
            this.LoginErrorMessage = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

