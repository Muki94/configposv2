﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class SubcategoriesCommunication
    {
        private static SubcategoriesService subcategoriesService = new SubcategoriesService();

        public static int SubcategoriesInsert(SubcategoryDM subcategory)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, SubcategoryDM>("api/SUBCATEGORIES/Insert", subcategory);
            else
                return subcategoriesService.SubcategoryInsert(subcategory);
        }

        public static void SubcategoriesUpdate(SubcategoryDM subcategory)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<SubcategoryDM, SubcategoryDM>("api/SUBCATEGORIES/Update", subcategory);
            else
                subcategoriesService.SubcategoryUpdate(subcategory);
        }

        public static void SubcategoriesDelete(int subcategoryId)
        {
            if (App.usingWebApi)
                WebApiHelper.Delete<SubcategoryDM>("api/SUBCATEGORIES/Delete", subcategoryId);
            else
                subcategoriesService.SubcategoryDelete(subcategoryId);
        }

        public static List<SubcategoryDM> SubcategoriesGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<SubcategoryDM>>("api/SUBCATEGORIES/GetAll");
            else
                return subcategoriesService.SubcategoriesGetAll();
        }

        public static SubcategoryDM SubcategoriesGetByID(int subcategoryId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<SubcategoryDM>("api/SUBCATEGORIES/GetById", new FilterAttributesDM(){ idName = subcategoryId.ToString() });
            else
                return subcategoriesService.SubcategoriesGetByID(subcategoryId);
        }
        public static List<SubcategoryDM> SubcategoriesGetByCategoryId(int categoryId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<SubcategoryDM>>("api/SUBCATEGORIES/GetAllByCategoryId", new FilterAttributesDM() { idName = categoryId.ToString() });
            else
                return subcategoriesService.SubcategoriesGetByCategoryID(categoryId);
        }
    }
}
