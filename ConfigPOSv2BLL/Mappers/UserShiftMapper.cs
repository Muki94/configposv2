﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;

namespace ConfigPOSv2BLL.Mappers
{
    public static class UserShiftMapper
    {
        public static UserShiftDM MapUSERSHIFTToUserShiftDM(this USER_SHIFTS item)
        {
            if (item == null)
                return null;

            UserShiftDM itemModel = new UserShiftDM()
            {
                UserShiftID = item.UserShiftID,
                ShiftID = item.ShiftID,
                UserID = item.UserID,
                SignInTime = item.SignInTime,
                SignOutTime = item.SignOutTime
            };

            return itemModel;
        }

        public static List<UserShiftDM> MapUSERSHIFTListToUserShiftDMList(this List<USER_SHIFTS> items)
        {
            if (items == null)
                return null;

            List<UserShiftDM> itemModels = new List<UserShiftDM>();

            foreach (var item in items)
            {
                itemModels.Add(new UserShiftDM()
                {
                    UserShiftID = item.UserShiftID,
                    ShiftID = item.ShiftID,
                    UserID = item.UserID,
                    SignInTime = item.SignInTime,
                    SignOutTime = item.SignOutTime
                });
            }

            return itemModels;
        }
         
    }
}
