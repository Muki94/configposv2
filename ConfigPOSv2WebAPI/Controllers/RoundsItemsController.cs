﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class RoundsItemsController : ApiController
    {
        RoundsItemsService roundsItemsService = new RoundsItemsService();

        // INSERT
        [Route("api/ROUNDS_ITEMS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertRoundItem([FromBody]RoundItemDM item)
        {
            try
            {
                int addedId = roundsItemsService.RoundItemInsert(item);
                return Request.CreateResponse(HttpStatusCode.OK, addedId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY SECTOR ID
        [Route("api/ROUNDS_ITEMS/GetByRoundID")]
        [HttpGet]
        public HttpResponseMessage GetByRoundID([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, roundsItemsService.RoundsItemsGetByRoundID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        [Route("api/ROUNDS_ITEMS/GetByOrderIDView")]
        [HttpGet]
        public HttpResponseMessage GetByOrderIDView([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, roundsItemsService.RoundsItemsGetByOrderIDView(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        [Route("api/ROUNDS_ITEMS/GetByOrderID")]
        [HttpGet]
        public HttpResponseMessage GetByOrderID([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, roundsItemsService.RoundsItemsGetByOrderID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
