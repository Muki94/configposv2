﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class SettingsCommunication
    {
        private static SettingsService settingsService = new SettingsService();

        public static List<SettingDM> SettingsGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<SettingDM>>("api/SETTINGS/GetAll");
            else
                return settingsService.GetSettingsAll();
        }

        public static SettingDM SettingsGetByName(string name)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<SettingDM>("api/SETTINGS/GetByName", new FilterAttributesDM() { idName = name });
            else
                return settingsService.GetSettingsByName(name);
        }
    }
}
