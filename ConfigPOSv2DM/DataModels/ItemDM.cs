﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ItemDM
    {
        public int ItemID { get; set; }
       
        public string Code { get; set; }
        
        public string Name { get; set; }
        
        public int SubcategoryID { get; set; }
        
        public Nullable<int> ImageID { get; set; }
        public ImageDM Image { get; set; }
        
        public Nullable<int> ItemGroupID { get; set; }
        public ItemGroupDM ItemGroup { get; set; }

        public decimal Price { get; set; }
        
        public bool Active { get; set; }
        
        public string Description { get; set; }
        
        public string RefCode { get; set; }
        
        public bool DailyItem { get; set; }
        
        public bool Favorite { get; set; }
    }
}
