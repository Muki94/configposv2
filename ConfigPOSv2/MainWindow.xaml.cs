﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ConfigPOSv2BLL.Services;
using System.Windows.Threading;
using System.Reflection;
using System.Diagnostics;
using ConfigPOSv2.Helpers;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.Timers;
using System.Text.RegularExpressions;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using Fiscal;
using System.Windows.Media.Animation;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MainWindow.cs");
        DispatcherTimer timerDateTime = new DispatcherTimer();

        //TeamViewerHelper teamV = new TeamViewerHelper();

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                //Inicjalizacija verzije aplikacije
                #region start
                var version = ((AssemblyFileVersionAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyFileVersionAttribute))).Version;
                lblVersion.Content = "v" + version;
                #endregion

                //inicjalizacija informacija o smjeni i dnevniku
                #region start
                if (Properties.Settings.Default.JOURNAL_SHIFT_ENABLED)
                {
                if (JournalForm.IsJournalOpen())
                {
                    btnJournalActive.IsDefault = !btnJournalActive.IsDefault;
                    btnShiftActive.IsEnabled = true;
                }
                else
                {
                    btnShiftActive.IsEnabled = false;
                    btnShiftActive.Opacity = 0.3;
                }
                
                if (ShiftForm.IsShiftOpen())
                {
                    btnShiftActive.IsDefault = !btnShiftActive.IsDefault;
                    UserShift.IsEnabled = true;
                }
                }
                else
                {
                    btnJournalShiftsClosedMenu.Visibility = Visibility.Hidden;
                    btnJournalShiftsOpenMenu.Visibility = Visibility.Hidden;
                    UserShift.Visibility = Visibility.Collapsed;
                }
                #endregion

                if (App.eCardUsing == "True")
                {
                    btnECard.Visibility = Visibility.Visible;
                }
                else
                {
                    btnECard.Visibility = Visibility.Hidden;
                }

                LoadSettings();

                //Timer za update datuma i vremena
                timerDateTime.Interval = new TimeSpan(0, 1, 0);
                timerDateTime.Tick += timerDateTime_Tick;
                timerDateTime.Start();

                if (App.prikazi_TeamViewer)
                {
                    btnTeamViewer.Visibility = System.Windows.Visibility.Visible;
                    gridHelpDesk.Margin = new Thickness(140, 40.817, 0, 0);
                }
                else
                {
                    btnTeamViewer.Visibility = System.Windows.Visibility.Collapsed;
                    gridHelpDesk.Margin = new Thickness(28, 40.817, 0, 0);
                }
            }
            catch
            { }
        }

        #region DEFAULT SETTINGS

        private void timerDateTime_Tick(object sender, EventArgs e)
        {
            try
            {
                SetDateTime();
            }
            catch
            {
            }
        }

        private void SetDateTime()
        {
            lblTime.Content = DateTime.Now.ToString("HH:mm");
            lblDate.Content = DateTime.Now.ToString("dd.MM.yyyy.");
        }

        private void LoadSettings()
        {

            if (App.login_type == "USERNAME_PASS")
            {
                gridPinPrijava.Visibility = Visibility.Collapsed;
                gridPassPrijava.Visibility = Visibility.Visible;
                gridShiftUsernamePass.Visibility = Visibility.Visible;
                txtUserName.Focus();
            }
            else
            {
                gridPinPrijava.Visibility = Visibility.Visible;
                gridPassPrijava.Visibility = Visibility.Collapsed;
                gridShiftPIN.Visibility = Visibility.Visible;
                txtPIN.Focus();
            }
            SetDateTime();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            frmMessageBox frm = new frmMessageBox("MsgBoxTitleShutDown", frmMessageBox.MsgButtons.YesNo);
            this.Opacity = 0.5;
            if (frm.ShowDialog() == true)
            {
                this.Close();
            }
            this.Opacity = 1;
        }

        #endregion

        #region ECARD, TEAMVIEWER

        private void btnECard_Click(object sender, RoutedEventArgs e)
        {
            LoginForm.StartECard();
            }

        private void btnTeamViewer_Click(object sender, RoutedEventArgs e)
            {
            LoginForm.StartTeamViewer();
            }

        #endregion

        #region PRIKAŽI SAKRI GRID ZA NARUDŽBE
        private void showOrdersOnTableGrid()
        {
            gridOrdersOnTable.Opacity = 1;
            gridOrdersOnTable.IsEnabled = true;
            gridOrder.Opacity = 1;
            gridOrder.IsEnabled = true;
            btnEditMode.Opacity = 1;
            btnEditMode.IsEnabled = true;
            btnEditModeTables.Opacity = 1;
            btnEditModeTables.IsEnabled = true;
        }

        private void hideOrdersOnTableGrid()
        {
            gridOrdersOnTable.Opacity = 0.3;
            gridOrdersOnTable.IsEnabled = false;
            gridOrder.Opacity = 0.3;
            gridOrder.IsEnabled = false;
            btnEditMode.Opacity = 0.3;
            btnEditMode.IsEnabled = false;
            btnEditModeTables.Opacity = 0.3;
            btnEditModeTables.IsEnabled = false;
        }
        #endregion

        #region LOGIN I LOGOUT

        private void btnInput_Click(object sender, RoutedEventArgs e)
        {
            Button number = (Button)sender;
            if (number.Uid == "X")
            {
                txtPIN.Password = string.Empty;
            }
            else if (number.Uid == "-1")
            {
                txtPIN.Password = txtPIN.Password.Length > 0 ? txtPIN.Password.Remove(txtPIN.Password.Length - 1) : string.Empty;
            }
            else
            {
                txtPIN.Password += number.Uid;
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Button btnLogin = (Button)sender;

            //defaultno dozvoli manipulaciju grida
            showOrdersOnTableGrid();

            if (btnLogin.Name == "btnLogin")
            {
                if (LoginForm.Login(txtPIN.Password))
                {
                    txtPIN.Password = string.Empty;
                    txtPIN.Focus();


                    ShiftDM lastOpenedShift = ShiftForm.GetLastShift();

                    if (!lastOpenedShift.Closed)
                    {
                        UserShiftDM userShift = UserShiftsCommunication.GetUserShift(Int32.Parse(App.Current.userId), lastOpenedShift.ShiftId);

                        if (userShift == null || userShift.SignOutTime != null)
                        {
                            hideOrdersOnTableGrid();
                        }
                        else
                        {
                            showOrdersOnTableGrid();
                        }
                    }
                    else
                    {
                        hideOrdersOnTableGrid();
                    }


                    LoadAllAfterLoginSuccess();
                }
                else
                {
                    MessageBox.Show("Pogrešni korisnički podaci!");
                    txtPIN.Focus();
                    txtPIN.Password = string.Empty;
                }
            }
            else
            {
                if (LoginForm.Login(txtPassword.Password, txtUserName.Text))
                {
                    txtPassword.Password = string.Empty;
                    txtPassword.Focus();


                    ShiftDM lastOpenedShift = ShiftForm.GetLastShift();

                    if (!lastOpenedShift.Closed)
                    {
                        UserShiftDM userShift = UserShiftsCommunication.GetUserShift(Int32.Parse(App.Current.userId), lastOpenedShift.ShiftId);

                        if (userShift == null || userShift.SignOutTime != null)
                        {
                            hideOrdersOnTableGrid();
                        }
                    }
                    else
                    {
                        hideOrdersOnTableGrid();
                    }
                    LoadAllAfterLoginSuccess();
                }
                else
                {
                    MessageBox.Show("Pogrešni korisnički podaci!");
                    txtPassword.Focus();
                    txtPassword.SelectAll();
                }
            }
        }

        private void LoadAllAfterLoginSuccess()
        {
            LoadOrdersLabels();
            stackRacunStavke.Children.Clear();
            LoadAllActiveCategories();
            gridLogin.Visibility = Visibility.Collapsed;
            if (App.usingTables)
            {
                gridTables.Visibility = Visibility.Visible;
                LoadTablesGrid();
        }
            else
            {
                gridTables.Visibility = Visibility.Collapsed;
                btnShowTables.IsEnabled = false;
                btnShowTablesOpenedMenu.IsEnabled = false;
                LoadOrderRoundsOnTable();
            }

        }

        private void LoadOrdersLabels()
        {
            lblUser.Content = App.Current.username;
            lblUserGroup.Content = App.Current.userType;
            lblDateTime.Content = DateTime.Now.ToString("dd.MM.yyyy.") + " " + DateTime.Now.ToString("HH:mm");
        }

        private void txtPIN_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //Prijava korisnika putem PIN-a
            if (e.Key == Key.Enter && Properties.Settings.Default.LOGIN_TYPE == "PIN")
            {
                btnLogin.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            bool odjaviSe = true;
            if (stackRacunStavke.Children.Count > 0)
            {
                frmMessageBox frm = new frmMessageBox("MsgBoxTitleLogOut", frmMessageBox.MsgButtons.YesNo);
                this.Opacity = 0.5;
                if (frm.ShowDialog() == false)
                {
                    odjaviSe = false;
                }
                this.Opacity = 1;
            }
            if (odjaviSe)
            {
                ClearOrdersFormAfterLogOut();
                if (App.login_type == "USERNAME_PASS")
                {
                    txtUserName.Focus();
                }
                else
                {
                    txtPIN.Focus();
                }
            }

        }

        private void ClearOrdersFormAfterLogOut()
        {
            if (btnEditMode.IsDefault == false)
                btnEditMode.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            if (btnEditModeTables.IsDefault == false)
                btnEditModeTables.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            stackRacunStavke.Children.Clear();
            OrdersForm.ResetOrders();
            lblUkupnoNarudzba.Content = OrdersForm.orderTotalZero;
            gridOpenedMenuSideBar.Visibility = Visibility.Collapsed;
            gridLogin.Visibility = Visibility.Visible;
            gridOrders.IsEnabled = true;
            gridOrders.Opacity = 1;
            ClearCategorySelected(wrapKategorije);
            wrapKategorije.Children.Clear();
            wrapPodkategorije.Children.Clear();
            wrapArtikliLista.Children.Clear();
            stackRacunStavke.Children.Clear();
        }

        #endregion

        #region UČITAVANJE KATEGORIJA, PODKATEGORIJA

        private void LoadAllActiveCategories()
        {
            List<CategoryDM> categories = OrdersForm.CategoriesGetAll();
            if (categories.Count > 0)
            {
                foreach (var item in categories)
                {
                    Button button = StyleHelper.CreateKategorijaButton(item.Name);
                    button.Style = (Style)this.Resources["KategorijaButtonStyle"];
                    button.Uid = item.CategoryID.ToString();
                    button.Click += new RoutedEventHandler(btnCategory_Click);
                    wrapKategorije.Children.Add(button);
                }
                if (categories.Count == 1)
                {
                    wrapKategorije.Visibility = Visibility.Collapsed;
                    stackKategorijePodkat.Margin = btnFavoriteArtikli.Margin;
                    this.SubcategoriesSet(categories.First().CategoryID.ToString());
                }
            }
        }

        private void btnCategory_Click(object sender, RoutedEventArgs e)
        {
            ClearCategorySelected(wrapKategorije);
            Button b = (Button)sender;
            b.IsDefault = false;
            this.SubcategoriesSet(b.Uid);
        }

        void ClearCategorySelected(WrapPanel wrapPanel)
        {
            try
            {
                for (int i = 0; i < wrapPanel.Children.Count; i++)
                {
                    Button b = (Button)wrapPanel.Children[i];
                    b.IsDefault = true;
                }
            }
            catch
            {

            }
        }

        public void SubcategoriesSet(string id)
        {
            wrapArtikliLista.Children.Clear();
            wrapPodkategorije.Children.Clear();
            List<SubcategoryDM> subcategories = OrdersForm.SubcategoriesGetByCategoryID(id);
            Button button = new Button();
            foreach (var item in subcategories)
            {
                button = StyleHelper.CreatePodkategorijaButton(item.Name);
                button.Style = (Style)this.Resources["PodkategorijaButtonStyle"];
                button.Uid = item.SubcategoryID.ToString();
                button.Click += new RoutedEventHandler(btnSubCategory_Click);
                wrapPodkategorije.Children.Add(button);
            }
            if (!btnEditMode.IsDefault)
            {
                if (wrapPodkategorije.Children.Count < 6)
                {
                    CreateEditModeAddButton(wrapPodkategorije);
                    if (gridNovaPodkategorijaEditMode.Visibility == Visibility.Visible)
                    {
                        SetCategoryAddButtonsFixed(wrapPodkategorije, false);
                    }
                }
                else
                {
                    SetCategoryAddButtonsFixed(wrapPodkategorije, true);
                }
                CreateEditModeAddButton(wrapArtikliLista);
            }
        }

        private void btnSubCategory_Click(object sender, RoutedEventArgs e)
        {
            ClearCategorySelected(wrapPodkategorije);
            Button b = (Button)sender;
            b.IsDefault = false;
            this.ItemsSet(b.Uid);
        }

        #endregion

        #region UČITAVANJE ARTIKALA

        private void ItemsSet(string id)
        {
            wrapArtikliLista.Children.Clear();
            List<ItemDM> items = OrdersForm.ItemsGetBySubcategoryID(id, !btnFavoriteArtikli.IsDefault);
            StationsDM station = StationsCommunication.StationsGetByUserID(App.Current.userId);

            Button button = new Button();
            foreach (var item in items)
            {
                ItemPricesDM itemPrice = ItemPricesCommunication.ItemPricesGetByItemID(item.ItemID.ToString(), station.BusinessUnitID.ToString());

                //učitati slike iz temporary tabele koja se ucitava na pocetku programa <--napomena
                Byte[] imageData = item.ImageID != null && ItemsCommunication.ItemsGetImageByImageId(item.ImageID.Value) != null ? ItemsCommunication.ItemsGetImageByImageId(item.ImageID.Value).Data : null;
                button = StyleHelper.CreateArtikalButton(item.Name, item.ItemID, itemPrice.Price, imageData);
                button.Style = (Style)this.Resources["ArtikalButtonStyle"];
                button.Click += new RoutedEventHandler(btnArtikal_Click);
                wrapArtikliLista.Children.Add(button);
            }
            if (!btnEditMode.IsDefault)
            {
                CreateEditModeAddButton(wrapArtikliLista);
            }
        }

        private void btnArtikal_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;

            if (!btnEditMode.IsDefault)
            {
                btnAddArtikal_Click(b, new RoutedEventArgs());
            }
            else
            {
                if (btnShowNarudzba.IsDefault == true)
                {
                    btnShowNarudzba.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                }
                OrdersFormAddOrderItemUI(Convert.ToInt32(b.ToolTip), b.Content.ToString(), Convert.ToDecimal(b.Tag));
            }
        }

        private void btnFavoriteArtikli_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            b.IsDefault = b.IsDefault ? false : true;

            if (b.IsDefault == true)
            {
                string podkategorijaID = string.Empty;
                for (int i = 0; i < wrapPodkategorije.Children.Count; i++)
                {
                    Button b2 = (Button)wrapPodkategorije.Children[i];
                    if (!b2.IsDefault)
                    {
                        podkategorijaID = b2.Uid;
                        break;
                    }
                }
                if (podkategorijaID == string.Empty)
                {
                    wrapArtikliLista.Children.Clear();
                }
                else
                {
                    ItemsSet(podkategorijaID);
                }
            }
            else
            {
                wrapArtikliLista.Children.Clear();
            }

        }

        #endregion

        #region DODAVANJE ARTIKLA NA NARUDŽBU

        private void OrdersFormAddOrderItemUI(int itemID, string name, decimal price)
        {
            OrdersForm.OrderItemVM orderItem = OrdersForm.AddOrderItem(itemID, name, price);
            if (OrdersForm.isOnOrder)
            {
                for (int i = 0; i < stackRacunStavke.Children.Count; i++)
                {
                    Button b = (Button)stackRacunStavke.Children[i];
                    if (Convert.ToInt32(b.Uid) == orderItem.ItemID)
                    {
                        b.Tag = OrdersForm.GetItemQuantityWithOrWithoutDecimals(orderItem.Quantity);
                        b.IsDefault = true;
                        b.IsDefault = false;
                        b.BringIntoView();
                        break;
                    }
                }
            }
            else
            {
                Button b = StyleHelper.CreateStavkaNaRacunuButton(orderItem.ItemID, orderItem.Name, orderItem.Quantity, orderItem.Price);
                b.Style = (Style)this.Resources["ArtikalNaRacunuButtonStyle"];
                stackRacunStavke.Children.Add(b);
                b.BringIntoView();
            }
            lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;
        }
        
        private void btnRacunStavkaKolicina_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Button b = (Button)sender;
            for (int i = 0; i < stackRacunStavke.Children.Count; i++)
            {
                Button b2 = (Button)stackRacunStavke.Children[i];

                if (b2.Uid.ToString() == b.Uid.ToString())
                {
                    b2.IsDefault = true;
                    b2.IsDefault = false;
                    decimal kolicinaNakonSmanjenja = Convert.ToDecimal(b2.Tag) - 1;
                    decimal oldTotal = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total;
                    OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total = oldTotal - Convert.ToDecimal(b2.ToolTip) <= 0 ? 0 : oldTotal - Convert.ToDecimal(b2.ToolTip);//item.Price
                    OrdersForm.EditOrderItemQuantity(b2.Uid.ToString(), true, kolicinaNakonSmanjenja);
                    if (kolicinaNakonSmanjenja <= 0)
                    {
                        stackRacunStavke.Children.Remove(b2);
                    }
                    else
                    {
                        b2.Tag = OrdersForm.GetItemQuantityWithOrWithoutDecimals(kolicinaNakonSmanjenja);
                    }
                    lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;
                    break;
                }
            }


        }

        #endregion

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void scrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
        }

        private void ScrolToSelectedButton(WrapPanel wrapPanel)
        {
            try
        {
                for (int i = 0; i < wrapPanel.Children.Count; i++)
            {
                    if (((Button)wrapPanel.Children[i]).IsDefault == false)
                {
                        ((Button)wrapPanel.Children[i]).BringIntoView();
                        break;
                }
            }
                }
            catch
                {

                }
            }

        #region EDIT MODE FORM

        public void CreateEditModeAddButton(WrapPanel wrapPanel)
        {
            if (wrapPanel == wrapPodkategorije)
            {
                Button button = StyleHelper.CreatePodkategorijaButton(string.Empty);
                button.Style = (Style)this.Resources["PodkategorijaButtonStyle"];
                button.Uid = "0";
                button.Click += new RoutedEventHandler(btnAddSubcategory_Click);
                wrapPodkategorije.Children.Add(button);
                button.BringIntoView();
        }
            else if (wrapPanel == wrapKategorije)
        {
                Button button = StyleHelper.CreateKategorijaButton(string.Empty);
                button.Style = (Style)this.Resources["KategorijaButtonStyle"];
                button.Uid = "0";
                button.Click += new RoutedEventHandler(btnAddCategory_Click);
                wrapKategorije.Children.Add(button);
                button.BringIntoView();
            }
            else if (wrapPanel == wrapArtikliLista)
            {
                Button button = StyleHelper.CreateNoviArtikalButton();
                button.Style = (Style)this.Resources["NoviArtikalButtonStyle"];
                button.Click += new RoutedEventHandler(btnAddArtikal_Click);
                wrapArtikliLista.Children.Add(button);
                button.BringIntoView();
            }
        }

        public void RemoveEditModeAddButton(WrapPanel wrapPanel)
        {
                for (int i = 0; i < wrapPanel.Children.Count; i++)
                {
                Button b2 = (Button)wrapPanel.Children[i];
                if (b2.Uid == "0")
                    {
                    wrapPanel.Children.Remove(wrapPanel.Children[i]);
                        break;
                    }
                }
            if (wrapPanel == wrapPodkategorije)
            {
                scrollPodkategorije.ScrollToHome();
            }
            else if (wrapPanel == wrapKategorije)
            {
                scrollKategorije.ScrollToHome();
            }
            else if (wrapPanel == wrapArtikliLista)
            {
                scrollArtikliLista.ScrollToHome();
            }

            }

        private void SetCategoryAddButtonsFixed(WrapPanel wrapPanel, bool set)
        {
            if (wrapPanel == wrapKategorije)
            {
                gridNovaKategorijaEditMode.Visibility = set ? Visibility.Visible : Visibility.Collapsed;
                scrollKategorije.Width = set ? 840 : 1020;
        }
            else
            {
                gridNovaPodkategorijaEditMode.Visibility = set ? Visibility.Visible : Visibility.Collapsed;
                scrollPodkategorije.Width = set ? 840 : 1020;
            }
        }

        private void btnEditMode_Click(object sender, RoutedEventArgs e)
        {
            btnEditMode.IsDefault = btnEditMode.IsDefault == true ? false : true;

            if (btnEditMode.IsDefault == false)
            {
                gridNarudzbaSideBar.Opacity = 0.3;
                gridNarudzbaSideBar.IsEnabled = false;

                if (wrapKategorije.Children.Count >= 5)
                {
                    SetCategoryAddButtonsFixed(wrapKategorije, true);
                    ScrolToSelectedButton(wrapKategorije);
                }
                else
                {
                    CreateEditModeAddButton(wrapKategorije);

                }
                if (wrapPodkategorije.Children.Count >= 5)
                {
                    SetCategoryAddButtonsFixed(wrapPodkategorije, true);
                    ScrolToSelectedButton(wrapPodkategorije);
                }
                else
                {
                    CreateEditModeAddButton(wrapPodkategorije);
                }
                CreateEditModeAddButton(wrapArtikliLista);
            }
            else
            {
                gridNarudzbaSideBar.Opacity = 1;
                gridNarudzbaSideBar.IsEnabled = true;

                if (gridNovaKategorijaEditMode.Visibility == Visibility.Visible)
                {
                    SetCategoryAddButtonsFixed(wrapKategorije, false);
                    ScrolToSelectedButton(wrapKategorije);
                }
                else
                {
                    RemoveEditModeAddButton(wrapKategorije);
                }
                if (gridNovaPodkategorijaEditMode.Visibility == Visibility.Visible)
                {
                    SetCategoryAddButtonsFixed(wrapPodkategorije, false);
                    ScrolToSelectedButton(wrapPodkategorije);
                }
                else
                {
                    RemoveEditModeAddButton(wrapPodkategorije);
                }

                RemoveEditModeAddButton(wrapArtikliLista);
            }
        }

        private void btnAddArtikal_Click(object sender, RoutedEventArgs e)
        {
            this.Opacity = 0.5;
            Button b = (Button)sender;
            int index = wrapArtikliLista.Children.IndexOf(b);
            frmItemsEdit frm = new frmItemsEdit(Convert.ToInt32(b.ToolTip));
            var result = frm.ShowDialog();

            var buttons = wrapPodkategorije.Children;
            bool isSameSubcategory = true;

            if (result == true)
            {
                ItemDM items = frm.addedEditedArticle;

                foreach (var btn in buttons)
                {
                    bool isDefaultButton = ((Button)btn).IsDefault;

                    if (!isDefaultButton)
                    {
                        int subcategoryId = Int32.Parse(((Button)btn).Uid);
                        if (subcategoryId != items.SubcategoryID)
                        {
                            isSameSubcategory = false;
                        }
                    }
                }

                Byte[] imageData = items.Image != null ? items.Image.Data : null;
                Button button = StyleHelper.CreateArtikalButton(items.Name, items.ItemID, items.Price, imageData);
                button.Style = (Style)this.Resources["ArtikalButtonStyle"];
                button.Click += new RoutedEventHandler(btnArtikal_Click);

                if (frm.isEdit)
                {
                    if (isSameSubcategory)
                    {
                        wrapArtikliLista.Children.RemoveAt(index);
                        wrapArtikliLista.Children.Insert(index, button);
                    }
                    else
                    {
                        wrapArtikliLista.Children.RemoveAt(index);
                    }
                }
                else
                {
                    if (isSameSubcategory)
                    {
                        wrapArtikliLista.Children.RemoveAt(index);
                        wrapArtikliLista.Children.Insert(index, button);
                        wrapArtikliLista.Children.Add(b);
                    }
                }

                wrapArtikliLista.UpdateLayout();
            }

            this.Opacity = 1;

        }

        private void btnAddSubcategory_Click(object sender, RoutedEventArgs e)
        {
            frmSubcategoriesAddMain frm = new frmSubcategoriesAddMain();
            if (frm.ShowDialog() == true)
            {

                string selectedCategoryID = string.Empty;
                for (int i = 0; i < wrapKategorije.Children.Count; i++)
                {
                    Button b2 = (Button)wrapKategorije.Children[i];
                    if (b2.IsDefault == false)
                    {
                        selectedCategoryID = b2.Uid;
                        break;
                    }
                }
                if (selectedCategoryID == frm.addedEditedSubcategory.CategoryID.ToString())
                {
                    for (int i = 0; i < wrapPodkategorije.Children.Count; i++)
                    {
                        Button b2 = (Button)wrapPodkategorije.Children[i];
                        if (b2.Uid == "0")
                        {
                            wrapPodkategorije.Children.Remove(wrapPodkategorije.Children[i]);
                            break;
                        }
                    }
                    Button b = (Button)sender;
                    wrapPodkategorije.Children.Remove(b);
                    Button button = StyleHelper.CreatePodkategorijaButton(frm.addedEditedSubcategory.Name);
                    button.Style = (Style)this.Resources["PodkategorijaButtonStyle"];
                    button.Uid = frm.addedEditedSubcategory.SubcategoryID.ToString();
                    button.Click += new RoutedEventHandler(btnSubCategory_Click);
                    wrapPodkategorije.Children.Add(button);
                    if (wrapPodkategorije.Children.Count >= 5)
                    {
                        SetCategoryAddButtonsFixed(wrapPodkategorije, true);
                        button.BringIntoView();
                    }
                    else
                    {
                        wrapPodkategorije.Children.Add(b);
                        b.BringIntoView();
                    }
                }

            }
        }

        private void btnAddCategory_Click(object sender, RoutedEventArgs e)
        {

            frmCategoriesAddMain frm = new frmCategoriesAddMain();
            if (frm.ShowDialog() == true)
            {
                for (int i = 0; i < wrapKategorije.Children.Count; i++)
                {
                    Button b2 = (Button)wrapKategorije.Children[i];
                    if (b2.Uid == "0")
                    {
                        wrapKategorije.Children.Remove(wrapKategorije.Children[i]);
                        break;
                    }
                }
      
        
                Button b = (Button)sender;
                wrapKategorije.Children.Remove(b);
                Button button = StyleHelper.CreateKategorijaButton(frm.addedEditedCategory.Name);
                button.Style = (Style)this.Resources["KategorijaButtonStyle"];
                button.Uid = frm.addedEditedCategory.CategoryID.ToString();
                button.Click += new RoutedEventHandler(btnCategory_Click);
                wrapKategorije.Children.Add(button);
                if (wrapKategorije.Children.Count >= 5)
                {
                    SetCategoryAddButtonsFixed(wrapKategorije, true);
                    button.BringIntoView();
                }
                else
                {
                    wrapKategorije.Children.Add(b);
                    b.BringIntoView();
                }
            }

        }

        #endregion
        
        #region RIGHT SIDEBAR MENU

        private void btnRightSideBarMenu_ItemClick(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            if (b.Uid == "0")
            {
                gridOrder.Visibility = Visibility.Visible;
                gridFiskalizacija.Visibility = Visibility.Collapsed;
                gridOrdersOnTable.Visibility = Visibility.Collapsed;
                btnShowNarudzba.IsDefault = false;
                btnShowStol.IsDefault = true;
                btnShowFiskalizacija.IsDefault = true;
            }
            else if (b.Uid == "1")
            {
                gridOrder.Visibility = Visibility.Collapsed;
                gridOrdersOnTable.Visibility = Visibility.Visible;
                gridFiskalizacija.Visibility = Visibility.Collapsed;
                btnShowNarudzba.IsDefault = true;
                btnShowStol.IsDefault = false;
                gridOrdersOnTableOpenedOrder.Visibility = Visibility.Collapsed;
                btnShowFiskalizacija.IsDefault = true;
            }
            else
            {
                gridOrder.Visibility = Visibility.Collapsed;
                gridOrdersOnTable.Visibility = Visibility.Collapsed;
                gridFiskalizacija.Visibility = Visibility.Visible;
                btnShowNarudzba.IsDefault = true;
                btnShowStol.IsDefault = true;
                btnShowFiskalizacija.IsDefault = false;
            }
        }

        private void btnMenuOpenClose_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            if (b.Uid == "1")
            {
                gridOrders.IsEnabled = false;
                gridOrders.Opacity = 0.5;
                gridOpenedMenuSideBar.Visibility = Visibility.Visible;
            }
            else
            {
                gridOrders.IsEnabled = true;
                gridOrders.Opacity = 1;
                gridOpenedMenuSideBar.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region CALCULATOR FORM

        private void btnRacunStavkaDetail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                string tempArtikalNaziv = b.Content.ToString();
                string tempArtikalKolicina = b.Tag.ToString();
                string tempArtikalCijena = b.ToolTip.ToString();

                CalculatorForm.TempItemTotal = Convert.ToDecimal(tempArtikalKolicina) * Convert.ToDecimal(tempArtikalCijena);
                CalculatorForm.TempItemID = b.Uid.ToString();
                txtArtikalNapomena.Text = string.Empty;
                if (b.ContentStringFormat != null)
                {
                    CalculatorForm.TempItemNote = b.ContentStringFormat.ToString();
                    txtArtikalNapomena.Text = CalculatorForm.TempItemNote;
                }
                txtArtikalNaziv.Text = tempArtikalNaziv;
                txtArtikalKolicina.Text = tempArtikalKolicina;
                txtArtikalCijena.Content = tempArtikalCijena + " KM";
                txtArtikalUkupno.Content = CalculatorForm.TempItemTotal.ToString("0.00") + " KM";

                gridCalculator.Visibility = System.Windows.Visibility.Visible;
                txtArtikalKolicina.Focus();
                txtArtikalKolicina.SelectionStart = txtArtikalKolicina.Text.Length;
                CalculatorForm.ClickedFirstTime = true;
            }
            catch
            { }
        }

        private void btnCalculatorInput_Click(object sender, RoutedEventArgs e)
        {
            string tempArtikalKolicina = string.Empty;
            Button b = (Button)sender;

            if (CalculatorForm.ClickedFirstTime == false)
            {
                tempArtikalKolicina = txtArtikalKolicina.Text;
            }
            CalculatorForm.ClickedFirstTime = false;

            if (b.Uid.ToString() == "-1")
            {
                if (tempArtikalKolicina != string.Empty)
                {
                    txtArtikalKolicina.IsTabStop = false;
                    txtArtikalKolicina.IsTabStop = true;
                    tempArtikalKolicina = tempArtikalKolicina.Remove(tempArtikalKolicina.Count() - 1, 1);
                }
            }
            else if (b.Uid.ToString() == ",")
            {
                if (!tempArtikalKolicina.Contains(b.Uid.ToString()))
                {
                    txtArtikalKolicina.IsTabStop = false;
                    txtArtikalKolicina.IsTabStop = true;
                    tempArtikalKolicina += b.Uid;
                }
            }
            else
            {
                txtArtikalKolicina.IsTabStop = false;
                txtArtikalKolicina.IsTabStop = true;
                tempArtikalKolicina += b.Uid;
            }

            txtArtikalKolicina.Text = tempArtikalKolicina;

            decimal cijena = Convert.ToDecimal(txtArtikalCijena.Content.ToString().Split(' ')[0]);
            decimal kolicina = tempArtikalKolicina == string.Empty ? 0 : Convert.ToDecimal(tempArtikalKolicina);
            txtArtikalUkupno.Content = (cijena * kolicina).ToString("0.00") + " KM";
        }

        private void gridCalculatorSaveCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = (Button)sender;

                if (b.Uid == "1")
                {
                    for (int i = 0; i < stackRacunStavke.Children.Count; i++)
                    {
                        Button b2 = (Button)stackRacunStavke.Children[i];
                        if (b2.Uid.ToString() == CalculatorForm.TempItemID)
                        {
                            decimal kolicina = Convert.ToDecimal(txtArtikalKolicina.Text);
                            OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total -= CalculatorForm.TempItemTotal;
                            OrdersForm.EditOrderItemQuantity(CalculatorForm.TempItemID, false, kolicina);
                            if (kolicina <= 0)
                            {
                                stackRacunStavke.Children.Remove(stackRacunStavke.Children[i]);
                            }
                            else
                            {
                                b2.Tag = OrdersForm.GetItemQuantityWithOrWithoutDecimals(kolicina);
                            }
                            lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;
                            if (txtArtikalNapomena.Text != string.Empty)
                            {
                                b2.ContentStringFormat = txtArtikalNapomena.Text;
                                b2.AllowDrop = true;
                            }
                            else
                            {
                                b2.ContentStringFormat = null;
                                b2.AllowDrop = false;
                            }
                            CalculatorForm.TempItemNote = txtArtikalNapomena.Text;
                            break;
                        }
                    }
                    gridCalculator.Visibility = Visibility.Collapsed;
                }
                else
                {
                    gridCalculator.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gridCalculatorPlusMinus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                decimal trenutnaKolicina = 0;
                decimal novaKolicina = 0;
                if (b.Uid == "1")
                {
                    txtArtikalKolicina.IsTabStop = false;
                    txtArtikalKolicina.IsTabStop = true;
                    trenutnaKolicina = txtArtikalKolicina.Text == null || txtArtikalKolicina.Text == string.Empty ? 0 : Convert.ToDecimal(txtArtikalKolicina.Text);
                    novaKolicina = trenutnaKolicina + 1;
                }
                else
                {
                    txtArtikalKolicina.IsTabStop = false;
                    txtArtikalKolicina.IsTabStop = true;
                    trenutnaKolicina = txtArtikalKolicina.Text == null || txtArtikalKolicina.Text == string.Empty ? 0 : Convert.ToDecimal(txtArtikalKolicina.Text);
                    novaKolicina = trenutnaKolicina <= 0 ? 0 : trenutnaKolicina - 1;

                }
                txtArtikalKolicina.Text = novaKolicina <= 0 ? "0" : novaKolicina.ToString();
                decimal cijena = Convert.ToDecimal(txtArtikalCijena.Content.ToString().Split(' ')[0]);
                txtArtikalUkupno.Content = (cijena * novaKolicina).ToString("0.00") + " KM";
            }
            catch (Exception)
            { }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,]+"); //regex that matches disallowed text
            e.Handled = regex.IsMatch(e.Text);
        }

        private void txtArtikalKolicina_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal kolicina = txtArtikalKolicina.Text == string.Empty ? 0 : Convert.ToDecimal(txtArtikalKolicina.Text);
            decimal cijena = Convert.ToDecimal(txtArtikalCijena.Content.ToString().Split(' ')[0]);
            txtArtikalUkupno.Content = (cijena * kolicina).ToString("0.00") + " KM";
        }

        private void txtArtikalKolicina_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnConfirm.Focus();
                btnConfirm.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        #endregion

        #region MANIPULISANJE SMJENAMA KORISNIKA 

        private void UserShift_Click(object sender, RoutedEventArgs e)
        {
            gridShiftLogin.Visibility = Visibility.Visible;
            txtShiftPIN.Focus();
        }

        private void OpenShiftClick(object sender, RoutedEventArgs e)
        {
            var user = ShiftForm.OpenUserShift(App.login_type == "PIN" ? txtShiftPIN.Password : txtShiftPassword.Password, App.login_type == "PIN" ? "" : txtShiftUserName.Text);

            if (user != null)
            {
                LoginShiftErrorMessage.Visibility = Visibility.Collapsed;
                LoginShiftSuccessMessage.Visibility = Visibility.Visible;
                LoginSuccessMessage.Content = "Prijava na smjenu uspješna.Dobrodošli," + user.Username;
                CallAnimationFadeOut();

                gridShiftLogin.Visibility = Visibility.Collapsed;
                return;
            }

            LoginShiftSuccessMessage.Visibility = Visibility.Collapsed;
            LoginShiftErrorMessage.Visibility = Visibility.Visible;
            LoginErrorMessage.Content = "Prijavljeni ste ili ste bili prijavljeni!";

            CallAnimationFadeOut();
        }

        private void CloseShiftClick(object sender, RoutedEventArgs e)
        {
            var user = ShiftForm.CloseUserShift(App.login_type == "PIN" ? txtShiftPIN.Password : txtShiftPassword.Password, App.login_type == "PIN" ? "" : txtShiftUserName.Text);

            if (user != null)
            {
                LoginShiftErrorMessage.Visibility = Visibility.Collapsed;
                LoginShiftSuccessMessage.Visibility = Visibility.Visible;
                LoginSuccessMessage.Content = "Odjava sa smjene uspješna.Doviđenja," + user.Username;

                CallAnimationFadeOut();

                gridShiftLogin.Visibility = Visibility.Collapsed;
                return;
            }

            LoginShiftSuccessMessage.Visibility = Visibility.Collapsed;
            LoginShiftErrorMessage.Visibility = Visibility.Visible;
            LoginErrorMessage.Content = "Zatvaranje smjene nije uspjelo!";

            CallAnimationFadeOut();
        }

        private void CloseForm(object sender, RoutedEventArgs e)
        {
            gridShiftLogin.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region SHIFT JOURNAL MANIPULACIJA

        private void btnShiftPinInput_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;

            if (b.Uid.ToString() == "-1")
            {
                if (txtShiftPIN.Password.Length > 0)
                    txtShiftPIN.Password = txtShiftPIN.Password.Remove(txtShiftPIN.Password.Length - 1);
            }
            else
                txtShiftPIN.Password += b.Uid;
        }

        private void CallAnimationFadeOut()
            {
            //animacija za fadeout
            var a = new DoubleAnimation
            {
                From = 1.0,
                To = 0.0,
                FillBehavior = FillBehavior.Stop,
                BeginTime = TimeSpan.FromSeconds(2),
                Duration = new Duration(TimeSpan.FromSeconds(1))
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(a);
            Storyboard.SetTarget(a, LoginShiftSuccessMessage);
            Storyboard.SetTarget(a, LoginShiftErrorMessage);
            Storyboard.SetTargetProperty(a, new PropertyPath(OpacityProperty));
            storyboard.Completed += delegate { LoginShiftSuccessMessage.Visibility = Visibility.Collapsed; };
            storyboard.Completed += delegate { LoginShiftErrorMessage.Visibility = Visibility.Collapsed; };
            storyboard.Begin();
            }

        /// <summary>
        /// Promjena vrijednosti na button-u "btnJournalActive"
        /// </summary>
        public void btnJournalActiv_Click(object sender, RoutedEventArgs e)
        {
            if (btnJournalActive.IsDefault)
            {
                if (JournalForm.AddJournal())
                {
                    btnJournalActive.IsDefault = !btnJournalActive.IsDefault;
                    btnShiftActive.IsEnabled = true;
                    btnShiftActive.Opacity = 1;
                    MessageBox.Show("Uspjesno otvoren dnevnik.");
                }
            else
            {
                    MessageBox.Show("Desila se greska pri dodavanju dnevnika.");
            }
        }
            else
        {
                if (JournalForm.CloseJournal())
            {
                    btnJournalActive.IsDefault = !btnJournalActive.IsDefault;
                    btnShiftActive.IsEnabled = false;
                    btnShiftActive.Opacity = 0.3;
                    MessageBox.Show("Uspjesno zatvoren dnevnik.");
            }
                else
                {
                    MessageBox.Show("Doslo je do greske prilikom zatvaranje dnevnika. Dali ste zatvorili sve smjene?");
        }
            }
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "btnShiftActive"
        /// </summary>
        public void btnShiftActiv_Click(object sender, RoutedEventArgs e)
        {
            if (btnShiftActive.IsDefault)
            {
                if (ShiftForm.AddShift())
                {
                    btnShiftActive.IsDefault = !btnShiftActive.IsDefault;
                    UserShift.IsEnabled = true;
                    MessageBox.Show("Uspjesno otvorena smjena.");
                }
                else
                {
                    MessageBox.Show("Doslo je do greske prilikom otvaranja nove smjene.");
                }
            }
            else
            {
                if (ShiftForm.CloseShift())
                {
                    btnShiftActive.IsDefault = !btnShiftActive.IsDefault;
                    UserShift.IsEnabled = false;
                    MessageBox.Show("Uspjesno zatvorena smjena.");
            }
            else
            {
                    MessageBox.Show("Doslo je do greske prilikom zatvaranja smjene. Dali su svi radnici zatvorili svoju smjenu?");
                }
            }
        }

        //Prikaz forme za otvaranje i zatvaranje dnevnika i smjene
        private void showJournalShiftForm_Click(object sender, RoutedEventArgs e)
        {
            gridJournalShift.Visibility = Visibility.Visible;
            }

        //Zatvaranje forme za otvaranje i zatvaranje dnevnika i smjene 
        private void CloseJournalShiftForm_Click(object sender, RoutedEventArgs e)
        {
            gridJournalShift.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region TABLES

        private void LoadTablesGrid()
        {
            //gridOrders.Visibility = Visibility.Collapsed;
            LoadAllActiveSectors();
            LoadTablesLabels();

        }

        private void LoadTablesLabels()
        {
            lblUserTables.Content = App.Current.username;
            lblUserGroupTables.Content = App.Current.userType;
            lblDateTimeTables.Content = DateTime.Now.ToString("dd.MM.yyyy.") + " " + DateTime.Now.ToString("HH:mm");
        }

        private void btnShowTables_Click(object sender, RoutedEventArgs e)
        {
            if (gridOpenedMenuSideBar.Visibility == Visibility.Visible)
            {
                btnMenuClose.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
            //gridOrders.Visibility = Visibility.Collapsed;
            LoadAllActiveSectors();
            gridTables.Visibility = Visibility.Visible;
        }

        private void LoadAllActiveSectors()
        {
            wrapSectors.Children.Clear();
            wrapTables.Children.Clear();
            List<SectorDM> sectors = TablesForm.SectorsGetAll();

            if (sectors.Count > 0)
            {
                foreach (var item in sectors)
                {
                    Button button = StyleHelper.CreateSectorButton(item.Name);
                    button.Style = (Style)this.Resources["SectorButtonStyle"];
                    button.Uid = item.SectorID.ToString();
                    button.Click += new RoutedEventHandler(btnSector_Click);
                    wrapSectors.Children.Add(button);

                }
                if (sectors.Count == 1)
                {
                    LoadAllTablesBySectorID(sectors.First().SectorID.ToString());
                }
                Button b = (Button)wrapSectors.Children[0];
                b.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        private void btnSector_Click(object sender, RoutedEventArgs e)
        {
            ClearCategorySelected(wrapSectors);
            Button b = (Button)sender;
            b.IsDefault = false;
            LoadAllTablesBySectorID(b.Uid);
        }

        private void LoadAllTablesBySectorID(string sectorID)
        {
            wrapTables.Children.Clear();
            List<TableDM> tables = TablesForm.TablesGetBySectorID(Convert.ToInt32(sectorID));

            Button button = new Button();
            foreach (var item in tables)
            {
                button = StyleHelper.CreateTableButton(item.Name, item.NumberOrder);
                button.Style = (Style)this.Resources["TableButtonStyle"];
                button.Uid = item.TableID.ToString();
                button.Click += new RoutedEventHandler(btnTable_Click);
                wrapTables.Children.Add(button);
            }
            if (!btnEditModeTables.IsDefault)
            {
                for (int i = 0; i < wrapTables.Children.Count; i++)
                {
                    Button b = (Button)wrapTables.Children[i];
                    b.AllowDrop = true;
                }
            }
        }

        private void btnEditModeTables_Click(object sender, RoutedEventArgs e)
        {
            btnEditModeTables.IsDefault = btnEditModeTables.IsDefault == true ? false : true;

            if (btnEditModeTables.IsDefault == false)
            {
                gridTablesEditMode.Visibility = Visibility.Visible;
                for (int i = 0; i < wrapTables.Children.Count; i++)
                {
                    Button b = (Button)wrapTables.Children[i];
                    b.AllowDrop = true;
                }
                //gridTablesRightMenu.Opacity = 0.3;
                //gridTablesRightMenu.IsEnabled = false;

                //CreateEditModeAddButton(wrapSectors);
                //CreateEditModeAddButton(wrapTables);
            }
            else
            {
                gridTablesEditMode.Visibility = Visibility.Collapsed;
                for (int i = 0; i < wrapTables.Children.Count; i++)
                {
                    Button b = (Button)wrapTables.Children[i];
                    b.AllowDrop = false;
                }
                //gridTablesRightMenu.Opacity = 1;
                //gridTablesRightMenu.IsEnabled = true;

                //RemoveEditModeAddButton(wrapSectors);
                //RemoveEditModeAddButton(wrapTables);
            }
        }

        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            stackTableOrders.Children.Clear();

            bool isEditMode = !btnEditModeTables.IsDefault;
            Button b = (Button)sender;
            if (isEditMode)
            {
                int index = wrapTables.Children.IndexOf(b);
                frmTablesAdd frm = new frmTablesAdd(Convert.ToInt32(b.Uid), true);
                this.Opacity = 0.5;
                var result = frm.ShowDialog();
                bool isSameSector = true;

                if (result == true)
                {
                    TableDM table = frm.addedEditedTable;
                    for (int i = 0; i < wrapSectors.Children.Count; i++)
                    {
                        Button b1 = (Button)wrapSectors.Children[i];
                        bool isDefaultButton = b1.IsDefault;

                        if (!isDefaultButton)
                        {
                            int sectorID = Int32.Parse(b1.Uid);
                            if (sectorID != table.SectorID)
                            {
                                isSameSector = false;
                            }
                        }
                    }

                    Button button = StyleHelper.CreateTableButton(table.Name, table.NumberOrder);
                    button.Style = (Style)this.Resources["TableButtonStyle"];
                    button.Uid = table.TableID.ToString();
                    button.Click += new RoutedEventHandler(btnTable_Click);
                    button.AllowDrop = true;

                    if (frm.isEdit)
                    {
                        if (isSameSector && frm.tableActive)
                        {
                            wrapTables.Children.RemoveAt(index);
                            wrapTables.Children.Insert(index, button);
                        }
                        else
                        {
                            wrapTables.Children.RemoveAt(index);
                        }
                    }
                    else
                    {
                        if (isSameSector)
                        {
                            wrapTables.Children.RemoveAt(index);
                            wrapTables.Children.Insert(index, button);
                            wrapTables.Children.Add(b);
                        }
                    }

                    wrapTables.UpdateLayout();
                }
                this.Opacity = 1;
            }
            else
            {
                gridTables.Visibility = Visibility.Collapsed;
                btnShowStol.Content = b.Content;
                OrdersForm.tableID = Convert.ToInt32(b.Uid);
                LoadActiveOrdersOnTable();
                LoadOrderRoundsOnTable();
                btnShowNarudzba.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            }
        }

        private void btnNewSector_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnNewTable_Click(object sender, RoutedEventArgs e)
        {
            int selectedSectorID = 0;
            for (int i = 0; i < wrapSectors.Children.Count; i++)
            {
                Button b = (Button)wrapSectors.Children[i];
                if (!b.IsDefault)
                    selectedSectorID = Int32.Parse(b.Uid);
            }

            frmTablesAdd frm = new frmTablesAdd(selectedSectorID);
            this.Opacity = 0.5;
            var result = frm.ShowDialog();
            bool isSameSector = false;
            if (result == true && frm.tableActive)
            {
                TableDM table = frm.addedEditedTable;
                for (int i = 0; i < wrapSectors.Children.Count; i++)
                {
                    Button b1 = (Button)wrapSectors.Children[i];
                    if (!b1.IsDefault)
                    {
                        if (b1.Uid == table.SectorID.ToString())
                        {
                            isSameSector = true;
                            break;
                        }
                    }
                }
                if (isSameSector)
                {
                    Button button = StyleHelper.CreateTableButton(table.Name, table.NumberOrder);
                    button.Style = (Style)this.Resources["TableButtonStyle"];
                    button.Uid = table.TableID.ToString();
                    button.Click += new RoutedEventHandler(btnTable_Click);
                    button.AllowDrop = true;
                    wrapTables.Children.Add(button);
                    button.BringIntoView();
                }

            }
            this.Opacity = 1;
        }

        private void btnNewSectorImage_Click(object sender, RoutedEventArgs e)
        {
            gridTablesEditModeImage.Visibility = gridTablesEditModeImage.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion

        #region TABLE ORDERS

        private void LoadOrderRoundsOnTable()
        {
            stackTableOrders.Children.Clear();
            int tableID = OrdersForm.tableID != null ? Convert.ToInt32(OrdersForm.tableID) : 0;
            List<RoundViewDM> rounds = OrdersCommunication.RoundsGetByTableId(tableID);
            OrdersForm.SetOrdersTotal(0);
            if (rounds.Count > 0)
            {
                OrdersForm.orderID = rounds[0].OrderID;
                decimal roundsTotal = 0;
                for (int i = 0; i < rounds.Count; i++)
                {
                    decimal roundAmount = Convert.ToDecimal(rounds[i].RoundAmount);
                    roundsTotal += roundAmount;
                    Button button = StyleHelper.CreateTableRoundButton(rounds[i].Number, roundAmount);
                    button.Style = (Style)this.Resources["OrderOnOrderListButtonStyle"];
                    button.Uid = rounds[i].RoundID.ToString();
                    button.Click += new RoutedEventHandler(btnTableOrderRound_Click);
                    stackTableOrders.Children.Add(button);
                }
                btnShowAllTableOrderItems.Visibility = Visibility.Visible;
                OrdersForm.ordersTotal += roundsTotal;
                OrdersForm.ordersTotalContent = OrdersForm.ordersTotal.ToString("0.00");
                btnShowAllTableOrderItems.Tag = OrdersForm.ordersTotal;
                lblTableOrdersTotal.Content = OrdersForm.ordersTotalContent;
                btnShowStol.AllowDrop = true;
                btnShowStol.ContentStringFormat = rounds.Count.ToString();
            }
            else
            {
                btnShowStol.AllowDrop = false;
                btnShowAllTableOrderItems.Visibility = Visibility.Collapsed;
                lblTableOrdersTotal.Content = OrdersForm.ordersTotalContent;
            }
        }

        private void LoadActiveOrdersOnTable()
        {
            int tableID = OrdersForm.tableID != null ? Convert.ToInt32(OrdersForm.tableID) : 0;
            List<OrdersForm.OrderItemVM> ordersOnTable = OrdersForm.orderItems.Where(x => x.TableID == tableID).ToList();

            stackRacunStavke.Children.Clear();
            if (ordersOnTable.Count > 0)
            {
                foreach (var orderItem in ordersOnTable)
                {
                    Button b = StyleHelper.CreateStavkaNaRacunuButton(orderItem.ItemID, orderItem.Name, orderItem.Quantity, orderItem.Price);
                    b.Style = (Style)this.Resources["ArtikalNaRacunuButtonStyle"];
                    stackRacunStavke.Children.Add(b);
                    b.BringIntoView();
                }
                lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;
            }
            else
            {
                lblUkupnoNarudzba.Content = OrdersForm.ResetOrderTotalOnActiveTable();
            }

        }

        private void btnBackToAllTableOrders_Click(object sender, RoutedEventArgs e)
        {
            gridOrdersOnTableOpenedOrder.Visibility = Visibility.Collapsed;
        }
        
        private void btnShowAllTableOrderItems_Click(object sender, RoutedEventArgs e)
        {
            gridOrdersOnTableOpenedOrder.Visibility = Visibility.Visible;

            List<RoundItemViewDM> roundItems = OrdersCommunication.RoundsItemsGetByOrderIDView(Convert.ToInt32(OrdersForm.orderID));

            stackTableOrderItems.Children.Clear();
            for (int i = 0; i < roundItems.Count; i++)
            {
                Button button = StyleHelper.CreateTableRoundItemButton(roundItems[i].ItemName, roundItems[i].Quantity);
                button.Style = (Style)this.Resources["OrderOnOrderListButtonStyle"];
                button.Uid = roundItems[i].ItemID.ToString();
                //button.Click += new RoutedEventHandler(btnTableOrderRound_Click);
                stackTableOrderItems.Children.Add(button);
                button.BringIntoView();
            }
            lblGridOpenedOrderOrderNumber.Content = (String)App.Current.FindResource("BtnTableOrderAllOrdersContent");
            lblTableOrderTotal.Content = btnShowAllTableOrderItems.Tag;
            //btnGridCheckoutConfirm.Tag = "2";
            OrdersForm.checkOutAllSelected = true;
            //btnGridCheckoutConfirm.Uid = "2";
        }

        private void btnCreateOrder_Click(object sender, RoutedEventArgs e)
        {
            if (OrdersForm.orderItems.Count > 0)
            {
                List<RoundViewDM> rounds = OrdersCommunication.RoundsGetByTableId(Convert.ToInt32(OrdersForm.tableID));
                if (rounds.Count == 0 || OrdersForm.orderID == null)
                {
                    OrderDM order = new OrderDM();
                    order.IsVIP = false;
                    order.ShiftID = 29;
                    order.TableID = OrdersForm.tableID;
                    order.OpenUserID = Convert.ToInt32(App.Current.userId);
                    OrdersForm.orderID = OrdersCommunication.OrdersInsert(order);
                }
                int orderID = Convert.ToInt32(OrdersForm.orderID);

                RoundDM round = new RoundDM();
                round.Number = OrdersCommunication.RoundsGetNextOrderNumber();
                round.UserID = Convert.ToInt32(App.Current.userId);
                round.OrderID = orderID;
                round.StationID = App.stationID;
                int roundID = OrdersCommunication.RoundsInsert(round);

                for (int i = 0; i < OrdersForm.orderItems.Count; i++)
                {
                    RoundItemDM roundItem = new RoundItemDM();
                    roundItem.RoundID = roundID;
                    roundItem.ItemID = OrdersForm.orderItems[i].ItemID;
                    roundItem.Quantity = OrdersForm.orderItems[i].Quantity;
                    OrdersCommunication.RoundsItemsInsert(roundItem);
                }
                OrdersForm.DeleteOrderOnActiveTable();
                decimal roundTotal = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total;
                btnShowStol.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                btnShowAllTableOrderItems.Visibility = Visibility.Visible;
                string ordersTotal = OrdersForm.AddTotalToOrdersTotal();
                btnShowAllTableOrderItems.Tag = ordersTotal;
                lblTableOrdersTotal.Content = ordersTotal;
                lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;

                Button button = StyleHelper.CreateTableRoundButton(round.Number, roundTotal);
                button.Style = (Style)this.Resources["OrderOnOrderListButtonStyle"];
                button.Uid = roundID.ToString();
                button.Click += new RoutedEventHandler(btnTableOrderRound_Click);
                stackTableOrders.Children.Add(button);
                button.BringIntoView();

                btnShowStol.AllowDrop = true;
                btnShowStol.ContentStringFormat = stackTableOrders.Children.Count.ToString();
                stackRacunStavke.Children.Clear();

                //OrdersForm.SetOrderTotal(0);
            }

        }

        private void btnTableOrderRound_Click(object sender, RoutedEventArgs e)
        {
            stackTableOrderItems.Children.Clear();
            Button b = (Button)sender;
            gridOrdersOnTableOpenedOrder.Visibility = Visibility.Visible;
            lblGridOpenedOrderOrderNumber.Content = b.ContentStringFormat;

            int roundID = Convert.ToInt32(b.Uid);
            List<RoundItemViewDM> roundItems = OrdersCommunication.RoundsItemsGetByRoundId(roundID);
            for (int i = 0; i < roundItems.Count; i++)
            {
                Button button = StyleHelper.CreateTableRoundItemButton(roundItems[i].ItemName, roundItems[i].Quantity);
                button.Style = (Style)this.Resources["OrderOnOrderListButtonStyle"];
                button.Uid = roundItems[i].ItemID.ToString();
                //button.Click += new RoutedEventHandler(btnTableOrderRound_Click);
                stackTableOrderItems.Children.Add(button);
                button.BringIntoView();
            }
            lblTableOrderTotal.Content = b.Tag;
            btnTableOrderCheckOut.Uid = b.Uid;
            //btnGridCheckoutConfirm.Tag = "1";
            OrdersForm.checkOutAllSelected = false;
        }

        #endregion

        #region GRID CHECKOUT

        private void btnGridCheckoutPaymentMethod_Click(object sender, RoutedEventArgs e)
        {
            int selected = 0;
            string selectedMethod = string.Empty;

            string firstPaymentMethodUid = string.Empty;
            string secondPaymentMethodUid = string.Empty;
            string firstPaymentMethodIcon = string.Empty;
            string secondPaymentMethodIcon = string.Empty;

            Button b = (Button)sender;
            b.AllowDrop = b.AllowDrop == true ? false : true;

            //for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
            //{
            //    Button button = (Button)stackGridCheckoutPaymentMethods.Children[i];
            //    if (button.AllowDrop == true && b.Content != button.Content)
            //    {
            //        button.AllowDrop = false;
            //    }
            //}

            selectedMethod = b.Content.ToString().ToUpper();
            btnGridCheckoutConfirm.Content = selectedMethod;



            //string selectedMethod = string.Empty;
            for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
            {
                Button button = (Button)stackGridCheckoutPaymentMethods.Children[i];
                if (button.AllowDrop == true)
                {
                    selected++;
                    selectedMethod = button.Content.ToString().ToUpper();
                    if (firstPaymentMethodIcon == string.Empty)
                    {
                        firstPaymentMethodIcon = button.Tag.ToString();
                        firstPaymentMethodUid = button.Uid.ToString();
                }
                    else
                    {
                        secondPaymentMethodIcon = button.Tag.ToString();
                        secondPaymentMethodUid = button.Uid.ToString();   
            }
                }
            }

            //Console.WriteLine("Način placanja 1: " + firstPaymentMethodIcon);
            //Console.WriteLine("Način placanja 2: " + secondPaymentMethodIcon);
            //Console.WriteLine("Način placanja 1 UID: " + firstPaymentMethodUid);
            //Console.WriteLine("Način placanja 2 UID: " + secondPaymentMethodUid);


            Button b1 = new Button();
            if (selected >= 2)
            {
                gridCheckoutMorePayMethods.Visibility = Visibility.Visible;
                
                string firstIcon = string.Empty;
                string secondIcon = b.Tag.ToString();
                string firstID = string.Empty;
                string secondID = b.Uid.ToString();
                string payMethods = string.Empty;
                for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
                {
                    b1 = (Button)stackGridCheckoutPaymentMethods.Children[i];
                    if (b1.AllowDrop == true && b1.Uid != b.Uid)
                    {
                        payMethods = b1.Content.ToString().ToUpper() + " / ";
                        firstIcon = b1.Tag.ToString();
                        firstID = b1.Uid.ToString();
                        break;
                    }

                }
                payMethods += b.Content.ToString().ToUpper();
                btnGridCheckoutConfirm.Content = payMethods;
                //txtGridCheckoutMorePayMethodsFirst.Tag = firstIcon;
                //txtGridCheckoutMorePayMethodsSecond.Tag = secondIcon;
                txtGridCheckoutMorePayMethodsFirst.Tag = firstPaymentMethodIcon;
                txtGridCheckoutMorePayMethodsSecond.Tag = secondPaymentMethodIcon;
                txtGridCheckoutMorePayMethodsFirst.Uid = firstPaymentMethodUid;
                txtGridCheckoutMorePayMethodsSecond.Uid = secondPaymentMethodUid;

                if (selected > 2)
                {
                    for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
                    {
                        Button b2 = (Button)stackGridCheckoutPaymentMethods.Children[i];
                        if (b2.AllowDrop == true && b2.Uid != b.Uid && b2.Uid != b1.Uid)
                        {
                            b2.AllowDrop = false;
                        }
                    }
                }
            }

            if (selected == 1)
            {
                gridCheckoutMorePayMethods.Visibility = Visibility.Collapsed;
                btnGridCheckoutConfirm.Content = selectedMethod;
            }

            BillsForm.NumberOfPaymentTypes = selected;
            GridCheckoutCheckAndShowDoubleGridsOnRight();
            //gridOrdersOnTableOpenedOrder.Visibility = Visibility.Visible;
        }

        private void GridCheckoutCheckAndShowDoubleGridsOnRight()
        {
            if (gridCheckoutDiscount.Visibility == Visibility.Visible && gridCheckoutMorePayMethods.Visibility == Visibility.Visible)
            {
                gridCheckoutDiscount.Margin = new Thickness(0);
                gridCheckoutDiscount.VerticalAlignment = VerticalAlignment.Top;
                gridCheckoutMorePayMethods.VerticalAlignment = VerticalAlignment.Top;
                gridCheckoutMorePayMethods.Margin = new Thickness(0, gridCheckoutDiscount.Height, 0, 0);
            }
            else
            {
                gridCheckoutDiscount.Margin = new Thickness(0, 10, 0, 0);
                gridCheckoutDiscount.VerticalAlignment = VerticalAlignment.Center;
                gridCheckoutMorePayMethods.VerticalAlignment = VerticalAlignment.Center;
                gridCheckoutMorePayMethods.Margin = new Thickness(0);
            }
        }

        private void btnTableOrderCheckOut_Click(object sender, RoutedEventArgs e)
        {
            if (stackTableOrderItems.Children.Count > 0)
            {
            gridCheckout.Visibility = Visibility.Visible;
                btnGridCheckoutConfirm.Tag = btnTableOrderCheckOut.Uid;
                lblGridCheckoutTotal.Content = lblTableOrderTotal.Content;
                txtGridCheckoutDiscountBillAmount.Content = lblGridCheckoutTotal.Content;
                stackGridCheckoutPaymentMethods.Children[0].RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

        }

        private void btnTableOrdersCheckOutAll_Click(object sender, RoutedEventArgs e)
        {
            if (stackTableOrders.Children.Count > 0)
            {
            gridCheckout.Visibility = Visibility.Visible;
                OrdersForm.checkOutAllSelected = true;
                //btnGridCheckoutConfirm.Uid = "2";
                lblGridCheckoutTotal.Content = lblTableOrdersTotal.Content;
                txtGridCheckoutDiscountBillAmount.Content = lblGridCheckoutTotal.Content;
                stackGridCheckoutPaymentMethods.Children[0].RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        private void btnCheckOut_Click(object sender, RoutedEventArgs e)
        {
            if (stackRacunStavke.Children.Count > 0)
            {
                lblGridCheckoutTotal.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total.ToString("0.00");
                txtGridCheckoutDiscountBillAmount.Content = lblGridCheckoutTotal.Content;
            gridCheckout.Visibility = Visibility.Visible;
                OrdersForm.checkOutAllSelected = null;
                stackGridCheckoutPaymentMethods.Children[0].RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }
        }

        private void btnGridCheckoutCancel_Click(object sender, RoutedEventArgs e)
        {
            if (btnGridCheckoutDiscount.AllowDrop)
                btnGridCheckoutDiscount.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            gridCheckout.Visibility = Visibility.Collapsed;
                for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
                {
                stackGridCheckoutPaymentMethods.Children[i].AllowDrop = false;
        }
            txtGridCheckoutMorePayMethodsFirst.Text = "0,00";
            txtGridCheckoutMorePayMethodsSecond.Text = "0,00";
        }

        private void txtGridCheckoutMorePayMethodsFirst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtGridCheckoutMorePayMethodsFirst.Focus();
                //decimal total = OrdersForm.orderTotal;
                decimal total = Convert.ToDecimal(lblGridCheckoutTotal.Content);
                decimal paymentMethodFirst = Convert.ToDecimal(txtGridCheckoutMorePayMethodsFirst.Text);
                decimal paymentMethodSecond = Convert.ToDecimal(txtGridCheckoutMorePayMethodsSecond.Text);

                if (paymentMethodFirst < total)
                {
                    txtGridCheckoutMorePayMethodsFirst.Text = paymentMethodFirst.ToString("0.00");
                    txtGridCheckoutMorePayMethodsSecond.Text = (total - paymentMethodFirst).ToString("0.00");
                }
                else
                {
                    txtGridCheckoutMorePayMethodsFirst.Text = "0,00";
                    txtGridCheckoutMorePayMethodsSecond.Text = "0,00";
                    MessageBox.Show("Unesene vrijednosti nisu ispravne!");
                }
            }
        }

        private void txtGridCheckoutMorePayMethodsSecond_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtGridCheckoutMorePayMethodsFirst.Focus();
                //decimal total = OrdersForm.orderTotal;
                decimal total = Convert.ToDecimal(lblGridCheckoutTotal.Content);
                decimal paymentMethodFirst = Convert.ToDecimal(txtGridCheckoutMorePayMethodsFirst.Text);
                decimal paymentMethodSecond = Convert.ToDecimal(txtGridCheckoutMorePayMethodsSecond.Text);

                if (paymentMethodSecond < total)
                {
                    txtGridCheckoutMorePayMethodsFirst.Text = (total - paymentMethodSecond).ToString("0.00");
                    txtGridCheckoutMorePayMethodsSecond.Text = paymentMethodSecond.ToString("0.00");
                }
                else
                {
                    txtGridCheckoutMorePayMethodsFirst.Text = "0,00";
                    txtGridCheckoutMorePayMethodsSecond.Text = "0,00";
                    MessageBox.Show("Unesene vrijednosti nisu ispravne!");                    
                }
            }
        }

        private void btnGridCheckoutDiscount_Click(object sender, RoutedEventArgs e)
        {
            btnGridCheckoutDiscount.AllowDrop = gridCheckoutDiscount.Visibility == Visibility.Visible ? false : true;
            gridCheckoutDiscount.Visibility = gridCheckoutDiscount.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            lblGridCheckoutTotal.Content = txtGridCheckoutDiscountBillAmount.Content;
            int zero = 0;
            string zeroString = zero.ToString("0.00");
            txtGridCheckoutDiscountEnter.Text = zeroString;
            txtGridCheckoutDiscount.Content = "0";
            txtGridCheckoutDiscountAmount.Content = zeroString;
            GridCheckoutCheckAndShowDoubleGridsOnRight();
        }

        private void txtGridCheckoutDiscountEnter_KeyDown(object sender, KeyEventArgs e)
        {
            string oldDiscount = txtGridCheckoutDiscountEnter.Text;

            if (e.Key == Key.Enter)
            {
                txtGridCheckoutDiscountEnter.Focus();
                decimal newDiscount = Convert.ToDecimal(txtGridCheckoutDiscountEnter.Text);
                if (newDiscount > 99)
            {
                    txtGridCheckoutDiscountEnter.Text = oldDiscount;
            }
            else
                {
                    txtGridCheckoutDiscount.Content = txtGridCheckoutDiscountEnter.Text;
                    decimal discountAmount = 0;
                    decimal price = Convert.ToDecimal(txtGridCheckoutDiscountBillAmount.Content);
                    //decimal price = Convert.ToDecimal(lblGridCheckoutTotal.Content);
                    decimal discount = Convert.ToDecimal(txtGridCheckoutDiscount.Content);
                    discountAmount = price * discount / 100;
                    txtGridCheckoutDiscountAmount.Content = discountAmount.ToString("0.00");
                    lblGridCheckoutTotal.Content = (price - discountAmount).ToString("0.00");
                }
            }
        }

        #endregion

        #region RAČUNI

        private void MakeBill(object sender, RoutedEventArgs e)
        {
            if (BillsForm.NumberOfPaymentTypes <= 0)
            {
                MessageBox.Show("Odaberite način plaćanja!");
                return;
        }
            int roundID = 0;
            int orderID = 0;
            if (OrdersForm.checkOutAllSelected == null)
            {
                if (stackRacunStavke.Children.Count > 0)
                {
                    decimal discount = 0;
                    decimal Quantity = 0;

                    for (int i = 0; i < stackRacunStavke.Children.Count; i++)
            {
                        Button b = (Button)stackRacunStavke.Children[i];

                        int ItemID = Convert.ToInt32(b.Uid);
                        Quantity = Convert.ToDecimal(b.Tag);

                        BillsForm.AddBillItem(ItemID, Quantity, discount);
                }

                }
            }
            else if (OrdersForm.checkOutAllSelected == false)
            {
                roundID = Convert.ToInt32(btnGridCheckoutConfirm.Tag);
                List<RoundItemViewDM> roundItems = OrdersCommunication.RoundsItemsGetByRoundId(roundID);
                if (roundItems.Count > 0)
            {
                    decimal discount = 0;
                    //decimal Quantity = 0;
                    for (int i = 0; i < roundItems.Count; i++)
                {
                        //Button b = (Button)stackRacunStavke.Children[i];

                        //int ItemID = Convert.ToInt32(b.Uid);
                        //Quantity = Convert.ToDecimal(b.Tag);

                        BillsForm.AddBillItem(roundItems[i].ItemID, roundItems[i].Quantity, discount);
            }
        }

            }
            else if (OrdersForm.checkOutAllSelected == true)
        {
                orderID = Convert.ToInt32(OrdersForm.orderID);
                List<RoundItemViewDM> roundItems = OrdersCommunication.RoundsItemsGetByOrderIDView(Convert.ToInt32(OrdersForm.orderID));
                if (roundItems.Count > 0)
            {
                    decimal discount = 0;
                    //decimal Quantity = 0;
                    for (int i = 0; i < roundItems.Count; i++)
                {
                        //Button b = (Button)stackRacunStavke.Children[i];

                        //int ItemID = Convert.ToInt32(b.Uid);
                        //Quantity = Convert.ToDecimal(b.Tag);

                        BillsForm.AddBillItem(roundItems[i].ItemID, roundItems[i].Quantity, discount);
                    }
                }
                }
            if (BillsForm.NumberOfPaymentTypes > 1)
            {
                decimal total = Convert.ToDecimal(lblGridCheckoutTotal.Content);
                decimal paymentType_1 = Convert.ToDecimal(txtGridCheckoutMorePayMethodsFirst.Text);
                decimal paymentType_2 = Convert.ToDecimal(txtGridCheckoutMorePayMethodsSecond.Text);

                if (total != (paymentType_1 + paymentType_2))
                {
                    MessageBox.Show("Unesite ispravne vrijednosti za pojedinačne načine plaćanja!");
                    return;
                }
            }

            List<Payment> fiskPayments = new List<Payment>();
            int FiscalNumber = 0;
            for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
            {
                Button b = (Button)stackGridCheckoutPaymentMethods.Children[i];
                if (b.AllowDrop)
            {
                    if (BillsForm.NumberOfPaymentTypes == 1)
                {
                        fiskPayments.Add(BillsForm.CreatePaymentType(b.Content.ToString(), 0));
                }
                else
                {
                        //txtGridCheckoutMorePayMethodsFirst
                        decimal amountTemp = 0;

                        if (b.Uid == txtGridCheckoutMorePayMethodsFirst.Uid)
                            amountTemp = Convert.ToDecimal(txtGridCheckoutMorePayMethodsFirst.Text);
                        else
                            amountTemp = Convert.ToDecimal(txtGridCheckoutMorePayMethodsSecond.Text);

                        fiskPayments.Add(BillsForm.CreatePaymentType(b.Content.ToString(), amountTemp));
                }
            }
        }

            BillsForm.MapPaymentTypes(fiskPayments);
            decimal Discount = Convert.ToDecimal(txtGridCheckoutDiscountEnter.Text.ToString());

            if (Discount != 0)
            {
                foreach (var item in BillsForm.Articles)
        {
                    item.Discount = Discount;
                }
        }

            FiscalNumber = BillsForm.PrintBill(BillsForm.Articles, BillsForm.Amount, Discount, fiskPayments);

            if (FiscalNumber != 0)
        {
                BillsForm.CreateBill(FiscalNumber, orderID, roundID);

                if (OrdersForm.checkOutAllSelected == null)
        {
                    OrdersForm.orderItems = new List<OrdersForm.OrderItemVM>();
                    stackRacunStavke.Children.Clear();
                    OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total = 0;
                    OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).Total.ToString("0.00");
                    lblUkupnoNarudzba.Content = OrdersForm.orderTotals.Find(x => x.TableID == OrdersForm.tableID).TotalString;
                    FiscalNumber = 0;
                }
                else if (OrdersForm.checkOutAllSelected == false)
            {
                    stackTableOrders.Children.Clear();
                    btnBackToAllTableOrders.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    LoadOrderRoundsOnTable();
                }
                else if (OrdersForm.checkOutAllSelected == true)
                {
                    btnShowAllTableOrderItems.Visibility = Visibility.Collapsed;
                    stackTableOrders.Children.Clear();
                    btnBackToAllTableOrders.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    LoadOrderRoundsOnTable();
                }
                ClearValues();
                ResetPaymentMethods();
            }
                }

        public void ClearValues()
                {
            gridCheckout.Visibility = Visibility.Collapsed;
            gridCheckoutDiscount.Visibility = System.Windows.Visibility.Collapsed;
            gridCheckoutMorePayMethods.Visibility = System.Windows.Visibility.Collapsed;
            btnGridCheckoutDiscount.AllowDrop = false;
            txtGridCheckoutDiscountEnter.Text = "0";
            txtGridCheckoutDiscount.Content = "0";
            txtGridCheckoutDiscountAmount.Content = "0";
            txtGridCheckoutMorePayMethodsFirst.Text = "0,00";
            txtGridCheckoutMorePayMethodsSecond.Text = "0,00";
            txtGridCheckoutMorePayMethodsFirst.Uid = string.Empty;
            txtGridCheckoutMorePayMethodsSecond.Uid = string.Empty;
            BillsForm.NumberOfPaymentTypes = 1;
                }

        public void ResetPaymentMethods()
        {
            for (int i = 0; i < stackGridCheckoutPaymentMethods.Children.Count; i++)
            {
                stackGridCheckoutPaymentMethods.Children[i].AllowDrop = false;
            }
        }

        #endregion
    }
}
