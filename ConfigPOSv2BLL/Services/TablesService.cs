﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class TablesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novom stolu
        /// </summary>
        /// <param name="table">TABLEDM objekat</param>
        /// <returns>ID dodanog stola</returns>
        public int TableInsert(TableDM table)
        {
            OpenConnection();
            int addedTableId = Convert.ToInt32(entity.stp_TABLES_Insert(table.Name, table.NumberOrder, table.SectorID, table.Type, table.GraphX, table.GraphY, table.Rotation,table.Deleted).FirstOrDefault().Value);
            CloseConnection();

            return addedTableId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o stolu na osnovu proslijeđenog ID stola
        /// </summary>
        /// <param name="table">TABLEDM objekat</param>
        public void TableUpdate(TableDM table)
        {
            OpenConnection();
            entity.stp_TABLES_Update(table.TableID, table.Name, table.NumberOrder, table.SectorID, table.Type, table.GraphX, table.GraphY, table.Rotation, table.Deleted);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja radi update podataka o stolu na osnovu proslijeđenog ID stola
        /// </summary>
        /// <param name="table">TABLEDM objekat</param>
        public void TableUpdateDeleted(int tableID, bool deleted)
        {
            OpenConnection();
            entity.stp_TABLES_UpdateDeleted(tableID, deleted);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja briše podatke o stolu na osnovu proslijeđenog ID stola
        /// </summary>
        /// <param name="tableID"></param>
        public void TableDelete(int tableID)
        {
            OpenConnection();
            entity.stp_TABLES_Delete(tableID);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih stolova
        /// </summary>
        /// <returns>Lista stolova</returns>
        public List<TableDM> TablesGetAll()
        {
            OpenConnection();
            List<TableDM> list = entity.stp_TABLES_GetAll().ToList().MapTableListToTableDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća stol na osnovu prosljeđenog ID stola
        /// </summary>
        /// <param name="CategoryID">ID kategorije</param>
        /// <returns>Kategorija</returns>
        public TableDM TablesGetByID(int tableID)
        {
            OpenConnection();
            TableDM table = entity.stp_TABLES_GetByID(tableID).FirstOrDefault().MapTablesToTableDM();
            CloseConnection();

            return table;
        }

        /// <summary>
        /// Funkcija koja vraća listu svih stolova na osnovu prosljeđenog ID sektora
        /// </summary>
        /// <param name="sectorID">ID sektora</param>
        /// <returns>Listu stolova</returns>
        public List<TableDM> TablesGetBySectorID(int sectorID)
        {
            OpenConnection();
            List<TableDM> list = entity.stp_TABLES_GetBySectorID(sectorID).ToList().MapTableListToTableDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća naredni broj stola za dodavanje novog stola
        /// </summary>
        /// <returns>Broj stola</returns>
        public int TablesGetNextNumberOrderBySectorID(int sectorID)
        {
            OpenConnection();
            int nextNumber = Convert.ToInt32(entity.stp_TABLES_GetNextNumberOrderBySectorID(sectorID).FirstOrDefault().Value);
            CloseConnection();

            return nextNumber;
        }
    }
}
