﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class RoundMapper
    {
        public static RoundViewDM MapRoundsViewToRoundViewDM(this ROUNDS_VIEW item)
        {
            if (item == null)
                return null;

            RoundViewDM Model = new RoundViewDM()
            {
                Number = item.Number,
                OrderID = item.OrderID,
                RoundAmount = item.RoundAmount,
                RoundID = item.RoundID,
                ShiftID = item.ShiftID
            };

            return Model;
        }

        public static List<RoundViewDM> MapRoundsViewListToRoundViewDMList(this List<ROUNDS_VIEW> items)
        {
            if (items == null)
                return null;

            List<RoundViewDM> Models = new List<RoundViewDM>();

            foreach (var item in items)
            {
                Models.Add(new RoundViewDM()
                {
                    Number = item.Number,
                    OrderID = item.OrderID,
                    RoundAmount = item.RoundAmount,
                    RoundID = item.RoundID,
                    ShiftID = item.ShiftID
                });
            }

            return Models;
        }

    }
}
