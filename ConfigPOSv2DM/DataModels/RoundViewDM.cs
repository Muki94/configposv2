﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class RoundViewDM
    {
        public int OrderID { get; set; }

        public int ShiftID { get; set; }

        public Nullable<int> TableID { get; set; }

        public int RoundID { get; set; }

        public int Number { get; set; }

        public decimal? RoundAmount { get; set; }
    }
}
