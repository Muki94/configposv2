﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class ItemTaxesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public List<ItemTaxesViewDM> ItemTaxesGetByItemID(int ItemID)
        {
            OpenConnection();
            List<ItemTaxesViewDM> itemTaxes = entity.stp_ITEM_TAXES_GetByItemID(ItemID).ToList().MapItemTaxesListToItemTaxesViewDMList();
            CloseConnection();
            return itemTaxes;
        }
    }
}
