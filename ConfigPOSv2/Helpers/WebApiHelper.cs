﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using ConfigPOSv2DM.DataModels;

namespace ConfigPOSv2.Helpers
{
    public static class WebApiHelper
    {
        private static HttpResponseMessage result = new HttpResponseMessage();
        private static string baseAddress = Properties.Settings.Default.BASE_URL;
        private static string resultContent = "";

        public static void RefreshToken()
        {
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "refresh_token"), 
                new KeyValuePair<string, string>("refresh_token", App.Current.refresh_token)
            });

            var result = WebApiHelper.GetToken<object>(content);

            App.Current.access_token = (string)((JValue)((JObject)result)["access_token"]).Value;
            App.Current.refresh_token = (string)((JValue)((JObject)result)["refresh_token"]).Value;
        }

        public static object SetToken(string password, string username = "")
        {
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            });

            var result = WebApiHelper.GetToken<object>(content);

            //izvlacenje svih potrebnih informacija iz tokena
            if (!result.ToString().Contains("invalid_grant"))
            {
                App.Current.access_token = (string)((JValue)((JObject)result)["access_token"]).Value;
                App.Current.refresh_token = (string)((JValue)((JObject)result)["refresh_token"]).Value;
                App.Current.username = (string)((JValue)((JObject)result)["username"]).Value;
                App.Current.userType = (string)((JValue)((JObject)result)["userGroup"]).Value;
                App.Current.userId = (string)((JValue)((JObject)result)["userId"]).Value;
            }
            else
                result = null;

            return result;
        }

        //GET TOKEN
        public static T GetToken<T>(HttpContent content)
        {
            using (HttpClient client = new HttpClient())
            {
                //postavljanje adrese za poziv token rute 
                client.BaseAddress = new Uri(baseAddress + "token");
                HttpResponseMessage result = client.PostAsync(client.BaseAddress, content).Result;
                resultContent = result.Content.ReadAsStringAsync().Result;
                 return (T)(JsonConvert.DeserializeObject(resultContent));
            }
        }

        public static T Get<T>(string apiRoute, FilterAttributesDM parameters = null)
        {
            using (HttpClient client = new HttpClient())
            {
                string aditionalParameters = "";

                if (parameters != null)
                {
                    //Filter po kojem dobavljamo jedan item
                    aditionalParameters += "idName=" + parameters.idName;
                    aditionalParameters += "&userId=" + parameters.userId;
                    aditionalParameters += "&username=" + parameters.username;
                    aditionalParameters += "&password=" + parameters.password;
                
                    //Dodavanje dodatnih parametara za filtriranje
                    //aditionalParameters += "&parameter2=" + parameters.Parameter2;
                    //aditionalParameters += "&parameter3=" + parameters.Parameter3;
                    //aditionalParameters += "&parameter4=" + parameters.Parameter4;
                }

                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute + "?" + aditionalParameters);

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.GetAsync(client.BaseAddress).Result;

                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.GetAsync(client.BaseAddress).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T);
            }

            return JsonConvert.DeserializeObject<T>(resultContent);
        }

        //POST
        public static T1 Post<T1,T2>(string apiRoute, T2 content = default(T2))
        {
            using (HttpClient client = new HttpClient())
            {
                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute);

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                var jsonContent = JsonConvert.SerializeObject(content);
                var buffer = System.Text.Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.PostAsync(client.BaseAddress, byteContent).Result;

                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.PostAsync(client.BaseAddress, byteContent).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T1);
            }

            return JsonConvert.DeserializeObject<T1>(resultContent);
        }

        //POST WITHOUT PARAMETERS
        public static T1 Post<T1>(string apiRoute)
        {
            using (HttpClient client = new HttpClient())
            {
                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute);

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.PostAsync(client.BaseAddress, null).Result;

                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.PostAsync(client.BaseAddress, null).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T1);
            }

            return JsonConvert.DeserializeObject<T1>(resultContent);
        }

        //PUT
        public static T1 Put<T1,T2>(string apiRoute, T2 content)
        {
            using (HttpClient client = new HttpClient())
            {
                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute);

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                var jsonContent = JsonConvert.SerializeObject(content);
                var buffer = System.Text.Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.PutAsync(client.BaseAddress, byteContent).Result;

                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.PutAsync(client.BaseAddress, byteContent).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T1);
            }

            return JsonConvert.DeserializeObject<T1>(resultContent);
        }

        //PUT WITHOUT PARAMETERS
        public static T Put<T>(string apiRoute)
        {
            using (HttpClient client = new HttpClient())
            {
                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute);

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.PutAsync(client.BaseAddress, null).Result;
                 
                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.PutAsync(client.BaseAddress, null).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T);
            }

            return JsonConvert.DeserializeObject<T>(resultContent);
        }

        //DELETE
        public static T Delete<T>(string apiRoute, int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //Kreiranje rute za poziv prema API-u
                client.BaseAddress = new Uri(baseAddress + apiRoute + "/" + id.ToString());

                //Postavljanje access tokena u header
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);

                //Pokusaj poziva API-a, u slucaju da nije uspjesno inicira se refresh token
                result = client.DeleteAsync(client.BaseAddress).Result;

                if (result.StatusCode.ToString().Equals("Unauthorized"))
                {
                    RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", App.Current.access_token);
                    result = client.DeleteAsync(client.BaseAddress).Result;
                }

                if (result.IsSuccessStatusCode)
                    resultContent = result.Content.ReadAsStringAsync().Result;
                else
                    return default(T);
            }

            return JsonConvert.DeserializeObject<T>(resultContent);
        }
    }
}