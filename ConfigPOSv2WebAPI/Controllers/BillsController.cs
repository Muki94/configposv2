﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System.Net;

namespace ConfigPOSv2WebAPI.Controllers
{

    public class BillsController : ApiController
    {

        BillsService Bills = new BillsService();

        [Route("api/BILLS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertBills([FromBody] BillsDM bill)
        {
            try
            {
                int addedBillID = Bills.BillsInsert(bill);
                return Request.CreateResponse(HttpStatusCode.OK, addedBillID);
            }
            catch (Exception e)
            {
               return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [Route("api/BILLS/GetLastNumber")]
        [HttpGet]
        public HttpResponseMessage GetLastNumber()
        {
            try
            {
                int billNumber = Bills.BillsGetLastNumber();
                return Request.CreateResponse(HttpStatusCode.OK, billNumber);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,e.Message);
            }
        }
    }
}