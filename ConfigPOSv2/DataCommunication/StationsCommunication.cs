﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class StationsCommunication
    {
        private static StationsService stationsService = new StationsService();

        public static StationsDM StationsGetByUserID(string UserID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<StationsDM>("api/STATIONS/GetByUserID", new FilterAttributesDM { idName = UserID });
            else
                return stationsService.StationsGetByUserID(Convert.ToInt32(UserID));
        }
    }       
}
