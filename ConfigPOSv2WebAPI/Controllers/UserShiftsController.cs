﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class UserShiftsController : ApiController
    {
        UserShiftsService userShiftService = new UserShiftsService();

        // GET
        [Route("api/USERSHIFTS/Get")]
        [HttpGet]
        public HttpResponseMessage GetUserShift([FromUri]FilterAttributesDM filters)
        {
            try
            {
                UserShiftDM userShiftModel = userShiftService.GetUserShift(filters.userId,Int32.Parse(filters.idName));
                return Request.CreateResponse(HttpStatusCode.OK, userShiftModel);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET
        [Route("api/USERSHIFTS/GetNotClosedUserShiftsByShiftId")]
        [HttpGet]
        public HttpResponseMessage GetNotClosedUserShiftsByShiftId([FromUri]FilterAttributesDM filters)
        {
            try
            {
                List<UserShiftDM> userShiftModels = userShiftService.GetNotClosedUserShiftsByShiftId(Int32.Parse(filters.idName));
                return Request.CreateResponse(HttpStatusCode.OK, userShiftModels);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/USERSHIFTS/Insert")]
        [HttpPost]
        public HttpResponseMessage UserShiftsInsert([FromBody]UserShiftDM userShift)
        {
            try
            {
                int addedUserShiftId = userShiftService.UserShiftInsert(userShift);
                return Request.CreateResponse(HttpStatusCode.OK, addedUserShiftId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // CLOSE
        [Route("api/USERSHIFTS/Close")]
        [HttpPut]
        public HttpResponseMessage UserShiftsClose([FromBody]UserShiftDM userShift)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, userShiftService.UserShiftClose(userShift));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
