﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class BillsDM
    {
        public int BillID { get; set; }
        public int Number { get; set; }
        public string FullBillNumber { get; set; }
        public Nullable<int> OrderID { get; set; }
        public int CreateUserID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int StationID { get; set; }
        public bool Canceled { get; set; }
        public Nullable<int> CancelUserID { get; set; }
        public Nullable<System.DateTime> CancelTime { get; set; }
        public string CancelingReason { get; set; }
        public Nullable<int> FiscalNumber { get; set; }
        public decimal Total { get; set; }
        public string ProtectionCode { get; set; }
        public string BillUniqueIdentifier { get; set; }
        public Nullable<decimal> Tip { get; set; }
    }
}
