﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class OrdersService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja vraca narudzbe na osnovu Id smjene
        /// </summary>

        public List<OrderDM> OrderGetAllByShiftId(int shiftId)
        {
            OpenConnection();
            List<OrderDM> list = entity.stp_ORDERS_GetAllByShiftId(shiftId).ToList().MapORDERSListToOrderDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraca narudzbe na osnovu Id smjene i Id korisnika
        /// </summary>

        public List<OrderDM> OrderGetAllByShiftIdAndUserId(int shiftId, int userId)
        {
            OpenConnection();
            List<OrderDM> list = entity.stp_ORDERS_GetAllByShiftId(shiftId).Where(x => x.OpenUserID == userId).ToList().MapORDERSListToOrderDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novoj narudžbi
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int OrderInsert(OrderDM order)
        {
            OpenConnection();
            int addedOrderId = Convert.ToInt32(entity.stp_ORDERS_Insert(order.ShiftID, order.TableID, order.IsVIP, order.OpenUserID).FirstOrDefault().Value);
            CloseConnection();

            return addedOrderId;
        }

        public void OrdersUpdateClosed(int orderID, int closeUserID)
        {
            OpenConnection();
            entity.stp_ORDERS_UpdateClosed(orderID, closeUserID);
            CloseConnection();
        }
    }
}
