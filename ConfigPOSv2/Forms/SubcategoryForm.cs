﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.Forms
{
    public static class SubcategoryForm
    {
        public static SubcategoryDM AddEditSubcategory(string name, bool active, int categoryId) 
        {
            if (name == null || name == String.Empty)
                return null;

            SubcategoryDM subcategoryModel = new SubcategoryDM()
            {
                Name = name,
                Active = active,
                CategoryID = categoryId
            };

            subcategoryModel.SubcategoryID = SubcategoriesCommunication.SubcategoriesInsert(subcategoryModel);

            return subcategoryModel;
        }
    }
}
