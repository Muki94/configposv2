﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class RoundsItemsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }
        public int RoundItemInsert(RoundItemDM item)
        {
            OpenConnection();
            int addedOrderId = Convert.ToInt32(entity.stp_ROUNDS_ITEMS_Insert(item.RoundID, item.ItemID, item.Quantity, item.ProductionUnitID).FirstOrDefault().Value);
            CloseConnection();

            return addedOrderId;
        }
        public List<RoundItemViewDM> RoundsItemsGetByRoundID(int roundID)
        {
            OpenConnection();
            List<RoundItemViewDM> roundItems = entity.stp_ROUNDS_ITEMS_GetByRoundID(roundID).ToList().MapRoundsItemsViewListToRoundItemViewDMList();
            CloseConnection();

            return roundItems;
        }
        public List<RoundItemViewDM> RoundsItemsGetByOrderIDView(int orderID)
        {
            OpenConnection();
            List<RoundItemViewDM> roundItems = entity.stp_ROUNDS_ITEMS_GetByOrderID_View(orderID).ToList().MapRoundsItemsByOrderListToRoundItemViewDMList();
            CloseConnection();

            return roundItems;
        }
        public List<RoundItemViewDM> RoundsItemsGetByOrderID(int orderID)
        {
            OpenConnection();
            List<RoundItemViewDM> roundItems = entity.stp_ROUNDS_ITEMS_GetByOrderID(orderID).ToList().MapRoundsItemsSimpleListToRoundItemViewDMList();
            CloseConnection();

            return roundItems;
        }
    }
}
