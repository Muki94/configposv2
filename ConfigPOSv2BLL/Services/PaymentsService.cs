﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class PaymentsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public int PaymentsInsert(PaymentsDM payment)
        {
            OpenConnection();
            int paymentID = Convert.ToInt32(entity.stp_PAYMENTS_Insert(payment.BillID, payment.PaymentTypeID, payment.Amount, payment.Tip).FirstOrDefault());
            CloseConnection();
            return paymentID;
        }
    }
}
