﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace ConfigPOSv2.Helpers
{
    public class Hash
    {
        public enum HashType : int
        {
            /// <summary>MD5 Hashing,128bit kriptograska funkcija</summary>
            MD5,
            /// <summary>SHA1 Hashing,512bit NASA kriptograska funkcija</summary>
            SHA1,
            /// <summary>SHA256 Hashing,256bit NASA kriptograska funkcija</summary>
            SHA256,
            /// <summary>SHA384 Hashing,384bit NASA kriptograska funkcija</summary>
            SHA384,
            /// <summary>SHA512 Hashing,512bit NASA kriptograska funkcija</summary>
            SHA512

        }


        public Hash() { }

        public static string GetHash(string strPlain, HashType hshType)
        {
            string strRet;

            switch (hshType)
            {
                case HashType.MD5:
                    strRet = GetMD5(strPlain);
                    break;
                case HashType.SHA1:
                    strRet = GetSHA1(strPlain);
                    break;
                case HashType.SHA256:
                    strRet = GetSHA256(strPlain);
                    break;
                case HashType.SHA384:
                    strRet = GetSHA384(strPlain);
                    break;
                case HashType.SHA512:
                    strRet = GetSHA512(strPlain);
                    break;
                default:
                    strRet = "Invalid HasType";
                    break;

            }
            return strRet;
        }

        public static bool CheckHash(string strOrginal, string strHash, HashType hshType)
        {
            string strOrigHas = GetHash(strOrginal, hshType);
            return (strOrigHas == strHash);
        }

        private static string GetMD5(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            MD5 md5 = new MD5CryptoServiceProvider();
            string strHex = string.Empty;
            HashValue = md5.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);

            }
            return strHex;

        }//Get MD5

        private static string GetSHA1(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA1Managed SHhash = new SHA1Managed();
            string strHex = string.Empty;

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        }//Get SHA1

        private static string GetSHA256(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA256Managed SHhash = new SHA256Managed();
            string strHex = string.Empty;

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } // GetSHA256 

        private static string GetSHA384(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA384Managed SHhash = new SHA384Managed();
            string strHex = string.Empty;

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } // GetSHA384 

        private static string GetSHA512(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA512Managed SHhash = new SHA512Managed();
            string strHex = string.Empty;

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } // GetSHA512 
    }
}
