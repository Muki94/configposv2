﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
	/// <summary>
	/// Interaction logic for frmCategoriesAddMain.xaml
	/// </summary>
	public partial class frmCategoriesAddMain : Window
	{
        public CategoryDM addedEditedCategory;

		public frmCategoriesAddMain()
		{
			this.InitializeComponent();

            txtCategoryName.Focus();        
		}

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivan"
        /// </summary>
        public void btnActiv_Click(object sender, RoutedEventArgs e)
        {
            btnActiv.IsDefault = !btnActiv.IsDefault;
        }

        /// <summary>
        /// Funkcija koja sprema novu kategoriju u bazu podataka
        /// </summary>
        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.addedEditedCategory = CategoryForm.AddEditCategory(txtCategoryName.Text, !btnActiv.IsDefault);

            if (this.addedEditedCategory == null)
                return;

            this.DialogResult = true;

            this.Close();
        }

        /// <summary>
        /// Zatvaranje forme
        /// </summary>
        public void btnNo_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
	}
}