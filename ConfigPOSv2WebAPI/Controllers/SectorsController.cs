﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{

    public class SectorsController : ApiController
    {
        SectorsService Sectors = new SectorsService();

        // GET ALL
        [Route("api/SECTORS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Sectors.SectorsGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET SINGLE
        [Route("api/SECTORS/GetById")]
        [HttpGet]
        public HttpResponseMessage GetSectorById([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Sectors.SectorsGetByID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/SECTORS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertSector([FromBody]SectorDM sector)
        {
            try
            {
                int addedTableId = Sectors.SectorInsert(sector);
                return Request.CreateResponse(HttpStatusCode.OK, addedTableId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/SECTORS/Update")]
        [HttpPut]
        public HttpResponseMessage UpdateSector([FromBody]SectorDM sector)
        {
            try
            {
                Sectors.SectorUpdate(sector);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // DELETE
        [Route("api/SECTORS/Delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteSector(int id)
        {
            try
            {
                Sectors.SectorDelete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
