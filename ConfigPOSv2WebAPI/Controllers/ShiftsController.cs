﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class ShiftsController : ApiController
    {
        ShiftsService shiftService = new ShiftsService();

        // GET ALL
        [Route("api/SHIFTS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, shiftService.ShiftGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY SUBCATEGORYID
        [Route("api/SHIFTS/GetLast")]
        [HttpGet]
        public HttpResponseMessage GetLast()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, shiftService.ShiftGetLast());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/SHIFTS/Insert")]
        [HttpPost]
        public HttpResponseMessage ShiftInsert()
        {
            try
            {
                int addedShiftId = shiftService.ShiftInsert();
                return Request.CreateResponse(HttpStatusCode.OK, addedShiftId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/SHIFTS/Update")]
        [HttpPut]
        public HttpResponseMessage ShiftUpdate([FromBody]ShiftDM shift)
        {
            try
            {
                shiftService.ShiftUpdate(shift);
                return Request.CreateResponse(HttpStatusCode.OK, shift);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // CLOSE
        [Route("api/SHIFTS/Close")]
        [HttpPut]
        public HttpResponseMessage ShiftClose()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, shiftService.ShiftClose());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
