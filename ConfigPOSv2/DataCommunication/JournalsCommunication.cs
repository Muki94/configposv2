﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2.Helpers;


namespace ConfigPOSv2.DataCommunication
{
    public class JournalsCommunication
    {
        private static JournalsService journalsService = new JournalsService();

        public static int JournalsInsert()
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int>("api/JOURNALS/Insert");
            else
                return journalsService.JournalInsert();
        }

        public static void JournalsUpdate(JournalDM journal)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<JournalDM, JournalDM>("api/JOURNALS/Update", journal);
            else
                journalsService.JournalUpdate(journal);
        }

        public static bool JournalsClose(int journalId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Put<bool,int>("api/JOURNALS/Close", journalId);
            else
                return journalsService.JournalClose(journalId);
        }

        public static List<JournalDM> JournalsGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<JournalDM>>("api/JOURNALS/GetAll");
            else
                return journalsService.JournalGetAll();
        }

        public static JournalDM JournalsGetLast()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<JournalDM>("api/JOURNALS/GetLast");
            else
                return journalsService.JournalGetLast();
        }
    }	
}
