﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class ItemTaxesViewMapper
    {
        public static ItemTaxesViewDM MapItemTaxesToItemTaxesViewDM(this ITEM_TAXES_GetByItemID_View itemTaxes)
        {
            if (itemTaxes == null)
                return null;

            ItemTaxesViewDM itemTaxesModel = new ItemTaxesViewDM()
            {
                ItemID = itemTaxes.ItemID,
                TaxID = itemTaxes.TaxID,
                Rate = itemTaxes.Rate,
                RecalculatedRate = itemTaxes.RecalculatedRate,
                Name = itemTaxes.Name,
                Code = itemTaxes.Code,
                Active = itemTaxes.Active,
                ValidFrom = itemTaxes.ValidFrom,
                ValidTo = itemTaxes.ValidTo
            };
            return itemTaxesModel;
        }

        public static List<ItemTaxesViewDM> MapItemTaxesListToItemTaxesViewDMList(this List<ITEM_TAXES_GetByItemID_View> itemTaxes)
        {
            if (itemTaxes == null)
                return null;

            List<ItemTaxesViewDM> itemTaxesModels = new List<ItemTaxesViewDM>();

            foreach (var itemTax in itemTaxes)
            {
                itemTaxesModels.Add(new ItemTaxesViewDM()
                {
                    ItemID = itemTax.ItemID,
                    TaxID = itemTax.TaxID,
                    Rate = itemTax.Rate,
                    RecalculatedRate = itemTax.RecalculatedRate,
                    Name = itemTax.Name,
                    Code = itemTax.Code,
                    Active = itemTax.Active,
                    ValidFrom = itemTax.ValidFrom,
                    ValidTo = itemTax.ValidTo
                });
            }

            return itemTaxesModels;
        }
    }
}
