﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fiscal;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2.DataCommunication;

namespace ConfigPOSv2.Forms
{
    public static class BillsForm
    {       
        public static List<FiscalArticle> Articles = new List<FiscalArticle>();
        public static List<BillsItemsDM> BillItems = new List<BillsItemsDM>();
        public static List<PaymentTypesDM> PaymentTypes = new List<PaymentTypesDM>();
        public static decimal Amount = 0;
        public static int NumberOfPaymentTypes = 1;
        static StationsDM station = StationsCommunication.StationsGetByUserID(App.Current.userId);
        
        public static int PrintBill(List<FiscalArticle> Items,decimal Amount, decimal Discount, List<Payment> Payments)
        {
          return FP.CreateReceipt(Items, Payments, Amount);
        }

        
        public static void AddBillItem(int ItemID,decimal Quantity,decimal Discount)
        {   
            ItemDM item = new ItemDM();
            item = ItemForm.ItemGetByID(ItemID);
            ItemPricesDM itemPrice = ItemPricesCommunication.ItemPricesGetByItemID(ItemID.ToString(), station.BusinessUnitID.ToString());
            List<ItemTaxesViewDM> itemTaxes = ItemTaxesCommunication.ItemTaxesGetByItemID(ItemID.ToString());

            decimal totalTaxesPercentage = 0;
            decimal totalRecalculatedTaxPercentage = 0;

            for (int i = 0; i < itemTaxes.Count; i++)
            {
                totalTaxesPercentage += itemTaxes[i].Rate;
                totalRecalculatedTaxPercentage += itemTaxes[i].RecalculatedRate;
            }

            FiscalArticle Article = new FiscalArticle();
            Article.Code = item.Code;
            Article.Name = item.Name;
            Article.Quantity = Quantity;
            Article.Price = itemPrice.Price;
            Article.Discount = Discount;

            Articles.Add(Article);
            Amount += Article.Quantity * (Article.Price - (Article.Price * (Article.Discount/100)));

            BillsItemsDM billItem = new BillsItemsDM();
            
            billItem.ItemID = ItemID;
            billItem.Quantity = Quantity;
            billItem.Price = itemPrice.Price;
            billItem.Total = billItem.Quantity * billItem.Price;
            billItem.DiscountPercentage = Discount;
            billItem.DiscountAmount = billItem.Price * (billItem.DiscountPercentage / 100);
            billItem.PriceWithoutDiscount = billItem.Price - billItem.DiscountAmount;
            billItem.TotalWithoutDiscount = billItem.Quantity * billItem.PriceWithoutDiscount;
            billItem.TotalTaxesPercentage = totalTaxesPercentage;
            billItem.TaxAmount = billItem.TotalWithoutDiscount * (totalRecalculatedTaxPercentage/100);
            billItem.TotalWithoutTax = (billItem.TotalWithoutDiscount) - billItem.TaxAmount;
            billItem.BasePrice = billItem.Price -(billItem.Price * (totalRecalculatedTaxPercentage/100));
            billItem.BasePriceWithoutDiscount = (billItem.Price - billItem.DiscountAmount) - (billItem.Price * (totalRecalculatedTaxPercentage / 100));

            BillItems.Add(billItem);
        }

        public static void CreateBill(int FiscalNumber, int orderID = 0, int roundID = 0)
        {
            BillsDM Bill = new BillsDM();
            int BillID = 0;
                        
            Bill.Number = BillsCommunication.BillsGetLastNumber();
            Bill.FullBillNumber = Bill.Number.ToString() + "/" + DateTime.Now.Year.ToString();
            Bill.StationID = station.StationID;
            Bill.CreateUserID = Convert.ToInt32(App.Current.userId);
            Bill.FiscalNumber = FiscalNumber;
            Bill.Total = Amount;
            Bill.ProtectionCode = "xaxaxaxa";
            Bill.Tip = 0;
            if (OrdersForm.orderID != null)
            {
                Bill.OrderID = OrdersForm.orderID;
            }
            BillID = BillsCommunication.BillsInsert(Bill);
            int billItemID = 0;

            List<RoundItemViewDM> roundItems = new List<RoundItemViewDM>();
            if (roundID != 0)
            {
                roundItems = OrdersCommunication.RoundsItemsGetByRoundId(roundID).ToList();
            }
            if (orderID != 0)
            {
                roundItems = OrdersCommunication.RoundsItemsGetByOrderID(orderID).ToList();
                OrdersCommunication.OrdersUpdateClosed(orderID, Bill.CreateUserID);
            }
            for (int i = 0; i < BillItems.Count; i++)
            {
                BillItems[i].BillID = BillID;
                billItemID = BillsItemsCommunication.BillsItemsInsert(BillItems[i]);

                List<RoundItemViewDM> rItems = roundItems.Where(x => x.ItemID == BillItems[i].ItemID).ToList();
                for (int j = 0; j < rItems.Count; j++)
                {
                    BillsItemsRoundsItemsDM billItemRoundItem = new BillsItemsRoundsItemsDM();
                    billItemRoundItem.BillItemID = billItemID;
                    billItemRoundItem.RoundItemID = rItems[j].RoundItemID;
                    BillsItemsCommunication.BillsItemsRoundsItemsInsert(billItemRoundItem);
                }
                
            }



            foreach (var item in PaymentTypes)
            {
                PaymentsDM payment = new PaymentsDM();
                payment.BillID = BillID;
                payment.PaymentTypeID = item.PaymentTypeID;
                payment.Tip = 0;
                if (PaymentTypes.Count == 1)
                    payment.Amount = Amount;
                else
                    payment.Amount = item.Amount;

                PaymentsCommunication.Insert(payment);
            }

            ClearData();
        }

        public static Payment CreatePaymentType(string Name,decimal Amount)
        {
            Payment p = new Payment();

            switch (Name)
            {
                case "Gotovina": p.PaymentM = Payment.PaymentMethod.Gotovina; break;
                case "Kartica": p.PaymentM = Payment.PaymentMethod.Kartica; break;
                case "Virman": p.PaymentM = Payment.PaymentMethod.Virman; break;
                case "Ček": p.PaymentM = Payment.PaymentMethod.Cek; break;
                default:break;
            }
            p.Amount = Amount;
            return p;
        }

        public static void MapPaymentTypes(List<Payment> payments)
        {
            foreach (var payment in payments)
	        {
                PaymentTypesDM p = PaymentTypesCommunication.PaymentTypesGetByName(payment.PaymentM.ToString());
                p.Amount = payment.Amount;
                PaymentTypes.Add(p);
	        }   
        }

        public static void ClearData()
        {
            BillItems.Clear();
            Articles.Clear();
            PaymentTypes.Clear();
            Amount = 0;
        }
    }
}
