﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class ItemsCommunication
    {
        private static ItemsService itemsService = new ItemsService();

        public static int ItemsInsert(ItemDM item)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, ItemDM>("api/ITEMS/Insert", item);
            else
                return itemsService.ItemInsert(item);
        }

        public static void ItemsUpdate(ItemDM item)
        {
            if (App.usingWebApi)
                WebApiHelper.Put<ItemDM, ItemDM>("api/ITEMS/Update", item);
            else
                itemsService.ItemUpdate(item);
        }

        public static void ItemsDelete(int itemID)
        {
            if (App.usingWebApi)
                WebApiHelper.Delete<ItemDM>("api/ITEMS/Delete", itemID);
            else
                itemsService.ItemDelete(itemID);
        }

        public static List<ItemDM> ItemsGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<ItemDM>>("api/ITEMS/GetAll");
            else
                return itemsService.ItemGetAll();
        }

        public static ItemDM ItemsGetByID(int itemID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<ItemDM>("api/ITEMS/GetById", new FilterAttributesDM() { idName = itemID.ToString() });
            else
                return itemsService.ItemGetByID(itemID);
        }

        public static List<ItemDM> ItemsGetAllBySubcategoryId(int subcategoryId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<ItemDM>>("api/ITEMS/GetAllBySubcategoryId", new FilterAttributesDM() { idName = subcategoryId.ToString() });
            else
                return itemsService.ItemGetBySubcategoryID(subcategoryId);
        }

        public static ImageDM ItemsGetImageByImageId(int imageId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<ImageDM>("api/ITEMS/GetImageByImageId", new FilterAttributesDM() { idName = imageId.ToString() });
            else
                return itemsService.ImageGetByImageID(imageId);
        }
    }
}
