﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ConfigPOSv2.Forms
{
    public class OrdersForm
    {
        public class OrderItemVM
        {
            public int ItemID { get; set; }
            public int? TableID { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public decimal Quantity { get; set; }
        }
        public class OrderTotalVM
        {
            public int? TableID { get; set; }
            public decimal Total { get; set; }
            public string TotalString { get; set; }
        }
        public static List<OrderItemVM> orderItems = new List<OrderItemVM>();
        public static List<OrderTotalVM> orderTotals = new List<OrderTotalVM>();
        //public static decimal orderTotal = 0;
        public static bool isOnOrder = false;
        //public static string orderTotalContent = string.Empty;
        public static string orderTotalZero = string.Empty;
        public static string ordersTotalContent = string.Empty;
        public static int? tableID = null;
        public static decimal ordersTotal = 0;
        public static int? orderID = null;
        public static bool? checkOutAllSelected = null;
        public static void DeleteOrderOnActiveTable()
        {
            orderItems.RemoveAll(x => x.TableID == tableID);
        }
        public static string ResetOrderTotalOnActiveTable()
        {
            decimal zero = 0;
            orderTotalZero = zero.ToString("0.00");
            return orderTotalZero;
        }
        public static void ResetOrders()
        {
            orderItems = new List<OrdersForm.OrderItemVM>();
            for (int i = 0; i < OrdersForm.orderTotals.Count; i++)
            {
                OrdersForm.orderTotals[i].Total = 0;
                OrdersForm.orderTotals[i].TotalString = OrdersForm.orderTotals[i].Total.ToString("0.00");
            }
            decimal zero = 0;
            orderTotalZero = zero.ToString("0.00");
        }

        public static string SetOrderTotal(decimal OrderTotal)
        {
            orderTotals.Find(x => x.TableID == tableID).Total = OrderTotal;
            //orderTotal = OrderTotal;
            //orderTotalContent = orderTotal.ToString("0.00");
            orderTotals.Find(x => x.TableID == tableID).TotalString = OrderTotal.ToString("0.00"); ;
            return OrderTotal.ToString("0.00");
        }
        public static void SetOrdersTotal(decimal OrdersTotal)
        {
            ordersTotal = OrdersTotal;
            ordersTotalContent = ordersTotal.ToString("0.00");
        }
        public static string AddTotalToOrdersTotal()
        {
            ordersTotal += orderTotals.Find(x => x.TableID == tableID).Total;
            ordersTotalContent = ordersTotal.ToString("0.00");
            orderTotals.Find(x => x.TableID == tableID).Total = 0;
            orderTotals.Find(x => x.TableID == tableID).TotalString = orderTotals.Find(x => x.TableID == tableID).Total.ToString("0.00");

            return ordersTotalContent;
        }
        public static OrderItemVM AddOrderItem(int itemID, string name, decimal price)
        {
            decimal novaKolicina = 0;
            isOnOrder = false;
            List<OrderItemVM> ordersOnTable = orderItems.Where(x => x.TableID == tableID).ToList();
            for (int i = 0; i < ordersOnTable.Count; i++)
            {
                if (ordersOnTable[i].ItemID == itemID)
                {
                    orderTotals.Find(x => x.TableID == tableID).Total -= (ordersOnTable[i].Quantity * ordersOnTable[i].Price);
                    ordersOnTable[i].Quantity += 1;
                    novaKolicina = ordersOnTable[i].Quantity;
                    isOnOrder = true;
                    orderTotals.Find(x => x.TableID == tableID).Total += (ordersOnTable[i].Quantity * ordersOnTable[i].Price);
                    break;
                }
            }
            OrderItemVM item = new OrderItemVM();
            if (isOnOrder)
            {
                item = new OrderItemVM { ItemID = itemID, Name = name, Price = price, Quantity = novaKolicina, TableID = tableID };
            }
            else
            {
                OrderTotalVM ordertot = orderTotals.Find(x => x.TableID == tableID);
                if (ordertot == null)
                    orderTotals.Add(new OrderTotalVM { TableID = tableID, Total = price, TotalString = price.ToString("0.00") });
                else
                {
                    orderTotals.Find(x => x.TableID == tableID).Total += price;
                }
                item = new OrderItemVM { ItemID = itemID, Name = name, Price = price, Quantity = 1, TableID = tableID };
                orderItems.Add(item);
            }
            orderTotals.Find(x => x.TableID == tableID).TotalString = orderTotals.Find(x => x.TableID == tableID).Total.ToString("0.00");
            //orderTotalContent = orderTotal.ToString("0.00");
            return item;
        }
        public static string GetItemQuantityWithOrWithoutDecimals(decimal quantity)
        {
            string broj = quantity.ToString("0.000");
            bool sada = false;
            bool imaDecimala = false;
            for (int j = 0; j < broj.Length; j++)
            {
                if (sada)
                    if (broj[j] != '0')
                        imaDecimala = true;
                if (broj[j] == ',')
                    sada = true;
            }
            string krajnjiBroj = string.Empty;
            if (imaDecimala)
                krajnjiBroj = quantity.ToString("0.00");
            else
                krajnjiBroj = quantity.ToString("0");

            return krajnjiBroj;
        }
        public static void EditOrderItemQuantity(string itemID, bool decrease, decimal novaKolicina = 0)
        {
            OrderItemVM stavka = orderItems.Find(x => x.ItemID.ToString() == itemID && x.TableID == tableID);
            if (novaKolicina <= 0)
            {
                orderItems.Remove(stavka);
            }
            else
            {
                if (!decrease)
                {
                    orderTotals.Find(x => x.TableID == tableID).Total += (stavka.Price * novaKolicina);
                }
                orderItems.Find(x => x.ItemID.ToString() == itemID && x.TableID == tableID).Quantity = novaKolicina;
            }
            orderTotals.Find(x => x.TableID == tableID).TotalString = orderTotals.Find(x => x.TableID == tableID).Total.ToString("0.00");
        }

        public static List<CategoryDM> CategoriesGetAll()
        {
            List<CategoryDM> categories = CategoriesCommunication.CategoriesGetAll();
            return categories;
        }
        public static List<SubcategoryDM> SubcategoriesGetByCategoryID(string categoryID)
        {
            List<SubcategoryDM> subcategories = SubcategoriesCommunication.SubcategoriesGetByCategoryId(Int32.Parse(categoryID));
            return subcategories;
        }
        public static List<ItemDM> ItemsGetBySubcategoryID(string subcategoryID, bool ucitajFavorite)
        {
            List<ItemDM> items = new List<ItemDM>();
            if (!ucitajFavorite)
            {
                items = ItemsCommunication.ItemsGetAllBySubcategoryId(Int32.Parse(subcategoryID));
            }
            else
            {
                //Učitati favorite za tu podkategoriju
            }
            return items;
        }
        //public static void ScrolToSelectedButton(WrapPanel wrapPanel)
        //{
        //    try
        //    {
        //        for (int i = 0; i < wrapPanel.Children.Count; i++)
        //        {
        //            if (((Button)wrapPanel.Children[i]).IsDefault == false)
        //            {
        //                ((Button)wrapPanel.Children[i]).BringIntoView();
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //}
        //public static Button GetCreatedCategoryButton(CategoryDM category, Style buttonStyle)
        //{
        //    Button button = new Button();

        //    button = StyleHelper.CreateKategorijaButton(category.Name);
        //    button.Style = buttonStyle;// (Style)this.Resources["KategorijaButtonStyle"];
        //    button.Uid = category.CategoryID.ToString();

        //    return button;
        //}
        //public static void ClearSelectedCategories(WrapPanel wrapPanel)
        //{
        //    try
        //    {
        //        for (int i = 0; i < wrapPanel.Children.Count; i++)
        //        {
        //            Button b = (Button)wrapPanel.Children[i];
        //            b.IsDefault = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //}
       
        //public static Button GetCreatedSubcategoryButton(SubcategoryDM subcategory, Style buttonStyle)
        //{
        //    Button button = new Button();

        //    button = StyleHelper.CreatePodkategorijaButton(subcategory.Name);
        //    button.Style = buttonStyle;// Application.Current.FindResource("PodkategorijaButtonStyle") as Style;
        //    button.Uid = subcategory.SubcategoryID.ToString();

        //    return button;
        //}

        //public static Button GetCreatedItemButton(ItemDM item, Style buttonStyle)
        //{
        //    try
        //    {
        //        Button button = new Button();

        //        //učitati slike iz temporary tabele koja se ucitava na pocetku programa <--napomena
        //        Byte[] imageData = null;
        //        if (item.ImageID != null)
        //        {
        //            ImageDM image = ItemsCommunication.ItemsGetImageByImageId(item.ImageID.Value);
        //            if (image != null)
        //            {
        //                imageData = image.Data;
        //            }
        //        }
        //        button = StyleHelper.CreateArtikalButton(item.Name, item.ItemID, item.Price, imageData);
        //        button.Style = buttonStyle;

        //        return button;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}


    }
}
