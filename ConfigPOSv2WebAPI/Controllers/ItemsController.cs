﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    [Authorize]
    public class ItemsController : ApiController
    {
        ItemsService itemService = new ItemsService();

        // GET ALL
        [Route("api/ITEMS/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ItemGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY SUBCATEGORYID
        [Route("api/ITEMS/GetAllBySubcategoryId")]
        [HttpGet]
        public HttpResponseMessage GetAllBySubcategoryId([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ItemGetBySubcategoryID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET BY SUBCATEGORYID
        [Route("api/ITEMS/GetImageByImageId")]
        [HttpGet]
        public HttpResponseMessage GetImageByImageId([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ImageGetByImageID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT IMAGE
        [Route("api/ITEMS/ImageInsert")]
        [HttpPost]
        public HttpResponseMessage ImageInsert([FromBody] ImageDM image)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ImageInsert(image));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        //Update image
        [Route("api/ITEMS/ImageUpdate")]
        [HttpPut]
        public HttpResponseMessage ImageUpdate([FromBody] ImageDM image)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ImageUpdate(image));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        // GET SINGLE
        [Route("api/ITEMS/GetById")]
        [HttpGet]
        public HttpResponseMessage GetItemById([FromUri] FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, itemService.ItemGetByID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/ITEMS/Insert")]
        [HttpPost]
        public HttpResponseMessage ItemInsert([FromBody]ItemDM item)
        {
            try
            {
                int addedItemId = itemService.ItemInsert(item);
                return Request.CreateResponse(HttpStatusCode.OK, addedItemId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/ITEMS/Update")]
        [HttpPut]
        public HttpResponseMessage ItemUpdate([FromBody]ItemDM item)
        {
            try
            {
                itemService.ItemUpdate(item);
                return Request.CreateResponse(HttpStatusCode.OK,item);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // DELETE
        [Route("api/ITEMS/Delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage ItemDelete(int id)
        {
            try
            {
                itemService.ItemDelete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

    }
}
