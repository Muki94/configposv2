﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Forms;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConfigPOSv2
{
    /// <summary>
    /// Interaction logic for frmItemsEdit.xaml
    /// </summary>
    public partial class frmItemsEdit : Window
    {
        public ItemDM addedEditedArticle;
        public bool isEdit = false;
        public int articleId;
        private bool ignore = false;


        public frmItemsEdit(int articleId = 0)
        {
            InitializeComponent();
            this.articleId = articleId;

            addedEditedArticle = ItemsCommunication.ItemsGetByID(articleId);

            if (addedEditedArticle != null)
            {
                isEdit = true;

                //vraca podkategoriju na osnovu proslijedjenog id-a radi categoryId-a kako bi se pre-selektovao combobox
                SubcategoryDM subcategory = SubcategoriesCommunication.SubcategoriesGetByID(addedEditedArticle.SubcategoryID);
                var categoryID = subcategory.CategoryID;
                
                //inicjalizacija elemenata za uredjivanje 
                txtArticleName.Text = addedEditedArticle.Name;
                txtArticleCode.Text = addedEditedArticle.Code;
                txtBarcode.Text = addedEditedArticle.RefCode;
                txtPrice.Text = addedEditedArticle.Price.ToString();
                ArticleImage.Uid = addedEditedArticle.ImageID.ToString();
                btnActive.IsDefault = !addedEditedArticle.Active;
                btnPeriodicallyItem.IsDefault = addedEditedArticle.DailyItem;

                //ucitavanje svih kategorija i svih podkategorija na osnovu izvucenog categoryId-a
                LoadCategories();
                LoadSubcategories(categoryID);

                //postavljanje ignore flaga kako bi se zanemario selection_changed event na combo-boxu i setovanje defaultnih vrijednosti za kategoriju i podkategoriju 
                ignore = true;
                cmbCategories.SelectedValue = categoryID.ToString();
                cmbSubcategories.SelectedValue = subcategory.SubcategoryID.ToString();
                ignore = false;
            }
            else
            {
                txtArticleName.Text = "";
                txtArticleCode.Text = "";
                txtBarcode.Text = "";
                txtPrice.Text = "";
                txtDose.Text = "";
                txtUnitOfMeasure.Text = "";
                ArticleImage.Uid = "";
                btnActive.IsDefault = true;
                btnPeriodicallyItem.IsDefault = true;
                LoadCategories();
            }
        }

        /// <summary>
        /// Funkcija koja služi za učitavanje svih kategorija u combobox kategorija
        /// </summary>
        public void LoadCategories()
        {
            cmbCategories.ItemsSource = CategoriesCommunication.CategoriesGetAll();
            cmbCategories.DisplayMemberPath = "Name";
            cmbCategories.SelectedValuePath = "CategoryID";
        }

        /// <summary>
        /// Funkcija koja služi za učitavanje svih podkategorija u combobox na osnovu odabrane kategorije
        /// </summary>
        /// <param name="CategoryID">Id kategorije</param>
        public void LoadSubcategories(int categoryId)
        {
            cmbSubcategories.ItemsSource = SubcategoriesCommunication.SubcategoriesGetByCategoryId(categoryId);
            cmbSubcategories.DisplayMemberPath = "Name";
            cmbSubcategories.SelectedValuePath = "SubcategoryID";
        }

        /// <summary>
        /// Učitavanje podkategorija na osnovu trenutno odabrane kategorije
        /// </summary>
        private void cmbKategorije_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!ignore)
                LoadSubcategories(Int32.Parse(cmbCategories.SelectedValue.ToString()));
            }

        #region Nova kategorija
        /// <summary>
        /// Otvaranje forme za dodavanje nove kategorije
        /// </summary>
        public void AddCategory(object sender, RoutedEventArgs e)
        {
            gridCategory.Visibility = System.Windows.Visibility.Visible;
            gridMain.IsEnabled = false;
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivna"
        /// </summary>
        public void btnAktivan_KategorijaClick(object sender, RoutedEventArgs e)
        {
            btnActive_Category.IsDefault = !btnActive_Category.IsDefault;
        }

         /// <summary>
        /// Spremanje nove Kategorije
        /// </summary>
        public void SaveCategory(object sender, RoutedEventArgs e)
        {
            CategoryForm.AddEditCategory(txtName_Category.Text,btnActive_Category.IsDefault);

            gridCategory.Visibility = System.Windows.Visibility.Collapsed;
            gridMain.IsEnabled = true;

            //Ako je dodana nova kategorija ponovo se učitavaju sve kategorije iz baze podataka
                cmbCategories.ItemsSource = null;
                LoadCategories();
            }

        /// <summary>
        /// Zatvaranje prozora za dodavanje kategorije
        /// </summary>
        public void CloseCategory(object sender, RoutedEventArgs e)
        {
            gridCategory.Visibility = System.Windows.Visibility.Collapsed;
            gridMain.IsEnabled = true;
        }
        #endregion

        #region Nova podkategorija
        /// <summary>
        /// Otvaranje forme za dodavanje nove podkategorije
        /// </summary>
        public void AddSubcategory(object sender, RoutedEventArgs e)
        {
            gridSubcategory.Visibility = System.Windows.Visibility.Visible;
            gridMain.IsEnabled = false;

            //Load kategorija na formi za dodavanje nove podkategorije
            cmbCategory_Subcategories.ItemsSource = CategoriesCommunication.CategoriesGetAll();
            cmbCategory_Subcategories.DisplayMemberPath = "Name";
            cmbCategory_Subcategories.SelectedValuePath = "CategoryID";
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivna"
        /// </summary>
        public void btnAktivan_PodkategorijaClick(object sender, RoutedEventArgs e)
        {
            btnActive_Subcategory.IsDefault = !btnActive_Subcategory.IsDefault;
        }

         /// <summary>
        /// Spremanje nove podkategorije
        /// </summary>
        public void SaveSubcategory(object sender, RoutedEventArgs e)
        {
            if (cmbCategory_Subcategories.SelectedValue == null)
                return;
            
            SubcategoryForm.AddEditSubcategory(txtName_Subcategory.Text, btnActive_Subcategory.IsDefault, Int32.Parse(cmbCategory_Subcategories.SelectedValue.ToString()));

            gridSubcategory.Visibility = System.Windows.Visibility.Collapsed;
            gridMain.IsEnabled = true;

            //Ako je dodana nova podkategorija ponovo se učitavaju sve podkategorije iz baze podataka
             cmbSubcategories.ItemsSource = null;
            LoadSubcategories(Int32.Parse(cmbCategories.SelectedValue.ToString()));
            }

        public void CloseSubcategory(object sender, RoutedEventArgs e)
        {
            gridSubcategory.Visibility = System.Windows.Visibility.Collapsed;
            gridMain.IsEnabled = true;
        }
        #endregion

        /// <summary>
        /// Brisanje trenutne slike artikla
        /// </summary>
        public void ImageDelete(object sender, RoutedEventArgs e)
        {
            var uri = new Uri("pack://application:,,,/ImageResources/no-image_item.png");
            
            ImageBrush buttonBrush = new ImageBrush();
            buttonBrush.ImageSource = new BitmapImage(uri);
            ArticleImage.Background = buttonBrush;
        }

        /// <summary>
        /// Dodavanje nove ili izmjena postojeće slike artikla
        /// </summary>
        public void ImageEdit(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == true)
            {              
                System.IO.Stream stream = System.IO.File.Open(ofd.FileName, System.IO.FileMode.Open);
                BitmapImage imgSrc = new BitmapImage();
                imgSrc.BeginInit();
                imgSrc.StreamSource = stream;
                imgSrc.EndInit();
                ImageBrush buttonBrush = new ImageBrush();
                buttonBrush.ImageSource = imgSrc;
                ArticleImage.Background = buttonBrush;
            }
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "Aktivan"
        /// </summary>
        public void btnAktivanClick(object sender, RoutedEventArgs e)
        {
            btnActive.IsDefault = !btnActive.IsDefault;
        }

        /// <summary>
        /// Promjena vrijednosti na button-u "Povremeni artikal"
        /// </summary>
        public void btnPovremeniArtikalClick(object sender, RoutedEventArgs e)
        {
            btnPeriodicallyItem.IsDefault = !btnPeriodicallyItem.IsDefault;
        }

        /// <summary>
        /// Zatvaranje forme
        /// </summary>
        public void btnNoClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Dodavanje novog artikla
        /// </summary>
        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            bool notValid = false;
            decimal Price;

            //obicna validacija elemenata koji se moraju popuniti
            if (txtArticleName == null || txtArticleName.Text == String.Empty)
            {
                //lblNazivArtikla.Content = "Ime je obavezno polje!";
                //txtNazivArtikla.BorderBrush = Brushes.Red;
                //lblNazivArtikla.BorderBrush = Brushes.Red;
                notValid = true;
            }
            if (txtPrice == null || txtPrice.Text == String.Empty)
            {
                //lblCijena.Content = "Cijena je obavezno polje!";
                //lblCijena.BorderBrush = Brushes.Red;
                notValid = true;
            }
            if (decimal.TryParse(txtPrice.Text, out Price) == false)
            {
                //lblCijena.Content = "Cijena je u neispravnom formatu!";
                //lblCijena.BorderBrush = Brushes.Red;
                notValid = true;
            }
            if (cmbSubcategories == null || cmbSubcategories.Text == String.Empty)
            {
                //lblPodkategorije.Content = "Podkategorija je obavezno polje!";
                //lblPodkategorije.BorderBrush = Brushes.Red;
                notValid = true;
            }

            //prekid u slucaju da forma nije validna 
            if (notValid)
                return;

            btnActive.IsDefault = !btnActive.IsDefault;
            btnPeriodicallyItem.IsDefault = !btnPeriodicallyItem.IsDefault;

            //dodavanje editovanje artikla
            this.addedEditedArticle = ItemForm.AddEditArticle(articleId, txtArticleName.Text, Decimal.Parse(txtPrice.Text), Int32.Parse(cmbSubcategories.SelectedValue.ToString()), !btnActive.IsDefault, false, false, txtBarcode.Text,null,null,null);
            
            DialogResult = true;
            this.Close();
        }
    }
}
