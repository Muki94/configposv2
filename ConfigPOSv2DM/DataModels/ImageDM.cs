﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ImageDM
    {
        public int ImageID { get; set; }
        
        public byte[] Data { get; set; }

        public string StringEncodedData { get; set; }

        public int ItemID { get; set; } // Item koji koristivi ovu sliku, veza je 1 na 1, ako item već koristi neku drugu sliku, trebamo raditi update slike.
    }
}
