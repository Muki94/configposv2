﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class UserGroupDM
    {
        public int UserGroupID { get; set; }
       
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}
