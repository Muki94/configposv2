﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2.Helpers;

namespace ConfigPOSv2.DataCommunication
{
    public class OrdersCommunication
    {
        private static OrdersService orderService = new OrdersService();
        private static RoundsService roundsService = new RoundsService();
        private static RoundsItemsService roundsItemsService = new RoundsItemsService();


        public static List<OrderDM> OrdersGetAllByShiftId(int shiftId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<OrderDM>>("api/ORDERS/GetAllByShiftId", new FilterAttributesDM { idName = shiftId.ToString()});
            else
                return orderService.OrderGetAllByShiftId(shiftId);
        }

        public static List<OrderDM> OrdersGetAllByShiftIdAndUserId(int shiftId, int userId)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<OrderDM>>("api/ORDERS/GetAllByShiftIdAndUserId", new FilterAttributesDM { idName = shiftId.ToString(), userId = userId });
            else
                return orderService.OrderGetAllByShiftIdAndUserId(shiftId, userId);
        }
        public static int OrdersInsert(OrderDM item)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, OrderDM>("api/ORDERS/Insert", item);
            else
                return orderService.OrderInsert(item);
        }

        public static void OrdersUpdateClosed(int orderID, int closeUserID)
        {
            if (App.usingWebApi)
            {
                OrderDM order= new OrderDM();
                order.OrderID = orderID;
                order.CloseUserID = closeUserID;
                WebApiHelper.Put<OrderDM, OrderDM>("api/ORDERS/UpdateClosed", order);
            }
            else
                orderService.OrdersUpdateClosed(orderID, closeUserID);
        }
        public static int RoundsInsert(RoundDM item)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, RoundDM>("api/ROUNDS/Insert", item);
            else
                return roundsService.RoundInsert(item);
        }
        public static int RoundsGetNextOrderNumber()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<int>("api/ROUNDS/GetRoundNextOrderNumber", new FilterAttributesDM() { });
            else
                return roundsService.RoundGetNextOrderNumber();
        }
        public static List<RoundViewDM> RoundsGetByTableId(int tableID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<RoundViewDM>>("api/ROUNDS/GetByTableId", new FilterAttributesDM() { idName = tableID.ToString() });
            else
                return roundsService.RoundsGetByTableID(tableID);
        }
        public static int RoundsItemsInsert(RoundItemDM item)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, RoundItemDM>("api/ROUNDS_ITEMS/Insert", item);
            else
                return roundsItemsService.RoundItemInsert(item);
        }
        public static List<RoundItemViewDM> RoundsItemsGetByRoundId(int roundID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<RoundItemViewDM>>("api/ROUNDS_ITEMS/GetByRoundID", new FilterAttributesDM() { idName = roundID.ToString() });
            else
                return roundsItemsService.RoundsItemsGetByRoundID(roundID);
        }
        public static List<RoundItemViewDM> RoundsItemsGetByOrderIDView(int orderID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<RoundItemViewDM>>("api/ROUNDS_ITEMS/GetByOrderIDView", new FilterAttributesDM() { idName = orderID.ToString() });
            else
                return roundsItemsService.RoundsItemsGetByOrderIDView(orderID);
        }
        public static List<RoundItemViewDM> RoundsItemsGetByOrderID(int orderID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<RoundItemViewDM>>("api/ROUNDS_ITEMS/GetByOrderID", new FilterAttributesDM() { idName = orderID.ToString() });
            else
                return roundsItemsService.RoundsItemsGetByOrderID(orderID);
        }
    }	
}
