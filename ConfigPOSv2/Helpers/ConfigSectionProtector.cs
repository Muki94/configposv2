﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace ConfigPOSv2.Helpers
{
    public class ConfigSectionProtector
    {
        private string m_section;

        public ConfigSectionProtector(string section)
        {
            if (String.IsNullOrEmpty(section))
            {
                throw new ArgumentNullException("ConfigurationSection");
            }
            else
            {
                m_section = section;
            }
        }


        public void ProtectSection()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection protectedSection = config.GetSection(m_section);

            for (int i = 0; i < config.Sections.Count; i++)
            {
                string a = config.Sections[i].SectionInformation.Name.ToString();
                string b = config.Sections[i].SectionInformation.SectionName.ToString();
            }

            if (protectedSection != null && !protectedSection.IsReadOnly() && !protectedSection.SectionInformation.IsProtected && !protectedSection.SectionInformation.IsLocked && protectedSection.SectionInformation.IsDeclared)
            {
                protectedSection.SectionInformation.ProtectSection(null);
                protectedSection.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Full);
            }
        }
    }
}
