﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class PaymentsCommunication
    {
        private static PaymentsService paymentsService = new PaymentsService();

        public static int Insert(PaymentsDM payment)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int,PaymentsDM>("api/PAYMENTS/Insert", payment);
            else
                return paymentsService.PaymentsInsert(payment);
        }
    }
}
