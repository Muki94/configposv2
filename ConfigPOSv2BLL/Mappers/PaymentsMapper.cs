﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class PaymentsMapper
    {
        public static PaymentsDM MapStationToStationsDM(this PAYMENTS payment)
        {
            if (payment == null)
                return null;

            PaymentsDM paymentModel = new PaymentsDM()
            {
                PaymentID = payment.PaymentID,
                BillID = payment.BillID,
                PaymentTypeID = payment.PaymentTypeID,
                Amount = payment.Amount,
                Tip = payment.Tip
            };
            return paymentModel;
        }

        public static List<PaymentsDM> MapPaymentsListToPaymentsDMList(this List<PAYMENTS> payments)
        {
            if (payments == null)
                return null;

            List<PaymentsDM> paymentsModels = new List<PaymentsDM>();

            foreach (var payment in payments)
            {
                paymentsModels.Add(new PaymentsDM()
                {
                    PaymentID = payment.PaymentID,
                    BillID = payment.BillID,
                    PaymentTypeID = payment.PaymentTypeID,
                    Amount = payment.Amount,
                    Tip = payment.Tip
                });
            }

            return paymentsModels;
        }
    }
}
