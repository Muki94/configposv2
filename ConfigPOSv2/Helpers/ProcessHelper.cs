﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace ConfigPOSv2.Helpers
{
    public class ProcessHelper
    {
        public static void SetFocusToExternalApp(string strProcessName)
        {

            //Process[] arrProcesses = Process.GetProcessesByName(strProcessName);
            //if (arrProcesses.Length > 0)
            //{
            //    //Thread.Sleep(100);
            //    //arrProcesses[0].Refresh();
            //    IntPtr ipHwnd = arrProcesses[0].MainWindowHandle;
            //    Thread.Sleep(100);

            //    //SetActiveWindow(ipHwnd);

            //    SetForegroundWindow(ipHwnd);

            //    //Ova komanda služi za maksimiziranje
            //    SendKeys.SendWait("~");
            //}

            //const int WM_GETTEXT = 0x000D;
            //const int WM_GETTEXTLENGTH = 0x000E;

            //IntPtr hWndTeamViewer = FindWindowByCaption(IntPtr.Zero, "TeamViewer");

            //IntPtr hWndID = FindWindowEx(hWndTeamViewer, IntPtr.Zero, "Edit", IntPtr.Zero);
            //IntPtr hWndPass = FindWindowEx(hWndTeamViewer, hWndID, "Edit", IntPtr.Zero);

            //IntPtr pLengthID = SendMessage(hWndID, WM_GETTEXTLENGTH, IntPtr.Zero, IntPtr.Zero);
            //IntPtr pLengthPass = SendMessage(hWndPass, WM_GETTEXTLENGTH, IntPtr.Zero, IntPtr.Zero);

            //StringBuilder strbID = new StringBuilder((int)pLengthID);
            //StringBuilder strbPass = new StringBuilder((int)pLengthPass);

            //IntPtr pID = SendMessage(hWndID, WM_GETTEXT, (int)pLengthID, strbID);
            //IntPtr pPass = SendMessage(hWndPass, WM_GETTEXT, (int)pLengthPass, strbPass);

            IntPtr wdwIntPtr = FindWindow(null, strProcessName);

            //get the hWnd of the process
            Windowplacement placement = new Windowplacement();
            GetWindowPlacement(wdwIntPtr, ref placement);

            // Check if window is minimized
            if (placement.showCmd == 2)
            {
                //the window is hidden so we restore it
                ShowWindow(wdwIntPtr, ShowWindowEnum.Restore);
                //ShowWindow(wdwIntPtr, ShowWindowEnum.ShowNoActivate);
            }
            else if (placement.showCmd == 1)
            {
                ShowWindow(wdwIntPtr, ShowWindowEnum.Show);
            }


            //set user's focus to the window
            SetForegroundWindow(wdwIntPtr);
        }

        private struct Windowplacement
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        private enum ShowWindowEnum
        {
            Hide = 0,
            ShowNormal = 1, ShowMinimized = 2, ShowMaximized = 3,
            Maximize = 3, ShowNormalNoActivate = 4, Show = 5,
            Minimize = 6, ShowMinNoActivate = 7, ShowNoActivate = 8,
            Restore = 9, ShowDefault = 10, ForceMinimized = 11
        };

        //API-declaration
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int SetActiveWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string className, string windowTitle);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref Windowplacement lpwndpl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowEnum flags);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, int wParam, StringBuilder lParam);


    }
}
