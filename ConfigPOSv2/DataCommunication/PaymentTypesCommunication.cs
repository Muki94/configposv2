﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class PaymentTypesCommunication
    {
        private static PaymentTypesService paymentTypesService = new PaymentTypesService();

        public static List<PaymentTypesDM> PaymentTypesGetAll()
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<PaymentTypesDM>>("api/PAYMENT_TYPES/GetAll");
            else
                return paymentTypesService.PaymentTypesGetAll();
        }

        public static PaymentTypesDM PaymentTypesGetByID(string paymentTypeID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<PaymentTypesDM>("api/PAYMENT_TYPES/GetByID", new FilterAttributesDM { idName = paymentTypeID });
            else
                return paymentTypesService.PaymentTypesGetByID(Convert.ToInt32(paymentTypeID));
        }

        public static PaymentTypesDM PaymentTypesGetByName(string name)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<PaymentTypesDM>("api/PAYMENT_TYPES/GetByName", new FilterAttributesDM { idName = name });
            else
                return paymentTypesService.PaymentTypesGetByName(name);
        }
    }
}
