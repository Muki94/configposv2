﻿using ConfigPOSv2BLL.Mappers;
using ConfigPOSv2Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ConfigPOSv2DM.DataModels;

namespace ConfigPOSv2BLL.Services
{
    public class ItemsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novom artiklu
        /// </summary>
        /// <param name="Code">Kod</param>
        /// <param name="Name">Naziv artikla</param>
        /// <param name="SubcategoryID">ID podkategorije</param>
        /// <param name="ImageID">ID slike</param>
        /// <param name="ItemgroupID">ID grupe artikla</param>
        /// <param name="Price">Cijena</param>
        /// <param name="Active">Aktivan</param>
        /// <param name="Description">Opis</param>
        /// <param name="RefCode">Ref kod</param>
        /// <param name="DailyItem">Dnevni artikal</param>
        /// <param name="Favorite">Najdraži</param>

        public int ItemInsert(ItemDM item)
        {
            OpenConnection();
            int addedItemId = Convert.ToInt32(entity.stp_ITEMS_Insert(item.Code, item.Name, item.SubcategoryID, item.ImageID, item.ItemGroupID, item.Price, item.Active, item.Description, item.RefCode, item.DailyItem, item.Favorite).FirstOrDefault().Value);
            CloseConnection();

            return addedItemId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o artiklu na osnovu proslijeđenog ID artikla
        /// </summary>
        /// <param name="ItemID">ID artikla</param>
        /// <param name="Code">Kod</param>
        /// <param name="Name">Naziv artikla</param>
        /// <param name="SubcategoryID">ID podkategorije</param>
        /// <param name="ImageID">ID slike</param>
        /// <param name="ItemgroupID">ID grupe artikla</param>
        /// <param name="Price">Cijena</param>
        /// <param name="Active">Aktivan</param>
        /// <param name="Description">Opis</param>
        /// <param name="RefCode">Ref kod</param>
        /// <param name="DailyItem">Dnevni artikal</param>
        /// <param name="Favorite">Najdraži</param>
        public void ItemUpdate(ItemDM item)
        {
            OpenConnection();
            Boolean deleteImage = false;
            ITEMS tempItem = null;
            if (item.ImageID == null) //Ako se u update pošalje null za imageID, provjerava se jeli prije imageID referenciro neki IMAGE, ako jest, treba ga izbrisati
            {
                tempItem = entity.stp_ITEMS_GetByID(item.ItemID).SingleOrDefault();
                if (tempItem.ImageID != null)
                    deleteImage = true;              
            }

            entity.stp_ITEMS_Update(item.ItemID, item.Code, item.Name, item.SubcategoryID, item.ImageID, item.ItemGroupID, item.Price, item.Active, item.Description, item.RefCode, item.DailyItem, item.Favorite);
            
            if(deleteImage) // Brisanje stare slike koja se više ne koristi
                entity.stp_IMAGES_Delete(tempItem.ImageID);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja briše podatke o artiklu na osnovu proslijeđenog ID artikla
        /// </summary>
        /// <param name="ItemID">ID artikla</param>
        public void ItemDelete(int ItemID)
        {
            OpenConnection();
            ITEMS item = entity.stp_ITEMS_GetByID(ItemID).SingleOrDefault();          
            entity.stp_ITEMS_Delete(ItemID);
            if (item.ImageID != null && item.ImageID != 0)
            {
                try{    entity.stp_IMAGES_Delete(item.ImageID); }
                catch (Exception e){}
            }
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih artikala
        /// </summary>
        /// <returns>Lista artikala</returns>
        public List<ItemDM> ItemGetAll()
        {
            OpenConnection();
            List<ItemDM> list = entity.stp_ITEMS_GetAll().ToList().MapItemListToItemDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća artikal na osnovu prosljeđenog ID artikla
        /// </summary>
        /// <param name="ItemID">ID artikla</param>
        /// <returns>Artikal</returns>
        public ItemDM ItemGetByID(int ItemID)
        {
            OpenConnection();
            ItemDM item = entity.stp_ITEMS_GetByID(ItemID).FirstOrDefault().MapItemToItemDM();
            CloseConnection();

            return item;
        }

        /// <summary>
        /// Funkcija koja vraća listu artikala na osnovu prosljeđenog ID podkategorije
        /// </summary>
        /// <param name="SubcategoryID">ID podkategorije</param>
        /// <returns>Lista artikala</returns>
        public List<ItemDM> ItemGetBySubcategoryID(int SubcategoryID)
        {
            OpenConnection();
            List<ItemDM> item = entity.stp_ITEMS_GetBySubcategoryId(SubcategoryID).ToList().MapItemListToItemDMList();
            CloseConnection();

            return item;
        }

        public ImageDM ImageGetByImageID(int ImageID)
        {
            OpenConnection();
            ImageDM item = entity.stp_IMAGES_GetByID(ImageID).FirstOrDefault().MapImageToImageDM();
            CloseConnection();

            return item;
        }
        /// <summary>
        /// Vraća listu slika
        /// </summary>
        /// <returns>Lista slika</returns>
        public List<ImageDM> ImageGetAll()
        {
            OpenConnection();
            List<ImageDM> item = entity.stp_IMAGES_GetAll().ToList().MapImageListToImageDMList();
            CloseConnection();

            return item;
        }

        public List<ItemGroupDM> ItemGroupsGetAll()
        {
            OpenConnection();
            List<ItemGroupDM> item = entity.stp_ITEMGROUPS_GetAll().ToList().MapItemGroupListToItemGroupDMList();
            CloseConnection();

            return item;
        }

        public int ImageInsert(ImageDM image)
        {
            OpenConnection();

            if (image.StringEncodedData != null && image.StringEncodedData != "")
                image.Data = Convert.FromBase64String(image.StringEncodedData);

            ITEMS item = entity.stp_ITEMS_GetByID(image.ItemID).SingleOrDefault();
            int imageId = 0;
            if(item == null || item.ImageID == null || item.ImageID == 0)
                imageId= entity.stp_IMAGES_Insert(image.Data).FirstOrDefault().Value; //Ako item nema slike, dodaj sliku
            else
            {
                imageId = item.ImageID.Value;
                entity.stp_IMAGES_Update(item.ImageID, image.Data); // Ako item ima vec jednu sliku, onda radi update na nju, umjesto insert nove
            }
                
             
            CloseConnection();

            return imageId;
        }

        public int ImageUpdate(ImageDM image)
        {
            OpenConnection();

            if (image.StringEncodedData != null && image.StringEncodedData != "")
                image.Data = Convert.FromBase64String(image.StringEncodedData);

            entity.stp_IMAGES_Update(image.ImageID, image.Data);
            CloseConnection();

            return image.ImageID;
        }

        public void ImageDelete(int imageID)
        {
            OpenConnection();
            entity.stp_IMAGES_Delete(imageID);
            CloseConnection();
        }
    }
}
