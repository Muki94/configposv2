﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class StationsMapper
    {
        public static StationsDM MapStationToStationsDM(this STATIONS station)
        {
            if (station == null)
                return null;

            StationsDM stationModel = new StationsDM()
            {
                StationID = station.StationID,
                Name = station.Name,
                Deleted = station.Deleted,
                BusinessUnitID = station.BusinessUnitID,
                StationTypeID = station.StationTypeID

            };
            return stationModel;
        }

        public static List<StationsDM> MapStationListToStationsDMList(this List<STATIONS> stations)
        {
            if (stations == null)
                return null;

            List<StationsDM> stationsModels = new List<StationsDM>();

            foreach (var station in stations)
            {
                stationsModels.Add(new StationsDM()
                {
                    StationID = station.StationID,
                    Name = station.Name,
                    Deleted = station.Deleted,
                    BusinessUnitID = station.BusinessUnitID,
                    StationTypeID = station.StationTypeID
                });
            }

            return stationsModels;
        }
    }
}
