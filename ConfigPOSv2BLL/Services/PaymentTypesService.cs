﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class PaymentTypesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public PaymentTypesDM PaymentTypesGetByID(int paymentID)
        {
            OpenConnection();
            PaymentTypesDM paymentType = entity.stp_PAYMENT_TYPES_GetByID(paymentID).FirstOrDefault().MapPaymentTypeToPaymentTypeDM();
            CloseConnection();
            return paymentType;
        }

        public PaymentTypesDM PaymentTypesGetByName(string name)
        {
            OpenConnection();
            PaymentTypesDM paymentType = entity.stp_PAYMENT_TYPES_GetByName(name).FirstOrDefault().MapPaymentTypeToPaymentTypeDM();
            CloseConnection();
            return paymentType;
        }

        public List<PaymentTypesDM> PaymentTypesGetAll()
        {
            OpenConnection();
            List<PaymentTypesDM> paymentTypes = entity.stp_PAYMENT_TYPES_GetAll().ToList().MapPaymentTypesListToPaymentTypesDMList();
            CloseConnection();
            return paymentTypes;
        }
    }
}
