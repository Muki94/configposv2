﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class ItemPricesController : ApiController
    {
        ItemPricesService itemPriceService = new ItemPricesService();

        [Route("api/ITEM_PRICES/GetByItemID")]
        [HttpGet]
        public HttpResponseMessage GetByItemID([FromUri] FilterAttributesDM filter)
        {
            try
            {
                ItemPricesDM itemPrice = itemPriceService.ItemPricesGetByItemID(Convert.ToInt32(filter.idName),Convert.ToInt32(filter.username));
                return Request.CreateResponse(HttpStatusCode.OK, itemPrice);
            }
            catch (Exception e)
            {
               return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
