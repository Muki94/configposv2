﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class ItemMapper
    {
        public static ItemDM MapItemToItemDM(this ITEMS item) 
        {
            if (item == null)
                return null;

            ItemDM itemModel = new ItemDM() { 
                ItemID = item.ItemID,
                ItemGroupID = item.ItemgroupID,
                ImageID = item.ImageID,
                Name = item.Name,
                Price = item.Price,
                RefCode = item.RefCode,
                SubcategoryID = item.SubcategoryID,
                Active = item.Active,
                Code = item.Code,
                DailyItem = item.DailyItem,
                Description = item.Description,
                Favorite = item.Favorite
            };
            
            return itemModel;
        }
                
        public static List<ItemDM> MapItemListToItemDMList(this List<ITEMS> items)
        {
            if (items == null)
                return null;

            List<ItemDM> itemModels = new List<ItemDM>();

            foreach (var item in items)
            {
                itemModels.Add(new ItemDM()
                {
                    ItemID = item.ItemID,
                    ItemGroupID = item.ItemgroupID,
                    ImageID = item.ImageID,
                    Name = item.Name,
                    Price = item.Price,
                    RefCode = item.RefCode,
                    SubcategoryID = item.SubcategoryID,
                    Active = item.Active,
                    Code = item.Code,
                    DailyItem = item.DailyItem,
                    Description = item.Description,
                    Favorite = item.Favorite
                });
            }

            return itemModels;
        }

        public static List<ItemGroupDM> MapItemGroupListToItemGroupDMList(this List<ITEMGROUPS> items)
        {
            if (items == null)
                return null;

            List<ItemGroupDM> itemModels = new List<ItemGroupDM>();

            foreach (var item in items)
            {
                itemModels.Add(new ItemGroupDM()
                {
                    ItemgroupID = item.ItemgroupID,
                    ImageID = item.ImageID,
                    Name = item.Name,
                });
            }

            return itemModels;
        }
    }
}
