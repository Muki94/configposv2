﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.Forms
{
    public class TablesForm
    {
        public static List<TableDM> TablesGetBySectorID(int sectorID)
        {
            List<TableDM> tables = TablesCommunication.TablesGetBySectorID(sectorID);
            return tables;
        }
        public static List<SectorDM> SectorsGetAll()
        {
            List<SectorDM> sectors = SectorsCommunication.SectorsGetAll();
            return sectors;
        }
        public static TableDM AddEditTable(int tableID, string name, int numberOrder, int sectorID, int type, int graphX, int graphY, int rotation, bool deleted) 
        {
            if (numberOrder == 0)
                numberOrder = TablesCommunication.TablesGetNextNumberOrderBySectorID(sectorID);

            TableDM tableModel = new TableDM()
            {
                TableID = tableID,
                Name = name,
                NumberOrder = numberOrder,
                SectorID = sectorID,
                Type = type,
                GraphX = graphX,
                GraphY = graphY,
                Rotation = rotation,
                Deleted = deleted
            };

            if (tableID != 0)
                TablesCommunication.TablesUpdate(tableModel);
            else
                tableModel.TableID = TablesCommunication.TablesInsert(tableModel);

            return tableModel;
        }
        public static bool TablesDelete(int tableID)
        {
            try
            {
                TablesCommunication.TablesDelete(tableID);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static TableDM TablesGetByID(int tableID)
        {
            TableDM table = TablesCommunication.TablesGetByID(tableID);
            return table;
        }
    }
}
