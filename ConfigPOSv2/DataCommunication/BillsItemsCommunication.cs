﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class BillsItemsCommunication
    {
        private static BillsItemsService BillsItemsService = new BillsItemsService();

        public static int BillsItemsInsert(BillsItemsDM BillItem)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, BillsItemsDM>("api/BILLS_ITEMS/Insert", BillItem);
            else
                return BillsItemsService.BillsItemsInsert(BillItem);
        }
        public static int BillsItemsRoundsItemsInsert(BillsItemsRoundsItemsDM Item)
        {
            if (App.usingWebApi)
                return WebApiHelper.Post<int, BillsItemsRoundsItemsDM>("api/BILLS_ITEMS_ROUNDS_ITEMS/Insert", Item);
            else
                return BillsItemsService.BillsItemsRoundsItemsInsert(Item);
        }
    }
}
