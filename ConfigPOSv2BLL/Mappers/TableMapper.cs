﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class TableMapper
    {
        public static TableDM MapTablesToTableDM(this TABLES table)
        {
            if (table == null)
                return null;

            TableDM tableModel = new TableDM()
            {
                TableID = table.TableID,
                Name = table.Name,
                NumberOrder = table.NumberOrder,
                SectorID = table.SectorID,
                Type = table.Type,
                GraphX = table.GraphX,
                GraphY = table.GraphY,
                Rotation = table.Rotation,
                Deleted = table.Deleted
            };

            return tableModel;
        }

        public static List<TableDM> MapTableListToTableDMList(this List<TABLES> tables)
        {
            if (tables == null)
                return null;

            List<TableDM> tableModels = new List<TableDM>();

            foreach (var table in tables)
            {
                tableModels.Add(new TableDM()
                {
                    TableID = table.TableID,
                    Name = table.Name,
                    NumberOrder = table.NumberOrder,
                    SectorID = table.SectorID,
                    Type = table.Type,
                    GraphX = table.GraphX,
                    GraphY = table.GraphY,
                    Rotation = table.Rotation,
                    Deleted = table.Deleted
                });
            }

            return tableModels;
        }
         
    }
}
