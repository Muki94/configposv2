﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    [Authorize]
    public class TablesController : ApiController
    {
        TablesService Tables = new TablesService();

        // GET ALL
        [Route("api/TABLES/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Tables.TablesGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET SINGLE
        [Route("api/TABLES/GetById")]
        [HttpGet]
        public HttpResponseMessage GetTableById([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Tables.TablesGetByID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        // GET BY SECTOR ID
        [Route("api/TABLES/GetBySectorId")]
        [HttpGet]
        public HttpResponseMessage GetBySectorId([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Tables.TablesGetBySectorID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/TABLES/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertTable([FromBody]TableDM table)
        {
            try
            {
                int addedTableId = Tables.TableInsert(table);
                return Request.CreateResponse(HttpStatusCode.OK, addedTableId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/TABLES/Update")]
        [HttpPut]
        public HttpResponseMessage UpdateTable([FromBody]TableDM table)
        {
            try
            {
                Tables.TableUpdate(table);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // DELETE
        [Route("api/TABLES/Delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteTable(int id)
        {
            try
            {
                Tables.TableDelete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        // GET 
        [Route("api/TABLES/GetTableNextNumberOrderBySectorID")]
        [HttpGet]
        public HttpResponseMessage GetTableNextNumberOrderBySectorID([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Tables.TablesGetNextNumberOrderBySectorID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
