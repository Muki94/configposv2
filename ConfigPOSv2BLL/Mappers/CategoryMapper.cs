﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class CategoryMapper
    {
        public static CategoryDM MapCategoryToCategoryDM(this CATEGORIES category)
        {
            if (category == null)
                return null;

            CategoryDM categoryModel = new CategoryDM()
            {
                CategoryID = category.CategoryID,
                Name = category.Name,
                Active = category.Active
            };

            return categoryModel;
        }

        public static List<CategoryDM> MapCategoryListToCategoryDMList(this List<CATEGORIES> categories)
        {
            if (categories == null)
                return null;

            List<CategoryDM> categoryModels = new List<CategoryDM>();

            foreach (var category in categories)
            {
                categoryModels.Add(new CategoryDM()
                {
                    CategoryID = category.CategoryID,
                    Name = category.Name,
                    Active = category.Active
                });
            }

            return categoryModels;
        }
    }
}
