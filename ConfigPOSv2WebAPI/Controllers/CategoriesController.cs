﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        CategoriesService Categories = new CategoriesService();

        // GET ALL
        [Route("api/CATEGORIES/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Categories.CategoriesGetAll());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // GET SINGLE
        [Route("api/CATEGORIES/GetById")]
        [HttpGet]
        public HttpResponseMessage GetCategoryById([FromUri]FilterAttributesDM filters)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, Categories.CategoriesGetByID(Int32.Parse(filters.idName)));
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // INSERT
        [Route("api/CATEGORIES/Insert")]
        [HttpPost]
        public HttpResponseMessage CategoryInsert([FromBody]CategoryDM category)
        {
            try
            {
                int addedItemId = Categories.CategoryInsert(category);
                return Request.CreateResponse(HttpStatusCode.OK, addedItemId);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // UPDATE
        [Route("api/CATEGORIES/Update")]
        [HttpPut]
        public HttpResponseMessage CategoryUpdate([FromBody]CategoryDM category)
        {
            try
            {
                Categories.CategoryUpdate(category);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // DELETE
        [Route("api/CATEGORIES/Delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage CategoryDelete(int id)
        {
            try
            {
                Categories.CategoryDelete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
	}
}