﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2Data.Entity;

namespace ConfigPOSv2BLL.Mappers
{
    public static class JournalMapper
    {
        public static JournalDM MapJOURNALSToJournalDM(this JOURNALS item)
        {
            if (item == null)
                return null;

            JournalDM itemModel = new JournalDM()
            {
                JournalID = item.JournalID,
                StartTime = item.StartTime,
                EndTime = item.EndTime,
                Closed = item.Closed
            };

            return itemModel;
        }

        public static List<JournalDM> MapJOURNALSListToJournalDMList(this List<JOURNALS> items)
        {
            if (items == null)
                return null;

            List<JournalDM> itemModels = new List<JournalDM>();

            foreach (var item in items)
            {
                itemModels.Add(new JournalDM()
                {
                    JournalID = item.JournalID,
                    StartTime = item.StartTime,
                    EndTime = item.EndTime,
                    Closed = item.Closed
                });
            }

            return itemModels;
        }
    }
}
