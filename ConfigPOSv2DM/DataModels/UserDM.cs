﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class UserDM
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string PasswordPIN { get; set; }        
        
        public bool Active { get; set; }
        
        public Nullable<DateTime> LastLogin { get; set; }

        public int UserGroupID { get; set; }

        public string UserGroup { get; set; }
    }
}
