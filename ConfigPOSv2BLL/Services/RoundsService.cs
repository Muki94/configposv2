﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class RoundsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public int RoundInsert(RoundDM item)
        {
            OpenConnection();
            int addedOrderId = Convert.ToInt32(entity.stp_ROUNDS_Insert(item.Number,item.UserID,item.OrderID,item.StationID).FirstOrDefault().Value);
            CloseConnection();

            return addedOrderId;
        }



        public List<RoundViewDM> RoundsGetByTableID(int tableID)
        {
            OpenConnection();
            List<RoundViewDM> rounds = entity.stp_ROUNDS_GetByTableID(tableID).ToList().MapRoundsViewListToRoundViewDMList();
            CloseConnection();

            return rounds;
        }

        public int RoundGetNextOrderNumber()
        {
            OpenConnection();
            int number = Convert.ToInt32(entity.stp_ROUNDS_GetNextOrderNumber().FirstOrDefault().Value);
            CloseConnection();

            return number;
        }
    }
}
