﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class StationsDM
    {
        public int StationID { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public int BusinessUnitID { get; set; }
        public Nullable<int> DeviceFiscalPrinterID { get; set; }
        public Nullable<int> DevicePrinterID { get; set; }
        public Nullable<int> DevicePosPrinterID { get; set; }
        public Nullable<int> PosPrinterCopies { get; set; }
        public Nullable<int> PosPrinterWidth { get; set; }
        public Nullable<int> PosPrinterFeedLineNumber { get; set; }
        public int StationTypeID { get; set; }
    }
}
