﻿using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class BillsItemsController : ApiController
    {
        BillsItemsService BillsItemsService = new BillsItemsService();

        [Route("api/BILLS_ITEMS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertBillsItems([FromBody] BillsItemsDM BillItem)
        {
            try 
	        {
                int billItemID = BillsItemsService.BillsItemsInsert(BillItem);
		        return Request.CreateResponse(HttpStatusCode.OK,billItemID);
	        }
	        catch (Exception e)
	        {
		        return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
	        }
        }
        [Route("api/BILLS_ITEMS_ROUNDS_ITEMS/Insert")]
        [HttpPost]
        public HttpResponseMessage InsertBillsItemsRoundsItems([FromBody] BillsItemsRoundsItemsDM Item)
        {
            try
            {
                int ItemID = BillsItemsService.BillsItemsRoundsItemsInsert(Item);
                return Request.CreateResponse(HttpStatusCode.OK, ItemID);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}