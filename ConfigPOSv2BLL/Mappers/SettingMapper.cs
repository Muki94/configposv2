﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Mappers
{
    public static class SettingMapper
    {
        public static SettingDM MapSettingToSettingDM(this SETTINGS setting) 
        {
            if (setting == null)
                return null;

            SettingDM settingModel = new SettingDM()
            {
                Name = setting.Name,
                Description = setting.Description,
                Superadmin = setting.Superadmin,
                Value = setting.Value
            };

            return settingModel;
        }

        public static List<SettingDM> MapSettingToSettingDM(this List<SETTINGS> settings)
        {
            if (settings == null)
                return null;

            List<SettingDM> settingModels = new List<SettingDM>();

            foreach (var setting in settings)
            {
                settingModels.Add(new SettingDM()
                {
                    Name = setting.Name,
                    Description = setting.Description,
                    Superadmin = setting.Superadmin,
                    Value = setting.Value
                });
            }

            return settingModels;
        }
    }
}
