﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ConfigPOSv2WebAPI.Providers
{
    public class OAuthProvider:OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                var username = context.UserName;
                var password = context.Password;

                var userService = new UsersService();
                UserDM user = new UserDM();

                if (username == string.Empty)
                {
                    user = userService.LoginByPIN(password.ToString());
                }
                else
                {
                    user = userService.LoginByUsernamePassword(username, password.ToString());
                }

                if (user != null)
                {
                    var userGroup = userService.GetUserGroupByUserGroupId(user.UserGroupID);
                    var claims = new List<Claim>()
                   {
                       new Claim("UserID", user.UserID.ToString()),
                       new Claim(ClaimTypes.Role,userGroup.Name)
                   };

                    var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {"userId", user.UserID.ToString()},
                        {"username", user.Name},
                        {"userGroup", userGroup.Name}
                    });

                    ClaimsIdentity oAutIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
                    context.Validated(new AuthenticationTicket(oAutIdentity, props));
                }
                else
                {
                    context.SetError("invalid_grant", "Error");
                }
            });
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }
            return Task.FromResult<object>(null);
        }
    }
}