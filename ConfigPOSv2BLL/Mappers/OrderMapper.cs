﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2Data.Entity;

namespace ConfigPOSv2BLL.Mappers
{
    public static class OrderMapper
    {
        public static OrderDM MapToOrderDM(this ORDERS item)
        {
            if (item == null)
                return null;

            OrderDM itemModel = new OrderDM()
            {
                OrderID = item.OrderID,
                ShiftID = item.ShiftID,
                TableID = item.TableID,
                IsVIP = item.IsVIP,
                OpenTime = item.OpenTime, 
                CloseTime = item.CloseTime,
                Closed = item.Closed,
                OpenUserID = item.OpenUserID, 
                CloseUserID = item.CloseUserID
            };

            return itemModel;
        }

        public static List<OrderDM> MapORDERSListToOrderDMList(this List<ORDERS> items)
        {
            if (items == null)
                return null;

            List<OrderDM> itemModels = new List<OrderDM>();

            foreach (var item in items)
            {
                itemModels.Add(new OrderDM()
                {
                    OrderID = item.OrderID,
                    ShiftID = item.ShiftID,
                    TableID = item.TableID,
                    IsVIP = item.IsVIP,
                    OpenTime = item.OpenTime,
                    CloseTime = item.CloseTime,
                    Closed = item.Closed,
                    OpenUserID = item.OpenUserID,
                    CloseUserID = item.CloseUserID
                });
            }

            return itemModels;
        }
         
    }
}
