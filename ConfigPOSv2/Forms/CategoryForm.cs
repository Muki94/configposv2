﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ConfigPOSv2.Forms
{
    public static class CategoryForm
    {
        public static CategoryDM AddEditCategory(string name, bool active)
        {
            if (name == null || name == String.Empty)
                return null; 
            
            CategoryDM categoryModel = new CategoryDM()
            {
                Name = name,
                Active = active
            };
            
            categoryModel.CategoryID = CategoriesCommunication.CategoriesInsert(categoryModel);

            return categoryModel;
        }
    }
}
