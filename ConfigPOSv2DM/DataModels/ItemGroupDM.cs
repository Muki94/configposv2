﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class ItemGroupDM
    {
        public int ItemgroupID { get; set; }
        
        public string Name { get; set; }
        
        public Nullable<int> ImageID { get; set; }
        public ImageDM Image { get; set; }
    }
}
