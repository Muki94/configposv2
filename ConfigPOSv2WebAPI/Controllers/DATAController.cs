﻿using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConfigPOSv2WebAPI.Controllers
{
    public class DATAController : ApiController
    {

        CategoriesService Categories = new CategoriesService();
        ItemsService Items = new ItemsService();
        SubcategoriesService SubCategories = new SubcategoriesService();
        UsersService Users = new UsersService();

        [Route("api/DATA/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            DataDM data = new DataDM();
            data.ListOfCategory = Categories.CategoriesGetAll();
            data.ListOfImage = Items.ImageGetAll();
            data.ListOfItem = Items.ItemGetAll();
            data.ListOfSubcategory = SubCategories.SubcategoriesGetAll();
            data.ListOfUser = Users.GetUsersAll();
            data.ListOfUserGroup = Users.GetUserGroupAll();
            data.ListOfItemGroup = Items.ItemGroupsGetAll();


            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
