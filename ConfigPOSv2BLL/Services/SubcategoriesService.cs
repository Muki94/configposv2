﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2BLL.Mappers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigPOSv2BLL.Services
{
    public class SubcategoriesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novoj podkategoriji
        /// </summary>
        /// <param name="Name">Naziv podkategorije</param>
        /// <param name="CategoryID">ID kategorije</param>
        /// <param name="Active">Aktivna podkategorija</param>
        public int SubcategoryInsert(SubcategoryDM subcategory)
        {
            OpenConnection();
            int addedSubcategoryId = Convert.ToInt32(entity.stp_SUBCATEGORIES_Insert(subcategory.Name, subcategory.CategoryID, subcategory.Active).FirstOrDefault().Value);
            CloseConnection();

            return addedSubcategoryId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o podkategoriji na osnovu proslijeđenog ID podkategorije
        /// </summary>
        /// <param name="SubcategoryID">ID podkategorije</param>
        /// <param name="CategoryID">ID kategorije</param>
        /// <param name="Name">Naziv podkategorije</param>
        /// <param name="Active">Aktivna podkategorija</param>
        public void SubcategoryUpdate(SubcategoryDM subcategory)
        {
            OpenConnection();
            entity.stp_SUBCATEGORIES_Update(subcategory.SubcategoryID, subcategory.Name, subcategory.CategoryID, subcategory.Active);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja briše podatke o podkategoriji na osnovu proslijeđenog ID podkategorije
        /// </summary>
        /// <param name="SubcategoryID">ID podkategorije</param>
        public void SubcategoryDelete(int SubcategoryID)
        {
            OpenConnection();
            entity.stp_SUBCATEGORIES_Delete(SubcategoryID);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih aktivnih podkategorija 
        /// </summary>
        /// <returns>Lista podkategorija</returns>
        public List<SubcategoryDM> SubcategoriesGetAll()
        {
            OpenConnection();
            List<SubcategoryDM> list = entity.stp_SUBCATEGORIES_GetAll().ToList().MapSubcategoryListToSubcategoryDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća listu svih podkategorija na osnovu prosljeđenog ID kategorije
        /// </summary>
        /// <param name="CategoryID">ID kategorije</param>
        /// <returns>Lista podkategorija</returns>
        public List<SubcategoryDM> SubcategoriesGetByCategoryID(int CategoryID)
        {
            OpenConnection();
            List<SubcategoryDM> list = entity.stp_SUBCATEGORIES_GetByCategoryId(CategoryID).ToList().MapSubcategoryListToSubcategoryDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća podkategoriju na osnovu prosljeđenog ID podkategorije
        /// </summary>
        /// <param name="SubcategoryID">ID podkategorije</param>
        /// <returns>Podkategorija</returns>
        public SubcategoryDM SubcategoriesGetByID(int SubcategoryID)
        {
            OpenConnection();
            SubcategoryDM subcategory = entity.stp_SUBCATEGORIES_GetByID(SubcategoryID).FirstOrDefault().MapSubcategoryToSubcategoryDM();
            CloseConnection();

            return subcategory;
        }
    }
}
