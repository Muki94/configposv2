﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class BillsItemsRoundsItemsDM
    {
        public int BillItemID { get; set; }

        public int RoundItemID { get; set; }
    }
}
