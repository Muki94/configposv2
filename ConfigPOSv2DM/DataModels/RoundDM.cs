﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2DM.DataModels
{
    public class RoundDM
    {
        public int RoundID { get; set; }

        public int Number { get; set; }

        public int UserID { get; set; }

        public int OrderID { get; set; }

        public DateTime OrderTime { get; set; }

        public int StationID { get; set; }
    }
}
