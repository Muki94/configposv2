﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class BillsItemsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public int BillsItemsInsert(BillsItemsDM billItem)
        {
            OpenConnection();
            int BillItemID = Convert.ToInt32(entity.stp_BILLS_ITEMS_Insert(billItem.BillID, billItem.ItemID, billItem.Quantity, billItem.Price, billItem.Total, billItem.DiscountPercentage, billItem.DiscountAmount, billItem.PriceWithoutDiscount, billItem.TotalWithoutDiscount, billItem.TotalTaxesPercentage, billItem.TaxAmount, billItem.TotalWithoutTax, billItem.BasePrice, billItem.BasePriceWithoutDiscount).FirstOrDefault());
            CloseConnection();
            return BillItemID;
        }
        public int BillsItemsRoundsItemsInsert(BillsItemsRoundsItemsDM item)
        {
            OpenConnection();
            int BillItemID = Convert.ToInt32(entity.stp_BILLS_ITEMS_ROUNDS_ITEMS_Insert(item.BillItemID,item.RoundItemID).FirstOrDefault().Value);
            CloseConnection();
            return BillItemID;
        }

    }
}
