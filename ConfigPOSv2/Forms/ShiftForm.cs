﻿using ConfigPOSv2.DataCommunication;
using ConfigPOSv2.Helpers;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.Forms
{
    public static class ShiftForm
    {
        //Dodjeljivanje smjene korisniku
        public static UserDM OpenUserShift(string password, string username)
        {
            var passwordHash = Hash.GetHash(password, Hash.HashType.SHA512);
            UserDM user = UsersCommunication.GetUserByLogin(passwordHash, username);

            if (user == null)
                return null;

            UserShiftDM userShift = new UserShiftDM()
            {
                ShiftID = ShiftsCommunication.ShiftsGetLast().ShiftId,
                UserID = user.UserID
            };

            int id = UserShiftsCommunication.UserShiftsInsert(userShift);

            if (id != 0)
                return user;

            return null;
        }

        //Zatvaranje smjene korisniku
        public static UserDM CloseUserShift(string password, string username)
        {
            var passwordHash = Hash.GetHash(password, Hash.HashType.SHA512);
            UserDM user = UsersCommunication.GetUserByLogin(passwordHash, username);

            if (user == null)
                return null;

            UserShiftDM userShift = new UserShiftDM()
            {
                ShiftID = ShiftsCommunication.ShiftsGetLast().ShiftId,
                UserID = user.UserID
            };

            if (UserShiftsCommunication.UserShiftsClose(userShift)) { 
                return user;
            }

            return null;
        }

        //Provjera dali ima nezatvorenih dnevnika
        public static bool IsShiftOpen()
        {
            ShiftDM shift = ShiftsCommunication.ShiftsGetLast();

            if (shift == null)
                return false;
            else if (shift.Closed)
                return false;

            return true;
        }

        //Dodavanje nove smjene 
        public static bool AddShift()
        {
            int id = ShiftsCommunication.ShiftsInsert(JournalsCommunication.JournalsGetLast().JournalID);

            if (id != 0)
                return true;

            return false;
        }

        //Provjera dali ima nezavrsenih narudjbi za tu smjenu
        public static bool IsOrderClosed(int userId = 0) {
            List<OrderDM> notFinishedOrders = new List<OrderDM>();

            if (userId == 0)
                notFinishedOrders = OrdersCommunication.OrdersGetAllByShiftId(ShiftsCommunication.ShiftsGetLast().ShiftId);
            else
                notFinishedOrders = OrdersCommunication.OrdersGetAllByShiftIdAndUserId(ShiftsCommunication.ShiftsGetLast().ShiftId, userId);

            if (notFinishedOrders != null && notFinishedOrders.Count > 0)
                return false;

            return true;
        }

        //Zatvaranje smjene
        public static bool CloseShift()
        {
            return ShiftsCommunication.ShiftsClose();
            ////napraviti izvjestaj 
        }

        //Zadnje dodata smjena
        public static ShiftDM GetLastShift()
        {
            return ShiftsCommunication.ShiftsGetLast();
        }
    }
}
