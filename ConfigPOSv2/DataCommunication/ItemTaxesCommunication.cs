﻿using ConfigPOSv2.Helpers;
using ConfigPOSv2BLL.Services;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2.DataCommunication
{
    public class ItemTaxesCommunication
    {
        private static ItemTaxesService itemTaxesService = new ItemTaxesService();

        public static List<ItemTaxesViewDM> ItemTaxesGetByItemID(string ItemID)
        {
            if (App.usingWebApi)
                return WebApiHelper.Get<List<ItemTaxesViewDM>>("api/ITEM_TAXES/GetByItemID", new FilterAttributesDM { idName = ItemID});
            else
                return itemTaxesService.ItemTaxesGetByItemID(Convert.ToInt32(ItemID));
        }
    }
}
