﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using ConfigPOSv2BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigPOSv2BLL.Services
{
    public class SectorsService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        /// <summary>
        /// Funkcija koja insertuje podatke o novom sectoru
        /// </summary>
        /// <param name="table">SectorDM objekat</param>
        /// <returns>ID dodanog sectora</returns>
        public int SectorInsert(SectorDM sector)
        {
            OpenConnection();
            int addedSectorId = Convert.ToInt32(entity.stp_SECTORS_Insert(sector.Name, sector.NumberOrder, sector.Layout).FirstOrDefault().Value);
            CloseConnection();

            return addedSectorId;
        }

        /// <summary>
        /// Funkcija koja radi update podataka o sectoru na osnovu proslijeđenog ID sectora
        /// </summary>
        /// <param name="table">SectorDM objekat</param>
        public void SectorUpdate(SectorDM sector)
        {
            OpenConnection();
            entity.stp_SECTORS_Update(sector.SectorID, sector.Name, sector.NumberOrder, sector.Layout);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja radi update podataka o sectoru na osnovu proslijeđenog ID sectora
        /// </summary>
        /// <param name="sectorID">ID sectora</param>
        /// <param name="deleted">Određuje da li je sector izbrisan</param>
        public void SectorUpdateDeleted(int sectorID, bool deleted)
        {
            OpenConnection();
            entity.stp_SECTORS_UpdateDeleted(sectorID, deleted);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja briše podatke o sectoru na osnovu proslijeđenog ID sectora
        /// </summary>
        /// <param name="sectorID">ID sectora</param>
        public void SectorDelete(int sectorID)
        {
            OpenConnection();
            entity.stp_TABLES_Delete(sectorID);
            CloseConnection();
        }

        /// <summary>
        /// Funkcija koja vraća listu svih sectora
        /// </summary>
        /// <returns>Lista sectora</returns>
        public List<SectorDM> SectorsGetAll()
        {
            OpenConnection();
            List<SectorDM> list = entity.stp_SECTORS_GetAll().ToList().MapSectorsListToSectorDMList();
            CloseConnection();

            return list;
        }

        /// <summary>
        /// Funkcija koja vraća sector na osnovu prosljeđenog ID sectora
        /// </summary>
        /// <param name="sectorID">ID Sektora</param>
        /// <returns>SectorDM koji je pronađen</returns>
        public SectorDM SectorsGetByID(int sectorID)
        {
            OpenConnection();
            SectorDM sector = entity.stp_SECTORS_GetByID(sectorID).FirstOrDefault().MapSectorsToSectorDM();
            CloseConnection();

            return sector;
        }
    }
}
