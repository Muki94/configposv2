﻿using ConfigPOSv2Data.Entity;
using ConfigPOSv2DM.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigPOSv2BLL.Mappers;

namespace ConfigPOSv2BLL.Services
{
    public class ItemPricesService
    {
        ConfigPOSv2Entities entity;

        void OpenConnection()
        {
            entity = new ConfigPOSv2Entities();
            entity.Database.Connection.Open();
        }

        void CloseConnection()
        {
            entity.Database.Connection.Close();
            entity.Dispose();
        }

        public ItemPricesDM ItemPricesGetByItemID(int ItemID, int BussinesUnitID)
        {
            OpenConnection();
            ItemPricesDM itemPrice = entity.stp_ITEM_PRICES_GetItemPrice(ItemID, BussinesUnitID).FirstOrDefault().MapItemPriceToItemPriceDM();
            CloseConnection();
            return itemPrice;
        }
    }
}
